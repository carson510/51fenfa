# tako

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.12.1.

## Build & development

Run `grunt` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.


## Tools
JSON Formatter http://www.freeformatter.com/json-formatter.html

## APIs
###  注册 `POST /register`
#### Params
```javascript
{
  "email": "huzhiqiang02@hotmail.com",
  "password": "123456"
}
```
#### Response
```javascript
{
  "message": "ok",
  "ret": 0
}

// Response code return list:
//   Code_Success               = 0
//   Code_Error_OperationFailed = 1
//   Code_Error_InVaildMethod   = 22
//   Code_Error_ParamError      = 23
//   Code_Error_DuplicatedEmail = 26
//   Code_Error_ServerError     = 999
```

### 登录 `POST /login`
#### Params
```javascript
{
  "email": "huzhiqiang02@hotmail.com",
  "password": "123456"
}
```
#### Response
```javascript
{
  "message": "用户名或密码错误",
  "ret": 1002
}
// Response code return list:
//   Code_Error_Unknow          = -1
//   Code_Success               = 0
//   Code_Error_ParamError      = 23
//   Code_Error_NotActiveUser   = 25
//   Code_Error_ServerError     = 999
//   Code_Error_UserIsInVaild   = 1002
//   Code_Error_UserNotExist    = 1005
```

### 发送验证邮件 `POST /registermail`
#### Params
```javascript
{
  "email": "xunchetesting@163.com"
}
```
#### Response
```javascript
{
  "message": "用户不存在",
  "ret": 1005
}
// Response code return list:
//   Code_Error_Unknow          = -1
//   Code_Success               = 0
//   Code_Error_ParamError      = 23
//   Code_Error_ServerError     = 999
//   Code_Error_UserNotExist    = 1005
```

### 解析应用 `POST /parseapp`
#### Params
```javascript
{
  "userid": "xxx",
  "token": "xxx",
  "package": {},
  "platform": 2,
  "arsc": "xxxx",
  "manifest": "xxxx"
}
```
#### Response
```javascript
{
  "message": "ok",
  "ret": 0,
  "data": {
    "url": "",
    "platform": 2,
    "logo": "res/drawable-mdpi/icon.png",
    "appname": "XgsdkDemo",
    "package": "org.cocos2dx.hellocpp",
    "version": "1.0",
    "min_sdkverion": "8"
  }
}
```

###  上传应用 `POST /addapp`
#### Params
package.buildnumber必须是证书，所有字段名均为小写
```javascript
{
  "ispublic": true,
  "userid": "55af48d21048ca2b88000001",
  "token": "36176ee2c01d4bab4d2580ec44814bf1",
  "package": {
    "appname": "XgsdkDemo",
    "bundleid": "",
    "package": "org.cocos2dx.hellocpp",
    "url": "/static/app/android/2ac8cd83-4191-4e3f-6ce2-cdc143db7281.apk",
    "logo": "/static/app/logo/bb023958-4de1-4196-60c8-c9323cb6ac9f.jpg",
    "version": "1.0",
    "platform": 2,
    "size": 1024,
    "buildnumber": 1
  },
  "release_note": "test"
}
```
#### Response
```javascript
{
  "message": "ok",
  "ret": 0,
  "data": {
    "appid": "55b1a886e13823361e00000a",
    "createtime": 1437706409,
    "status": 0,
    "prioroty": 0,
    "package": {
      "url": "/static/app/android/2ac8cd83-4191-4e3f-6ce2-cdc143db7281.apk",
      "platform": 2,
      "logo": "/static/app/logo/bb023958-4de1-4196-60c8-c9323cb6ac9f.jpg",
      "appname": "XgsdkDemo",
      "package": "org.cocos2dx.hellocpp",
      "version": "1.0"
    },
    "release_note": "test",
    "userid": "55ae0785e13823072b000001",
    "AdditionalNote": null,
    "id": "55b1a8a9e13823361e00000e"
  }
}
```

###  应用更新 `GET /app/isvaliduri`
#### Request  
`/app/isvaliduri?appid=55e6b825cca305121c000005&appuri=mq7j`
#### Response
```javascript
{"message":"duplicate value","ret":27,"tag":"uri"}
```
#### Response
```javascript
{"message":"ok","ret":0}
```

###  应用更新 `POST /modifyappinfo`
#### Request
```javascript
{
    "userid":"55b6f570e138237ac6000001", /* required */
    "token":"a7c0dd2b1a1f4efb57404159c2a602aa", /* required */
    "appid": "55b60f1ee138236eff000002", /* required */
    "appname": "App Name", /* optional */
    "uri": "xxxx", /* optional */
    "appdesc": "App Description", /* optional */
    "logourl": "static/app/logo/test.jpg", /* optional */
    "imgurls": ["url1","url2"], /* optional */
    "releasenote": "Release Note", /* optional */
    "mailmsg": { invitetester: "<html></html>"}  /* optional */
}
```
#### Response
```javascript
{"message":"duplicate value","ret":27,"tag":"uri"}
```
#### Response
```javascript
{"message":"non-exist","ret":28,"tag":"appid"}
```
#### Response
```javascript
{"message":"ok","ret":0}
```

### 获取应用列表 `GET /getapps`
#### Params  
cursor, count为分页参数,默认：0,20  
usertype=-1所有用户可见，usertype=0 我创建的，usertype=1我是管理员 2：我是组员，3我是测试人员

#### Request  
`/getapps?usertype=-1`

#### Response
```javascript
{
  "message": "ok",
  "ret": 0,
  "data": {
    "allrows": 1,
    "apps": [
      {
        "createtime": 1438154189,
        "platform": 2,
        "uri": "1yxy",
        "appname": "XgsdkDemo",
        "packagename": "org.cocos2dx.hellocpp",
        "status": 0,
        "priority": 0,
        "releaseversion": {
          "appid": "55b87dcd1048ca2ff8000002",
          "createtime": 1438154501,
          "status": 0,
          "package": {
            "url": "/static/app/android/2ac8cd83-4191-4e3f-6ce2-cdc143db7281.apk",
            "platform": 2,
            "size": 4614586,
            "logo": "/static/app/logo/bb023958-4de1-4196-60c8-c9323cb6ac9f.jpg",
            "appname": "XgsdkDemo",
            "package": "org.cocos2dx.hellocpp",
            "version": "1.0",
            "buildnumber": 1
          },
          "releasenote": "test",
          "userid": "55af48d21048ca2b88000001",
          "downloadcount": 0,
          "id": "55b87f051048ca0ce4000002"
        },
        "userid": "55af48d21048ca2b88000001",
        "ispublic": true,
        "downloadcount": 0,
        "id": "55b87dcd1048ca2ff8000002"
      }
    ],
    "nextcursor": 1
  }
}
```

### 验证App下载密码 `GET /app/password/validate`
#### Request  
`GET /app/password/validate?id=55b87dcd1048ca2ff8000002&password=888888`
#### Response WRONG
```javascript
{"message":"wrong","ret":29,"tag":"password"}
```
#### Response CORRECT
```javascript
{"message":"ok","ret":0,"tag":""}
```

### 获取应用详情 `GET /getappinfo`

#### Request  
`getappinfo?appid=55b87dcd1048ca2ff8000002`

#### Response
```javascript
{
  "message": "ok",
  "ret": 0,
  "data": {
    "app": {
      "createtime": 1438154189,
      "platform": 2,
      "uri": "1yxy",
      "appname": "XgsdkDemo",
      "packagename": "org.cocos2dx.hellocpp",
      "status": 0,
      "priority": 0,
      "releaseversion": {
        "appid": "55b87dcd1048ca2ff8000002",
        "createtime": 1438154501,
        "status": 0,
        "package": {
          "url": "/static/app/android/2ac8cd83-4191-4e3f-6ce2-cdc143db7281.apk",
          "platform": 2,
          "size": 4614586,
          "logo": "/static/app/logo/bb023958-4de1-4196-60c8-c9323cb6ac9f.jpg",
          "appname": "XgsdkDemo",
          "package": "org.cocos2dx.hellocpp",
          "version": "1.0",
          "buildnumber": 1
        },
        "releasenote": "test",
        "userid": "55af48d21048ca2b88000001",
        "downloadcount": 0,
        "id": "55b87f051048ca0ce4000002"
      },
      "userid": "55af48d21048ca2b88000001",
      "ispublic": true,
      "downloadcount": 0,
      "id": "55b87dcd1048ca2ff8000002"
    },
    "appextinfo": {
      "appid": "",
      "appdesc": "",
      "screenshotimg": null,
      "lastmodifytime": 0,
      "id": ""
    }
  }
}
```

### 获取应用下载链接 `GET /getappurl`

#### Request  
`getappurl?id=55b87dcd1048ca2ff8000002`

#### Response
```javascript
{"message":"ok","ret":0,"data": "http://download.tako.im/xxx.apk"}
```

### 获取应用主页下载链接 `GET /app/home/url`
#### Request  
`GET /app/home/url?id=55b87dcd1048ca2ff8000002&password=888888`
#### Response [Wrong Password]
```javascript
{"message":"wrong","ret":29,"tag":"password"}
```
#### Response [Correct Password]
```javascript
{"message":"ok","ret":0,"data": "http://download.tako.im/xxx.apk"}

### 获取应用版本下载链接 `GET /app/version/url`

#### Request  
`/app/version/url?id=55b87dcd1048ca2ff8000002`

#### Response
```javascript
{"message":"ok","ret":0,"data": "http://download.tako.im/xxx.apk"}
```

### 获取应用版本列表 `GET /getappversions`
#### Params  
cursor, count为分页参数,默认：0,20

#### Request  
`/getappversions?appid=55b87dcd1048ca2ff8000002`

#### Response
```javascript
{
  "message": "ok",
  "ret": 0,
  "data": {
    "allrows": 3,
    "appversions": [
      {
        "appid": "55b87dcd1048ca2ff8000002",
        "createtime": 1438154189,
        "status": 0,
        "package": {
          "url": "/static/app/android/2ac8cd83-4191-4e3f-6ce2-cdc143db7281.apk",
          "platform": 2,
          "size": 4614586,
          "logo": "/static/app/logo/bb023958-4de1-4196-60c8-c9323cb6ac9f.jpg",
          "appname": "XgsdkDemo",
          "package": "org.cocos2dx.hellocpp",
          "version": "1.0",
          "buildnumber": 1
        },
        "releasenote": "test",
        "userid": "55af48d21048ca2b88000001",
        "downloadcount": 0,
        "id": "55b87dcd1048ca2ff8000001"
      },
      {
        "appid": "55b87dcd1048ca2ff8000002",
        "createtime": 1438154447,
        "status": 0,
        "package": {
          "url": "/static/app/android/2ac8cd83-4191-4e3f-6ce2-cdc143db7281.apk",
          "platform": 2,
          "size": 4614586,
          "logo": "/static/app/logo/bb023958-4de1-4196-60c8-c9323cb6ac9f.jpg",
          "appname": "XgsdkDemo",
          "package": "org.cocos2dx.hellocpp",
          "version": "1.0",
          "buildnumber": 1
        },
        "releasenote": "test",
        "userid": "55af48d21048ca2b88000001",
        "downloadcount": 0,
        "id": "55b87ecf1048ca0ce4000001"
      },
      {
        "appid": "55b87dcd1048ca2ff8000002",
        "createtime": 1438154501,
        "status": 0,
        "package": {
          "url": "/static/app/android/2ac8cd83-4191-4e3f-6ce2-cdc143db7281.apk",
          "platform": 2,
          "size": 4614586,
          "logo": "/static/app/logo/bb023958-4de1-4196-60c8-c9323cb6ac9f.jpg",
          "appname": "XgsdkDemo",
          "package": "org.cocos2dx.hellocpp",
          "version": "1.0",
          "buildnumber": 1
        },
        "releasenote": "test",
        "userid": "55af48d21048ca2b88000001",
        "downloadcount": 0,
        "id": "55b87f051048ca0ce4000002"
      }
    ],
    "next_cursor": 3
  }
}
```

### 增加测试应用 `POST /addtaskmember`
#### Request  
```javascript
{
    "userid":"55b6f570e138237ac6000001",
    "token":"a7c0dd2b1a1f4efb57404159c2a602aa",
    "appid": "55b60f1ee138236eff000002"
}
```

#### Response
```javascript
{"message":"ok","ret":0}
```

### 获取测试应用列表 `GET /gettaskapps`
#### Params  
cursor, count：分页参数,默认为0,20
pid：平台标识
#### Request  
```javascript
/gettaskapps?userid=55b6f570e138237ac6000001&token=a7c0dd2b1a1f4efb57404159c2a602aa&cursor=0&count=20&pid=2
```

#### Response
```javascript
Same with getapps
```

### 添加反馈 `POST /app/feedback/add`
#### Params  
token和userid可以放在cookie里
```javascript
{
    "token": "36176ee2c01d4bab4d2580ec44814bf1",
    "userid": "55af48d21048ca2b88000001",
    "versionid": "55b87f051048ca0ce4000002",
    "note": "test",
    "imgurls": [
        "http://www.sj88.com/attachments/bd-aldimg/1204/124/2.jpg",
        "http://images.enet.com.cn/2013/5/3/1367452933174.jpg"
    ]
}
```

#### Response
```javascript
{
    "message": "ok",
    "ret": 0,
    "data": { feedback object},
    "tag": "feedback"
    }
}
```

### 获取应用评论列表 `GET /app/feedbacks`
#### Params  
cursor, count为分页参数,默认：0,20  
#### Request  
`/app/feedbacks?appid=55b1a886e13823361e00000a`

#### Response
```javascript
{
  "message": "ok",
  "ret": 0,
  "data": {
    "allrows": 4,
    "feedbacks": [
      {
        "id": "5604fcf1cca305166400000a",
        "note": "反馈测试......",
        "createtime": 1443167473,
        "appid": "5604e4d3cca30520b4000005",
        "versionid": "5604e4d3cca30520b4000004",
        "taskid": "",
        "client": {
          "client_name": "Chrome",
          "client_version": "45.0.2454.93",
          "os_name": "Windows 7",
          "platform": "Windows"
        },
        "voiceurl": "",
        "imgurls": [
          "static/app/logo/47e63366-24fc-46d4-7dd7-e1aabf1f02b0.png",
          "static/app/logo/5e8b0689-bd26-43a2-444c-57d23072f0ed.png",
          "static/app/logo/32e32463-6ad5-4d47-7d50-2a356dcfc8d6.png"
        ],
        "userid": "56020d30cca30520a4000001",
        "username": "carson510",
        "status": "open",
        "likers": [
          {
            "userid": "56020d30cca30520a4000001",
            "username": "carson",
            "time": "2015-09-25T15:51:33.854+08:00"
          },
          {
            "userid": "56020da4cca30520a4000004",
            "username": "",
            "time": "2015-09-25T15:52:20.563+08:00"
          }
        ],
        "comments": [
          {
            "id": "5604fd0ccca305166400000b",
            "userid": "56020d30cca30520a4000001",
            "username": "carson",
            "targetid": "",
            "targetname": "",
            "comment": "cccccccccccccccccc",
            "time": "2015-09-25T15:51:40.212+08:00"
          },
          {
            "id": "5604fd14cca305166400000c",
            "userid": "56020d30cca30520a4000001",
            "username": "carson",
            "targetid": "",
            "targetname": "",
            "comment": "c2",
            "time": "2015-09-25T15:51:48.194+08:00"
          },
          {
            "id": "5604fd3fcca3051664000010",
            "userid": "56020da4cca30520a4000004",
            "username": "carson",
            "targetid": "",
            "targetname": "",
            "comment": "c3",
            "time": "2015-09-25T15:52:31.389+08:00"
          },
          {
            "id": "5604fd46cca3051664000011",
            "userid": "56020da4cca30520a4000004",
            "username": "carson",
            "targetid": "",
            "targetname": "",
            "comment": "c4",
            "time": "2015-09-25T15:52:38.682+08:00"
          }
        ]
      },
      {
        "id": "5604e64acca30520b400000c",
        "note": "three",
        "createtime": 1443161674,
        "appid": "5604e4d3cca30520b4000005",
        "versionid": "5604e4d3cca30520b4000004",
        "taskid": "",
        "client": {
          "client_name": "Chrome",
          "client_version": "45.0.2454.93",
          "os_name": "Windows 7",
          "platform": "Windows"
        },
        "voiceurl": "",
        "imgurls": [],
        "userid": "56020d30cca30520a4000001",
        "username": "carson510",
        "status": "open",
        "likers": [],
        "comments": []
      },
      {
        "id": "5604e644cca30520b400000a",
        "note": "second",
        "createtime": 1443161668,
        "appid": "5604e4d3cca30520b4000005",
        "versionid": "5604e4d3cca30520b4000004",
        "taskid": "",
        "client": {
          "client_name": "Chrome",
          "client_version": "45.0.2454.93",
          "os_name": "Windows 7",
          "platform": "Windows"
        },
        "voiceurl": "",
        "imgurls": [],
        "userid": "56020d30cca30520a4000001",
        "username": "carson510",
        "status": "open",
        "likers": [],
        "comments": []
      },
      {
        "id": "5604e63fcca30520b4000008",
        "note": "first",
        "createtime": 1443161663,
        "appid": "5604e4d3cca30520b4000005",
        "versionid": "5604e4d3cca30520b4000004",
        "taskid": "",
        "client": {
          "client_name": "Chrome",
          "client_version": "45.0.2454.93",
          "os_name": "Windows 7",
          "platform": "Windows"
        },
        "voiceurl": "",
        "imgurls": [],
        "userid": "56020d30cca30520a4000001",
        "username": "carson510",
        "status": "open",
        "likers": [],
        "comments": []
      }
    ],
    "nextcursor": 4
  }
}
```

### 获取应用评论列表 `GET /app/feedbacks/all`
#### Params  
cursor, count为分页参数,默认：0,20  
#### Request  
`/app/feedbacks/all?appid=55b1a886e13823361e00000a`

#### Response
```javascript
Same with /app/feedbacks
```

### 获取某个应用我的评论列表 `GET /app/feedbacks/my`
#### Params  
cursor, count为分页参数,默认：0,20  

#### Request  
`/app/feedbacks/my?appid=55b1a886e13823361e00000a`

#### Response
```javascript
Same with /app/feedbacks
```

### 删除反馈 `DELETE /app/feedback/delete`
#### Request 
`/app/feedback/delete/5604e63fcca30520b4000008`
#### Response
```javascript
{"message":"ok","ret":0}
```

### 关闭反馈 `PUT /app/feedback/status/update`
#### Request 
```javascript
{"id": "55af48d21048ca2b88000001", "status": "close"}
```

#### Response
```javascript
{"message":"ok","ret":0}
```

### 反馈点赞 `POST /app/feedback/like`
#### Request 
```javascript
{ "feedbackid":"5604e63fcca30520b4000008" }
```
#### Response
```javascript
{"message":"ok","ret":0}
```

### 反馈点赞取消 `DELETE /app/feedback/like/delete`
#### Request 
```javascript
{ "feedbackid":"5604e63fcca30520b4000008" }
```
#### Response
```javascript
{"message":"ok","ret":0}
```

### 反馈评论 `POST /app/feedback/comment`
#### Request 
```javascript
{
    "feedbackid":"5604e64acca30520b400000c",
    "comment": "c"
}
```
#### Response
```javascript
{
  "message": "ok",
  "ret": 0,
  "tag": "comment",
  "data": {
    "id": "5604ff8dcca3051664000018",
    "feedbackid": "5604e64acca30520b400000c",
    "userid": "56020d30cca30520a4000001",
    "username": "",
    "targetid": "",
    "targetname": "",
    "comment": "c",
    "time": "2015-09-25T16:02:21.239334+08:00"
  }
}
```

### 反馈评论删除 `DELETE /app/feedback/comment/delete`
#### Request 
```javascript
{
    "id": "5604f9aacca3051664000005",
    "feedbackid":"5604e64acca30520b400000c"
}
```
#### Response
```javascript
{"message":"ok","ret":0}
```

### 获取某个应用的发布版本 `GET /getappreleaseversion`

#### Request  
`/getappreleaseversion?appid=55b87dcd1048ca2ff8000002`

#### Response
```javascript
{
  "message": "ok",
  "ret": 0,
  "data": {
    "appid": "55b87dcd1048ca2ff8000002",
    "createtime": 1438154501,
    "status": 0,
    "package": {
      "url": "/static/app/android/2ac8cd83-4191-4e3f-6ce2-cdc143db7281.apk",
      "platform": 2,
      "size": 4614586,
      "logo": "/static/app/logo/bb023958-4de1-4196-60c8-c9323cb6ac9f.jpg",
      "appname": "XgsdkDemo",
      "package": "org.cocos2dx.hellocpp",
      "version": "1.0",
      "buildnumber": 1
    },
    "releasenote": "test",
    "userid": "55af48d21048ca2b88000001",
    "downloadcount": 0,
    "id": "55b87f051048ca0ce4000002"
  }
}
```


### 下载日志 `POST /createdownloadlog`
#### Params
```javascript
{
  "userid":"55b883453c9a561250000001",
  "token":"d0549c7f34ae47b249b36eddd5efbec0",
  "appversionid":"55b8b6063c9a560f8c000002"
}
```

### 获取KS3的js上传授权 `Get /ks3/auth`

#### Request  
`/ks3/auth`

#### Response
```javascript
{
	"message": "ok",
	"ret": 0,
	"data": {
		"KSSAccessKeyId": "xxxxxxx",
		"policy": "eyJleHBpcmF0aW9uIjogIjQ3MjgzNjgtNDcyODQ5Ni00NzI4NjI0VDQ3Mjg3NTI6NDcyODg4MDo0NzI5MDA4WiIsImNvbmRpdGlvbnMiOiBbeyJhY2wiOiAicHJpdmF0ZSIgfSx7ImJ1Y2tldCI6ICJ0YWtvIiB9LFsic3RhcnRzLXdpdGgiLCAiJGtleSIsICJhcHAvIl0sXX0=",
		"signature": "A20ltSu1NQsANu02WDU5ftBNixU=",
		"bucket_name": "tako",
		"key": "app/4436a5ad-530a-45a1-4e3d-f6080f864cba",
		"uploadDomain": "http://kssws.ks-cdn.com/tako"
	}
}
```

### 获取KS3下载地址 `Get /ks3/url`

#### Request  
`/ks3/url?path=test/voice/696a6080-e311-4a30-8bea-747ae46fc366.amr`

#### Response
{"message":"ok","ret":0,"data": "http://download.tako.im/test/voice/696a6080-e311-4a30-8bea-747ae46fc366?xxxxxxxxx"}
```

### 上传文件 `POST /upload`
#### Params  
`type` 上传文件类型  | (放在url中) 1:Screenshot 2:AppIcon	 3:Package_iOS	4:Package_Android  
`filename` 文件内容 | (放在body中)

#### Request  
`/upload?type=-1`

#### Response
```javascript
{
  "message": "ok",
  "ret": 0,
  "data": "xxx(filepath)"
}
```

### 添加成员 `POST /addmember`
#### Params  
usertype 1 admin 2 普通成员 3测试成员
#### Request  
```javascript
{
  "appid": "55b1a886e13823361e00000a",
  "users": [
    {
      "usertype": 1,
      "tangjie2@kingsoft.com",
    }
  ]
}
```

#### Response
```javascript
{
  "message": "ok",
  "ret": 0
}
```

### 删除人员 `DELETE /removemember`

#### Request  
```javascript
{
  "appid": "55b1a886e13823361e00000a",
  "memberids": [
    "55b087a9e1382326cf00000c"
  ]
}
```

### 获取所有成员 `GET /findallmember`
#### Params  
usertype : -1 不限，0：onwer 1admin 2普通成员 3测试成员

#### Request
`GET /findallmember?appid=55b1a886e13823361e00000a`

#### Response
```javascript
{
  "message": "ok",
  "ret": 0,
  "data": [
    {
      "appid": "55b1a886e13823361e00000a",
      "usertype": 0,
      "userid": "55ae0785e13823072b000001",
	  "email": "258627934qq.com"
      "addtime": 1437706374,
      "id": "55b1a886e13823361e00000c"
    },
    {
      "appid": "55b1a886e13823361e00000a",
      "usertype": -1,
      "userid": "ALL",
	  "email": "258627934qq.com"
      "addtime": 1437706374,
      "id": "55b1a886e13823361e00000d"
    },
    {
      "appid": "55b1a886e13823361e00000a",
      "usertype": 1,
      "userid": "55b0879de1382326cf000007",
	  "email": "258627934qq.com"
      "addtime": 1437706409,
      "id": "55b1a8a9e13823361e00000f"
    }
  ]
}
```
### 获取被邀请人邮件 `GET /getinvitedemail`
#### Request
`GET /getinvitedemail?token=55b1a886e13823361e00000a`

#### Response
```javascript
{"message":"","ret":0,"data":"invited@kingsoft.com"}
```

#### Request
`GET /getinvitedmember?appid=55b1a886e13823361e00000a`

#### Response
```javascript
{
  "message": "ok",
  "ret": 0,
  "data": [
    {
      "appid": "55b1a886e13823361e00000a",
      "usertype": 0,
	  "email": "258627934qq.com"
      "addtime": 1437706374,
      "id": "55b1a886e13823361e00000c"
    },
    {
      "appid": "55b1a886e13823361e00000a",
      "usertype": -1,
	  "email": "258627934qq.com"
      "addtime": 1437706374,
      "id": "55b1a886e13823361e00000d"
    },
    {
      "appid": "55b1a886e13823361e00000a",
      "usertype": 1,
	  "email": "258627934qq.com"
      "addtime": 1437706409,
      "id": "55b1a8a9e13823361e00000f"
    }
  ]
}
```

### 获取所有成员 `GET /getmyrole`
#### Params  
usertype : 0：onwer 1admin 2普通成员 3测试成员

#### Request
`GET /getmyrole?appid=55b1a886e13823361e00000a`

#### Response
```javascript
{
   "message": "ok",
   "ret": 0,
   "data": {
      "appid": "55b87dcd1048ca2ff8000002",
      "usertype": 0,
      "userid": "55af48d21048ca2b88000001",
      "email": "",
      "addtime": 1438154189,
      "id": "55b87dcd1048ca2ff8000004"
   }
}
```

### 获取所有成员 `GET /getinvitedmember`

#### Request
`GET /getinvitedmember?appid=55b87dcd1048ca2ff8000002`

#### Response
```javascript
{
    "message": "ok",
    "ret": 0,
    "data": {
        "inviteuserid": "55af48d21048ca2b88000001",
        "email": "258627934@qq.com",
        "token": "d397a6a6-65c0-48fb-40d1-1609abeb0ef5",
        "appid": "55b87dcd1048ca2ff8000002",
        "usertype": 0,
        "addtime": 1438313040,
        "activetime": 0,
        "status": 0,
        "id": "55baea501048cadec8000001"
    }
}
```

### 邀请测试成员 `POST /task/tester/invite`
#### Request  
```javascript
{
  "appid": "55b1a886e13823361e00000a",
  "members": [
    { "email": "a@kingsoft.com" },
    { "email": "b@kingsoft.com" },
    { "email": "c@kingsoft.com" }
  ]
}
```
#### Response
```javascript
{"message":"ok","ret":0}
```

### 获取所有测试人员 `GET /task/tester/all`

#### Request
`GET /task/tester/all?appid=55b1a886e13823361e00000a`

#### Response
```javascript
{
  "message": "ok",
  "ret": 0,
  "data": [
    { ... },
    { ... },
    { ... }
  ]
}
```

### 获取所有邀请的测试人员 `GET `

#### Request
`GET /task/tester/invited?appid=55b87dcd1048ca2ff8000002`

#### Response
```javascript
{
  "message": "ok",
  "ret": 0,
  "data": [
    { ... },
    { ... },
    { ... }
  ]
}
```

### 删除测试人员 `DELETE /task/tester/remove`
#### Request  
```javascript
{ "ids": ["", ""] }
```
#### Response
```javascript
{"message":"ok","ret":0}
```

### 删除邀请中人员(成员/测试人员) `DELETE /invite/remove`
#### Request  
```javascript
{ "ids": ["", ""] }
```
#### Response
```javascript
{"message":"ok","ret":0}
```

### 测试人员确认邀请 `GET /invite/tester/confirm`
#### Request
`GET /invite/tester/confirm?key=55b1a886e13823361e00000a`

#### Response
```javascript
location = /#/tester/confirm/true|false
```

### 获取当前用户DSP访问权限 `GET /dsp/permission`
#### Request  
`GET /dsp/permission`
#### Response NO
```javascript
{"message":"权限不足","ret":1004,"tag":""}
```
#### Response YES
```javascript
{"message":"ok","ret":0,"tag":""}
```

### 获取DSP项目列表 `GET /dsp/projects`

#### Request
`GET /dsp/projects`

#### Response
```javascript
{
  "message": "ok",
  "ret": 0,
  "data": [
    {
      "id": "26270A2B2ACC688F",
      "key": "r3oeRo2ePxp81kPP",
      "name": "剑网3"
    },
    {
      "id": "87AC1EDB1A7FE073",
      "key": "TwzEquComLLz4dN8",
      "name": "剑一"
    }
  ]
}
```

### 邀请DSP测试人员 `POST /dsp/addtester`
#### Request  
```javascript
{
  "appid": "55e6b825cca305121c000005",
  "projects": [
    {
      "id": "26270A2B2ACC688F",
      "key": "r3oeRo2ePxp81kPP"
    },
    {
      "id": "87AC1EDB1A7FE073",
      "key": "TwzEquComLLz4dN8"
    }
  ]
}
```
#### Response
```javascript
{"message":"ok","ret":0}
```


## =================问卷调查=================

### 获取应用的所有问卷 `GET /app/forms`
#### Request
`/app/forms?appid=55b1a886e13823361e00000a`
#### Response
```javascript
{
  "message": "ok",
  "ret": 0,
  "tag": "forms",
  "data": {
    "allrows": 6,
    "forms": [
      {
        "id": "55fb7eaecca3052f98000001",
        "appid": "55f6805acca305285c000003",
        "name": "Form Test2",
        "desc": "This is a from description2",
        "shuffle": false,
        "resubmit": false,
        "responsetext": "Your response has been recorded2.",
        "status": "draft",
        "itemcount": 6,
        "firstcreator": "55ec05f2cca3050680000004",
        "firstcreated": "2015-09-18T11:02:06.104+08:00",
        "lastmodifier": "55ec05f2cca3050680000004",
        "lastmodified": "2015-09-18T11:02:06.116+08:00"
      },
      {
        "id": "55fb7ea5cca3052cdc000001",
        "appid": "55f6805acca305285c000003",
        "name": "Form Test2",
        "desc": "This is a from description2",
        "shuffle": false,
        "resubmit": false,
        "responsetext": "Your response has been recorded2.",
        "status": "draft",
        "itemcount": 6,
        "firstcreator": "55ec05f2cca3050680000004",
        "firstcreated": "2015-09-18T11:01:57.552+08:00",
        "lastmodifier": "55ec05f2cca3050680000004",
        "lastmodified": "2015-09-18T11:01:57.585+08:00"
      },
      {
        "id": "55fb7e9dcca30529fc000001",
        "appid": "55f6805acca305285c000003",
        "name": "Form Test2",
        "desc": "This is a from description2",
        "shuffle": false,
        "resubmit": false,
        "responsetext": "Your response has been recorded2.",
        "status": "draft",
        "itemcount": 6,
        "firstcreator": "55ec05f2cca3050680000004",
        "firstcreated": "2015-09-18T11:01:49.298+08:00",
        "lastmodifier": "55ec05f2cca3050680000004",
        "lastmodified": "2015-09-18T11:01:49.311+08:00"
      },
      {
        "id": "55fb7e95cca30527f8000001",
        "appid": "55f6805acca305285c000003",
        "name": "Form Test2",
        "desc": "This is a from description2",
        "shuffle": false,
        "resubmit": false,
        "responsetext": "Your response has been recorded2.",
        "status": "draft",
        "itemcount": 6,
        "firstcreator": "55ec05f2cca3050680000004",
        "firstcreated": "2015-09-18T11:01:41.619+08:00",
        "lastmodifier": "55ec05f2cca3050680000004",
        "lastmodified": "2015-09-18T11:01:41.636+08:00"
      },
      {
        "id": "55fb7e8dcca3051b90000001",
        "appid": "55f6805acca305285c000003",
        "name": "Form Test2",
        "desc": "This is a from description2",
        "shuffle": false,
        "resubmit": false,
        "responsetext": "Your response has been recorded2.",
        "status": "draft",
        "itemcount": 6,
        "firstcreator": "55ec05f2cca3050680000004",
        "firstcreated": "2015-09-18T11:01:33.015+08:00",
        "lastmodifier": "55ec05f2cca3050680000004",
        "lastmodified": "2015-09-18T11:01:33.026+08:00"
      },
      {
        "id": "55fb7e84cca3052e64000001",
        "appid": "55f6805acca305285c000003",
        "name": "Form Test2",
        "desc": "This is a from description2",
        "shuffle": false,
        "resubmit": false,
        "responsetext": "Your response has been recorded2.",
        "status": "draft",
        "itemcount": 6,
        "firstcreator": "55ec05f2cca3050680000004",
        "firstcreated": "2015-09-18T11:01:24.2+08:00",
        "lastmodifier": "55ec05f2cca3050680000004",
        "lastmodified": "2015-09-18T11:01:24.213+08:00"
      }
    ],
    "nextcursor": 6
  }
}
```

### 获取问卷的详细信息(已包含答案) `GET /app/form/:id`
#### Request
`/app/form/55fb7e84cca3052e64000001`
#### Response
```javascript
{
  "message": "ok",
  "ret": 0,
  "tag": "form",
  "data": {
    "id": "55ffcd6dcca30501fc000001",
    "appid": "55eff0b2e138231c8200000c",
    "name": "Form Test2",
    "desc": "This is a from description2",
    "shuffle": false,
    "resubmit": false,
    "responsetext": "Your response has been recorded2.",
    "status": "draft",
    "itemcount": 7,
    "firstcreator": "55ec05f2cca3050680000004",
    "firstcreated": "2015-09-21T17:27:09.454+08:00",
    "lastmodifier": "55ec05f2cca3050680000004",
    "lastmodified": "2015-09-21T17:27:09.978+08:00",
    "response": {
      "id": "55ffcd6ecca30501fc00000a",
      "appid": "55eff0b2e138231c8200000c",
      "formid": "55ffcd6dcca30501fc000001",
      "begintime": "2015-09-21T17:27:10.135+08:00",
      "progress": 0,
      "endtime": "2015-09-21T17:27:10.385+08:00",
      "responderid": "55ec05f2cca3050680000004"
    },
    "items": [
      {
        "id": "55ffcd6dcca30501fc000009",
        "appid": "55eff0b2e138231c8200000c",
        "formid": "55ffcd6dcca30501fc000001",
        "title": "How about your school record?",
        "desc": "Your school record",
        "required": true,
        "shuffle": false,
        "gotoonanswer": false,
        "type": "grid",
        "grid": {
          "row": [
            {
              "text": "Art"
            },
            {
              "text": "History"
            },
            {
              "text": "Music"
            },
            {
              "text": "Biography"
            },
            {
              "text": "Maths"
            }
          ],
          "column": [
            {
              "text": "A"
            },
            {
              "text": "B"
            },
            {
              "text": "C"
            },
            {
              "text": "D"
            }
          ]
        },
        "index": 1,
        "response": {
          "id": "55ffcd6ecca30501fc000019",
          "questiontype": "grid",
          "comment": "",
          "itemanswers": [
            {
              "id": "55ffcd6ecca30501fc00001a",
              "questiontype": "grid",
              "baseon": "Art",
              "answer": "A"
            },
            {
              "id": "55ffcd6ecca30501fc00001b",
              "questiontype": "grid",
              "baseon": "History",
              "answer": "A"
            },
            {
              "id": "55ffcd6ecca30501fc00001c",
              "questiontype": "grid",
              "baseon": "Music",
              "answer": "A"
            },
            {
              "id": "55ffcd6ecca30501fc00001d",
              "questiontype": "grid",
              "baseon": "Biography",
              "answer": "A"
            },
            {
              "id": "55ffcd6ecca30501fc00001e",
              "questiontype": "grid",
              "baseon": "Maths",
              "answer": "A"
            }
          ]
        }
      },
      {
        "id": "55ffcd6dcca30501fc000007",
        "appid": "55eff0b2e138231c8200000c",
        "formid": "55ffcd6dcca30501fc000001",
        "title": "Your score, please?",
        "desc": "Your score",
        "required": true,
        "shuffle": false,
        "gotoonanswer": false,
        "type": "slider",
        "slider": {
          "minvalue": 1,
          "maxvalue": 5
        },
        "index": 2,
        "response": {
          "id": "55ffcd6ecca30501fc000017",
          "questiontype": "slider",
          "comment": "",
          "itemanswers": [
            {
              "id": "55ffcd6ecca30501fc000018",
              "questiontype": "slider",
              "answer": "4"
            }
          ]
        }
      },
      {
        "id": "55ffcd6dcca30501fc000003",
        "appid": "55eff0b2e138231c8200000c",
        "formid": "55ffcd6dcca30501fc000001",
        "title": "What's your story",
        "desc": "Your story",
        "required": true,
        "shuffle": false,
        "gotoonanswer": false,
        "type": "ptext",
        "text": {
          "minlength": 10,
          "maxlength": 100
        },
        "index": 3,
        "response": {
          "id": "55ffcd6ecca30501fc00000d",
          "questiontype": "ptext",
          "comment": "",
          "itemanswers": [
            {
              "id": "55ffcd6ecca30501fc00000e",
              "questiontype": "ptext",
              "answer": "my story\nmy story"
            }
          ]
        }
      },
      {
        "id": "55ffcd6dcca30501fc000004",
        "appid": "55eff0b2e138231c8200000c",
        "formid": "55ffcd6dcca30501fc000001",
        "title": "What's your gender",
        "desc": "Your gender",
        "required": true,
        "shuffle": false,
        "gotoonanswer": false,
        "type": "radio",
        "radio": {
          "options": [
            {
              "text": "Male"
            },
            {
              "text": "Female"
            }
          ]
        },
        "index": 4,
        "response": {
          "id": "55ffcd6ecca30501fc00000f",
          "questiontype": "radio",
          "comment": "",
          "itemanswers": [
            {
              "id": "55ffcd6ecca30501fc000010",
              "questiontype": "radio",
              "answer": "Male"
            }
          ]
        }
      },
      {
        "id": "55ffcd6dcca30501fc000005",
        "appid": "55eff0b2e138231c8200000c",
        "formid": "55ffcd6dcca30501fc000001",
        "title": "What color do you like?",
        "desc": "From seven colors",
        "required": true,
        "shuffle": false,
        "gotoonanswer": false,
        "type": "checkbox",
        "checkbox": {
          "options": [
            {
              "text": "Red"
            },
            {
              "text": "Orange"
            },
            {
              "text": "Yellow"
            },
            {
              "text": "Green"
            },
            {
              "text": "Blue"
            },
            {
              "text": "Indigo"
            },
            {
              "text": "Violet"
            }
          ],
          "atleast": 0,
          "atleasterror": "",
          "atmost": 0,
          "atmosterror": "",
          "exact": 0,
          "exacterror": ""
        },
        "index": 5,
        "response": {
          "id": "55ffcd6ecca30501fc000011",
          "questiontype": "checkbox",
          "comment": "",
          "itemanswers": [
            {
              "id": "55ffcd6ecca30501fc000012",
              "questiontype": "checkbox",
              "answer": "Green"
            },
            {
              "id": "55ffcd6ecca30501fc000013",
              "questiontype": "checkbox",
              "answer": "Blue"
            },
            {
              "id": "55ffcd6ecca30501fc000014",
              "questiontype": "checkbox",
              "answer": "Indigo"
            }
          ]
        }
      },
      {
        "id": "55ffcd6dcca30501fc000006",
        "appid": "55eff0b2e138231c8200000c",
        "formid": "55ffcd6dcca30501fc000001",
        "title": "How old are you?",
        "desc": "Your age",
        "required": true,
        "shuffle": false,
        "gotoonanswer": false,
        "type": "list",
        "list": {
          "options": [
            {
              "text": "1~17"
            },
            {
              "text": "18~25"
            },
            {
              "text": "26~30"
            },
            {
              "text": "31~35"
            },
            {
              "text": "35~"
            }
          ]
        },
        "index": 6,
        "response": {
          "id": "55ffcd6ecca30501fc000015",
          "questiontype": "list",
          "comment": "",
          "itemanswers": [
            {
              "id": "55ffcd6ecca30501fc000016",
              "questiontype": "list",
              "answer": "18~25"
            }
          ]
        }
      },
      {
        "id": "55ffcd6dcca30501fc000002",
        "appid": "55eff0b2e138231c8200000c",
        "formid": "55ffcd6dcca30501fc000001",
        "title": "What's your name",
        "desc": "Your name",
        "required": true,
        "shuffle": false,
        "gotoonanswer": false,
        "type": "text",
        "text": {
          "minlength": 10,
          "maxlength": 100
        },
        "index": 7,
        "response": {
          "id": "55ffcd6ecca30501fc00000b",
          "questiontype": "text",
          "comment": "",
          "itemanswers": [
            {
              "id": "55ffcd6ecca30501fc00000c",
              "questiontype": "text",
              "answer": "carson"
            }
          ]
        }
      }
    ]
  }
}
```

### 新增或修改问卷基本信息 `POST /app/form/edit`
#### Request
```javascript
{
    "id": "", //不存在表示新增，存在表示修改
    "appid": "55f0dc4acca3051f8c000005",
	"name": "Form Test2",
	"desc": "This is a from description2",
	"shuffle": false,
	"resubmit": false,
	"responsetext": "Your response has been recorded2."
}
```
#### Response
```javascript
{"message":"ok","ret":0,"tag":"id","data"55ec05f2cca3450680000004"}
```

### 新增或修改问卷问题项 `POST /app/form/item/edit`
#### Request
```javascript
{
    "id": "", //不存在表示新增，存在表示修改
	"appid": "55f0dc4acca3051f8c000005",
	"formid": "55fb6102cca3052cd4000003",
	"title": "What's your name",
	"desc": "Your name",
	"required": true,
	"shuffle": false,
	"gotoonanswer": false,
	"type": "text",
	"text": {
		"minlength": 10,
		"maxlength": 100
	}
}
```
#### Response
```javascript
{"message":"ok","ret":0,"tag":"id","data"55ec05f2cca3450680000004"}
```

### 开始答卷(用于记录答卷开始时间，获取答卷进度信息等) `GET /app/form/prerespond`
#### Request
`/app/form/prerespond?formid=55fb7e84cca3052e64000001`
#### Response
```javascript
{
  "message": "ok",
  "ret": 0,
  "tag": "formResponse",
  "data": {
    "id": "55fba34fcca3052040000001",
    "appid": "55f6805acca305285c000003",
    "formid": "55fb7e84cca3052e64000001",
    "begintime": "2015-09-18T13:38:23.804+08:00",
    "progress": 0, //0表示问卷答题完成，1表示未开始答题，大于1表示答题进度
    "endtime": "2015-09-18T14:50:53.941+08:00",
    "responderid": "55ec05f2cca3050680000004",
    "firstcreator": "55ec05f2cca3050680000004",
    "firstcreated": "2015-09-18T13:38:23.804+08:00",
    "lastmodifier": "55ec05f2cca3050680000004",
    "lastmodified": "2015-09-18T14:50:53.941+08:00"
  }
}
```

### 根据问题编号(1-N,默认为1)获取问题的信息，已包含当前用户答案，用于移动端逐个答题 `GET /app/form/item/byindex`
#### Request
`/app/form/item/index?formid=55fb7e84cca3052e64000001&index=4`
#### Response
```javascript
{
  "message": "ok",
  "ret": 0,
  "tag": "formItem",
  "data": {
    "id": "55fb7e84cca3052e64000005",
    "appid": "55f6805acca305285c000003",
    "formid": "55fb7e84cca3052e64000001",
    "title": "How old are you?",
    "desc": "Your age",
    "required": true,
    "shuffle": false,
    "gotoonanswer": false,
    "type": "list",
    "list": {
      "options": [
        {
          "text": "1~17"
        },
        {
          "text": "18~25"
        },
        {
          "text": "26~30"
        },
        {
          "text": "31~35"
        },
        {
          "text": "35~"
        }
      ]
    },
    "index": 4,
    "response": {
      "id": "55fbe522cca30518f000001b",
      "questiontype": "list",
      "comment": "",
      "itemanswers": [
        {
          "id": "55fbe522cca30518f000001c",
          "questiontype": "list",
          "answer": "18~25",
        }
      ]
    }
  }
}
```

### 提交问卷，用于PC答卷 `POST /app/form/respond`
#### Request
```javascript
{
    "id": "55fba34fcca3052040000001", //整个问卷应答的id
    "itemresponses": [
        {
            "id": "", //不存在表示新增，存在表示修改
            "itemid": "55fb7e84cca3052e64000002",
            "questiontype": "text",
            "comment": "Why chose this answer",
            "itemanswers": [
                {
                    "answer": "carson"
                }
            ]
        },
        {
            "id": "",
            "itemid": "55fb7e84cca3052e64000003",
            "questiontype": "radio",
            "comment": "",
            "itemanswers": [
                {
                    "answer": "Male"
                }
            ]
        },
        {
            "id": "",
            "itemid": "55fb7e84cca3052e64000004",
            "questiontype": "checkbox",
            "comment": "",
            "itemanswers": [
                {
                    "answer": "Green"
                },
                {
                    "answer": "Blue"
                },
                {
                    "answer": "Indigo"
                }
            ]
        },
        {
            "id": "",
            "itemid": "55fb7e84cca3052e64000005",
            "questiontype": "list",
            "comment": "",
            "itemanswers": [
                {
                    "answer": "18~25"
                }
            ]
        },
        {
            "id": "",
            "itemid": "55fb7e84cca3052e64000006",
            "questiontype": "slider",
            "comment": "",
            "itemanswers": [
                {
                    "answer": "4"
                }
            ]
        },
        {
            "id": "",
            "itemid": "55fb7e84cca3052e64000007",
            "questiontype": "grid",
            "comment": "",
            "itemanswers": [
                {
                    "baseon": "Art",
                    "answer": "A"
                },
                {
                    "baseon": "History",
                    "answer": "B"
                },
                {
                    "baseon": "Music",
                    "answer": "D"
                },
                {
                    "baseon": "Biography",
                    "answer": "C"
                },
                {
                    "baseon": "Maths",
                    "answer": "A"
                }
            ]
        }
    ]
}
```
#### Response
```javascript
{"message":"ok","ret":0}
```

### 提交问卷，用于移动端答卷 `POST /app/form/item/respond`
#### Request
```javascript
{
    "id": "", //不存在表示新增，存在表示修改
    "itemid": "55fb6102cca3052cd4000003",
    "formreponseid": "55f0dc4acca3051f8c000005",
    "questiontype": "text",
    "comment": "Why chose this answer",
    "itemanswers": [
        {
            "answer": "carson"
        }
    ]
}
```
#### Response
```javascript
{"message":"ok","ret":0,"tag":"id","data"55ec05f2cca3450680000004"}
```

### 问卷问题排序[移动] `PUT /app/form/item/move`
#### Request
```javascript
{
    "srcId": "55fb6102cca3052cd4000003",
    "aboveId": "55fb6102cca3052cd4000003"
}
```
#### Response
```javascript
{"message":"ok","ret":0}
```

### 删除问卷问题 `DELETE /app/form/item/delete`
#### Request
`DELETE /app/form/item/delete/55ec05f2cca3450680000004`
#### Response
```javascript
{"message":"ok","ret":0}
```