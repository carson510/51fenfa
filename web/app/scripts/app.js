'use strict';

/**
 * @ngdoc overview
 * @name takoApp
 * @description
 * # takoApp
 *
 * Main module of the application.
 */

angular
.module('takoApp', [
  'ngAnimate',
  'ngCookies',
  'ngResource',
  'ngRoute',
  'ngSanitize',
  'ngMaterial',
  'toaster',
  'froala',
  'ui.sortable',
  'ui.bootstrap',
  'infinite-scroll',
  'angularLazyImg',
  'angularSpinner',
  'bootstrapLightbox',
  'angular-loading-bar',
  'monospaced.qrcode'
])
.config(function ($routeProvider, $locationProvider) {
  var hostname = window.location.hostname;
  if (hostname != 'localhost' && hostname != '127.0.0.0' && hostname != '0.0.0.0') {
    $locationProvider.html5Mode(true);
  }
  $routeProvider
    .when('/', {
      //templateUrl: 'views/home.html',
      //controller: 'HomeCtrl'
      redirectTo: function() {
          return '/dash';
      }
    })
    .when('/user', {
      templateUrl: 'views/user.html',
      controller: 'UserCtrl'
    })
    .when('/dash', {
      templateUrl: 'views/dash.html',
      controller: 'DashCtrl'
    })
    .when('/group', {
      templateUrl: 'views/group.html',
      controller: 'GroupCtrl'
    })
    .when('/storage', {
      templateUrl: 'views/storage.html',
      controller: 'StorageCtrl'
    })
    .when('/item/:id', {
      templateUrl: 'views/item.html',
      controller: 'ItemCtrl'
    })
    .when('/signin/:to?', {
      templateUrl: 'views/login.html',
      controller: 'LoginCtrl'
    })
    .when('/login_with/:email/:role?', {
      templateUrl: 'views/login.html',
      controller: 'LoginCtrl'
    })
    .when('/activate/:result/:email?', {
      templateUrl: 'views/activate.html',
      controller:  'ActivateCtrl'
    })
    .when('/signup/:token?', {
      templateUrl: 'views/signup.html',
      controller: 'SignupCtrl'
    })
    .when('/release', {
      templateUrl: 'views/upload.html',
      controller: 'UploadCtrl'
    })
    .when('/uploads', {
      templateUrl: 'views/uploads.html',
      controller: 'UploadsCtrl'
    })
    .when('/store', {
      templateUrl: 'views/store.html',
      controller: 'StoreCtrl'
    })
    .when('/tester/confirm/:result', {
      templateUrl: 'views/tester_confirm.html',
      controller: 'TesterConfirmCtrl'
    })
    .when('/:appid', {
      templateUrl: 'appinfo.html',
      controller: 'appInfoCtrl'
    })
    .when('/web/private', {
      templateUrl: 'views/private.html',
      controller: 'privateCtrl'
    })
    .when('/web/404', {
      templateUrl: 'views/404.html'
    })
    .when('/web/jx3', {
      templateUrl: 'views/jx3.html',
      controller: 'jx3Ctrl'
    })
    .otherwise({
      redirectTo: function() {
        return '/';
      }
    });
})
.run(function($window, $rootScope, $location) {
  angular.element($window).on('resize', function() {
    angular.element('#main').css('min-height', angular.element(window).height());
  }).trigger('resize');
  $rootScope.goto = function(path) {
    if (!path) return;
    if ($location.path().indexOf("/#/") > 0) {
      if (path.substr(0, 1) == "/") {
        path = "/#" + path;
      } else {
        path = "/#/" + path;
      }
    }
    $location.path(path);
  };

  $rootScope.isNotEmpty = function(str) {
    return str != null && str != ""
  }

  var url = window.location.href;
  var begin = url.indexOf("(");
  var end = url.indexOf(")")
  $rootScope.serverhost = location.host;
  if (begin > 0 && end > 0) {
    $rootScope.serverhost = url.substring(begin+1, end)
  }

  })
.value('froalaConfig', {
  inlineMode: false,
  placeholder: "欢迎加入我们的测试大家庭"
})
.config(['lazyImgConfigProvider', function(lazyImgConfigProvider){
    lazyImgConfigProvider.setOptions({
      offset: 200,
      onError: function(image) {
        if (image.$elem.attr('no-fallback')) return;
        image.$elem.attr('src', angular.element('#applogo').attr('src') + '?' + +new Date);
      },
      onSuccess: function(image) {
        image.$elem.hide().fadeIn();
      }
    });
  }])
.config(function(LightboxProvider) {
  LightboxProvider.templateUrl = 'views/lightbox.html';
  LightboxProvider.getImageUrl = function(url) { return url; };
  LightboxProvider.calculateModalDimensions = function (dimensions) {
    // 400px = arbitrary min width
    // 32px = 2 * (1px border of .modal-content
    //             + 15px padding of .modal-body)
    var width = Math.max(400, dimensions.imageDisplayWidth + 32);

    // 200px = arbitrary min height
    // 66px = 32px as above
    //        + 34px outer height of .lightbox-nav
    var height = Math.max(200, dimensions.imageDisplayHeight + 66);

    // first case:  the modal width cannot be larger than the window width
    //              20px = arbitrary value larger than the vertical scrollbar
    //                     width in order to avoid having a horizontal scrollbar
    // second case: Bootstrap modals are not centered below 768px
    if (width >= dimensions.windowWidth - 20 || dimensions.windowWidth < 768) {
      width = 'auto';
    }

    // the modal height cannot be larger than the window height
    if (height >= dimensions.windowHeight) {
      height = 'auto';
    }

    return {
      'width': width,
      'height': height + 40
    };
  };
});
