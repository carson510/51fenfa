'use strict';

angular.module('takoApp')
.directive('fallbackSrc', function() {
  return {
    link: function(scope, element, attrs) {
      element.on('error', function() {
        attrs.$set('src', angular.element('#applogo').attr('src') + '?' + +new Date);
      });
    }
  }
});