'use strict';

angular.module('takoApp')
.controller('TesterConfirmCtrl', function($scope, $routeParams) {
  
  $scope.success = $routeParams.result == 'true';
  
});