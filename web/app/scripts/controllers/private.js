'use strict';

angular.module('takoApp')
.controller('privateCtrl', function($scope, $routeParams, $location) {
  $scope.goto = function (path) {
    $location.path(path);
  };

});
