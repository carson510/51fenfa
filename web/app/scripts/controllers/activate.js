'use strict';

angular.module('takoApp')
.controller('ActivateCtrl', function($scope, $routeParams, Registration) {
  
  $scope.email = $routeParams.email;
  $scope.success = $routeParams.result === 'true';
  
  $scope.resend = function() { Registration.resend($scope.email); };
  
});