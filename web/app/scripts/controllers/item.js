'use strict';

angular.module('takoApp')
.controller('ItemCtrl', function($rootScope, $scope, $route, $http, $timeout, $q, $sce, $modal, $location, $routeParams,$cookies, log, api,toaster, session, Lightbox) {

  if (!session.loggedIn()) return $location.path('/signin');

 $scope.host = location.host;

  getAppInfo();

  $scope.active_tab = 'dlog';
  $scope.set_active_tab = function(tab) {
    $scope.active_tab = tab;
    if (tab == 'dlog' && $scope.dlogs.length == 0) return getAppDlog();
  };

  $scope.selectMemberMode = false;
  $scope.toggleSelectMode = function(type) {
    if (type === 'member') $scope.selectMemberMode = !$scope.selectMemberMode;
    if (type === 'invite') $scope.selectInviteMode = !$scope.selectInviteMode;
    if (type === 'tester') $scope.selectTesterMode = !$scope.selectTesterMode;
  };
  $scope.removingMembers = false;
  $scope.removeMembers = function() {
    if ($scope.removingMembers) return;

    var memberids = _.reduce($scope.members, function(ids, member) {
      if (member.selected) ids.push(member.id);
      return ids;
    }, []);

    if (memberids.length === 0) return toaster.pop('error', '未选中成员', '请点击成员进行选择');

    $scope.removingMembers = true;
    $http.delete(api.removemember, {
      'Content-Type': 'applicaiton/json',
      data: {
        appid: $routeParams.id,
        memberids: memberids
      }
    })
    .success(function(result) {
      if (result.ret != 0) return toaster.pop('error', '删除失败', result.message);
      $scope.members = _.reject($scope.members, function(member) { return member.selected; });
      $scope.selectMemberMode = false;
      toaster.pop('success', '删除成功', result.message);
    })
    .finally(function() {
      $scope.removingMembers = false;
    })
  }

  $scope.selectInviteMode = false;
  $scope.removingInviteds = false;
  $scope.removeInviteds = function() {
    if ($scope.removingInviteds) return;

    var ids = _.reduce($scope.inviteds, function(memo, invited) {
      if (invited.selected) memo.push(invited.id);
      return memo;
    }, []);

    if (ids.length === 0) return toaster.pop('error', '未选中成员', '请点击成员进行选择');

    $scope.removingInviteds = true;
    $http.delete(api.removeInvite, {
      'Content-Type': 'applicaiton/json',
      data: { ids: ids }
    })
    .success(function(result) {
      if (result.ret != 0) return toaster.pop('error', '取消邀请失败', result.message);
      $scope.inviteds = _.reject($scope.inviteds, function(invited) { return invited.selected; });
      $scope.selectInviteMode = false;
      toaster.pop('success', '取消邀请成功', result.message);
    })
    .finally(function() {
      $scope.removingInviteds = false;
    })
  };

  //$scope.uploader = new FileUploader();
  //$scope.uploader.onAfterAddingFile = function(item) {
  //  var data = new FormData();
  //  data.append('uploadfile', item._file);
  //  $http.post("http://" + $rootScope.serverhost + api.upload + '?type=2&filename=' + item._file.name, data, {
  //    transformRequest: angular.identity,
  //    headers: { 'Content-Type': undefined }
  //  }).success(function(result) {
  //    if (result.ret != 0) return;
  //    $scope.app.logourlpath = result.data;
  //    updateAppInfo();
  //  })
  //};

  function logDownload(index, versionid) {
    log.download(versionid)
      .success(function(result) {
        if (result.ret != 0) return;
        $scope.versions[index].downloadcount += 1;
      });
  }

  $scope.releaseVersion = function(event, index) {
    event.preventDefault();
    var versionid = $scope.versions[index].id;
    $http.put(api.appVersionRelease + '/' + versionid, {}).success(function(result) {
      if (result.ret != 0) return;
      $route.reload();
    });
  }

  $scope.deleteVersion = function(event, index) {
    event.preventDefault();
    var versionid = $scope.versions[index].id;
    swal({
      title: '确认删除?',
      type: 'warning',
      showCancelButton: true,
      cancelButtonText: "取消",
      confirmButtonText: "确认"
    }, function(confirmed) {
      if (confirmed) {
        $http.delete(api.appVersionDelete + '/' + versionid, {}).success(function(result) {
          if (result.ret != 0) return;
          $scope.versions.splice(index, 1);
        });
      }
    });
  }

  $scope.versions = [];
  $scope.versionsCursor = 0;
  $scope.loadingVersions = false;

  $scope.feedbacks = [];
  $scope.feedbacksCursor = 0;
  $scope.loadingFeedbacks = false;

  $scope.section = 'members';
  $scope.toSection = function(section) { $scope.section = section; };

  $scope.toinvites = [{ email: undefined, usertype: 2 }];
  $scope.validateEmail = validateEmail;
  function validateEmail(email) { return /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i.test(email) };
  function validEmailCount(people)   { return _.filter(people, function(person) { return validateEmail(person.email); }).length; }
  function invalidEmailCount(people) { return _.filter(people, function(person) { return person.email && !validateEmail(person.email); }).length; }
  $scope.$watch(
    function() { return validEmailCount($scope.toinvites) > 0 && invalidEmailCount($scope.toinvites) == 0 },
    function(canSend) { $scope.canSend = canSend; }
  );

  // Mail editor options
  $scope.froalaOptions = {
    buttons : [
      "bold", "italic", "underline", "fontSize", "color", "align",
       "formatBlock", "insertOrderedList", "insertUnorderedList",
      "createLink", "undo", "redo"
    ]
  };
  $scope.editing_mail = false;
  $scope.toggleMailEditor = function() { $scope.editing_mail = !$scope.editing_mail; };
  $scope.saveMail = function() {
    $http.put(api.editMailMsg, {
      appid: $scope.app.id,
      mailmsg: $scope.app.mailmsg
    }).success(function(result) {
      if (result.ret != 0) return;
      toaster.pop('success', '保存邮件成功');
      $scope.editing_mail = false;
    });
  };

  $scope.selectTesterMode = false;
  $scope.new_testers = [{ email: '' }];
  $scope.all_testers = [];
  $scope.new_tester = function() { $scope.new_testers.push({ email: '' }); }
  $scope.del_tester = function(index) { $scope.new_testers.splice(index, 1); }
  $scope.$watch(
    function() { return validEmailCount($scope.new_testers) > 0 && invalidEmailCount($scope.new_testers) == 0 },
    function(can) { $scope.can_send_test_invitations = can; }
  );

  $scope.inviting_testers = false;
  $scope.invite_testers = function() {
    if ($scope.inviting_testers) return;
    var testers = _.filter($scope.new_testers, function(tester) { return validateEmail(tester.email); });
    if (testers.length === 0) return;
    $scope.inviting_testers = true;
    $http.post(api.inviteTester, {
      appid: $routeParams.id,
      members: testers
    }).success(function(result) {
      if (result.ret == 1) return toaster.pop('error', '被邀请人已经是测试人员');
      if (result.ret != 0) return toaster.pop('error', '发送邀请邮件失败', result.message);
      $scope.new_testers = [{ email: '' }];
      $scope.getTesters();
      toaster.pop('success', '已发送邀请邮件');
    }).finally(function() {
      $scope.inviting_testers = false;
    });
  };

  $scope.add = function() { $scope.toinvites.push({ email: undefined, usertype: 2 }); }
  $scope.remove = function(index) { $scope.toinvites.splice(index, 1); };
  $scope.invite = invite;
  $scope.settings = settings;
  $scope.getMembers = getMembers;
  $scope.getAllMembers = getAllMembers;
  $scope.getInvitedMembers = getInvitedMembers;
  $scope.getTesters = getTesters;
  $scope.getFeedbacks = getFeedbacks;
  $scope.validateUri = validateUri;
  $scope.updateAppInfo = updateAppInfo;
  $scope.getAppVersions = getAppVersions;
  $scope.getAppDlog = getAppDlog;
  $scope.viewImage = function(findex, index) {
    Lightbox.openModal($scope.feedbacks[findex].imgurls, index);
  };

  $scope.newFeedback = function(app, event) {
    if (event) event.stopPropagation();
    var modal = $modal.open({
      animation: true,
      templateUrl: 'views/feedback.html',
      controller: 'FeedbackCtrl',
      resolve: {
        app: function() { return app; }
      }
    });
  };

  $scope.showCommentBox = function (feedback, index) {
    $scope.feedbacks[index].hideCommentList = !$scope.feedbacks[index].hideCommentList;
  };
  var userCookies=$cookies.getAll();
  $scope.myUserid = userCookies.userid;
  $scope.cancelReply = function(feedback, index) {
    $scope.feedbacks[index].replying = false;
    $('.feedback').eq(index).find('textarea').attr("placeholder",'');
    $scope.feedbacks[index].replyTargetid = '';
  };
  $scope.reply = function (comment, findex, index) {
    $('.feedback').eq(findex).find('textarea').focus().attr("placeholder",'回复'+$scope.feedbacks[findex].comments[index]['username']);
    $scope.feedbacks[findex].replyTargetid = $scope.feedbacks[findex].comments[index]['userid'];
    $scope.feedbacks[findex].replying = true;
  }
  $scope.comfirmDelete = function(feedback, comment, findex, index){
    $http.delete(api.deleteComment, {
      'Content-Type': 'applicaiton/json',
      data: { id: comment.id,
        feedbackid: feedback.id
      }
    }).success(function(result){
      $http.get(api.getFeedbackInfo + feedback.id).then(function(data) {
        toaster.pop('success', '提示', '删除成功');
        $scope.feedbacks[findex].comments = data.data.data.comments;
        $scope.feedbacks[findex].hideCommentList = false;
      });
    });
  };
  $scope.closeDelete = function(feedback, comment, findex, index){
    $scope.feedbacks[findex].comments[index].deleteComment = false;
  };
  $scope.deleteComment = function(feedback, comment, findex, index) {
    $scope.feedbacks[findex].comments[index].deleteComment = true;
  };

  $scope.thumbsUp = function (feedback, index, bShowList) {

    if(feedback.liked){
      $http.delete(api.deleteLike + feedback.id).success(function(result){
        $http.get(api.getFeedbackInfo + feedback.id).then(function(data) {
          $scope.feedbacks[index].liked = data.data.data.liked;
          $scope.feedbacks[index].likers = data.data.data.likers;
          $scope.feedbacks[index].hideCommentList = bShowList;
        });
      })
    }else{
      $http.post(api.feedbackLike, {
        feedbackid: feedback.id
      }).success(function(result){
        $http.get(api.getFeedbackInfo + feedback.id).then(function(data) {
          $scope.feedbacks[index].liked = data.data.data.liked;
          $scope.feedbacks[index].likers = data.data.data.likers;
          $scope.feedbacks[index].hideCommentList = bShowList;
        });
      })
    }

  };
  // $scope.hideCommentBox = function (feedback, event) {
  //   console.log(event);
  //   feedback.showComment = false;
  // };
  $scope.releaseComment = function (feedback, index) {
    var comment={};
    if($scope.feedbacks[index].replyTargetid){
      comment = {
        "feedbackid": feedback.id,
        "targetid": $scope.feedbacks[index].replyTargetid ,
        "comment": feedback.newcomment
      }
    }else{
      comment = {
        "feedbackid": feedback.id,
        "comment": feedback.newcomment
      }
    }
    $http.post(api.feedbackComment, comment).success(function(result) {
      $http.get(api.getFeedbackInfo + feedback.id).then(function(data) {
          $scope.feedbacks[index].newcomment = '';
          $scope.feedbacks[index].replying = false;
          $scope.feedbacks[index].comments = data.data.data.comments;

      });
      // $scope.showComment = false;
      toaster.pop('success', '提示','评论成功');
    });
  };
  $scope.$on('feedback.created', function(event, feedback) {
    $scope.feedbacks.unshift(feedback);
    $scope.feedbacksCount += 1;
  });

  function settings() {
    $scope.app.newuri = $scope.app.uri;
  }

  // function queryFeedback(id){
  //   $http.get('/app/feedback/'+id).then()
  // }
  function getAppInfo() {
    $http.get(api.getAppInfo + '?appid=' + $routeParams.id)
      .then(check)
      .then(function(result) {
        $scope.app = result.app;
        $scope.app.newuri = $scope.app.uri;
        $scope.canUpdateApp = $scope.app.newuri_is_valid = true;
      });
  };

  $scope.validating_uri = false;
  function validateUri() {
    if ($scope.validating_uri) return;
    if ($scope.app.newuri.length != 4) return $scope.app.newuri_is_valid = $scope.canUpdateApp = false;
    if ($scope.app.uri === $scope.app.newuri) return $scope.app.newuri_is_valid = $scope.canUpdateApp = true;

    $scope.validating_uri = true;
    $http.get(api.isValid, {
      params: {
        appid: $routeParams.id,
        appuri: $scope.app.newuri
      }
    }).success(function(result) {
      $scope.app.newuri_is_valid = $scope.canUpdateApp = (result.ret == 0);
    }).finally(function() {
      $scope.validating_uri = false;
    });
  }

  function updateAppInfo() {
    $http.post(api.modifyappinfo, {
      appid:         $routeParams.id,
      appname:       $scope.app.appname,
      appdesc:       $scope.app.appdesc,
      logourl:       $scope.app.logourlpath,
      password:      $scope.app.password,
      releasenote:   $scope.app.releaseversion.releasenote
    }).success(function(result) {
      getAppInfo();
      if (result.ret != 0) toaster.pop('error', '修改失败', '修改应用设置失败');
      else toaster.pop('success', '修改成功', '修改应用设置成功');
    });
  }

  function getAppVersions() {
    if ($scope.active_tab != 'general' || $scope.loadingVersions || $scope.versionsCount == 0) return;
    if ($scope.versionsCount && ($scope.versionsCount == $scope.versionsCursor)) return;
    $scope.loadingVersions = true;
    $http.get(api.getappversions + '?appid=' + $routeParams.id, { params: { count: 20, cursor: $scope.versionsCursor } })
      .then(check)
      .then(function(result) {
        $scope.versions.push.apply($scope.versions, result.appversions);
        $scope.versionsCount = result.allrows;
        $scope.versionsCursor = result.nextcursor;
      })
      .finally(function() {
        $scope.loadingVersions = false;
      });
  }

  function getFeedbacks() {
    if ($scope.active_tab != 'feedbacks' || $scope.loadingFeedbacks || $scope.feedbacksCount == 0) return;
    if ($scope.feedbacksCount && ($scope.feedbacksCount == $scope.feedbacksCursor)) return;
    $scope.loadingFeedbacks = true;
    $http.get(api.getFeedbacks + '?appid=' + $routeParams.id, { params: { count: 10, cursor: $scope.feedbacksCursor } })
      .then(check)
      .then(function(result) {
        _.each(result.feedbacks, getAudioUrl);
        $scope.feedbacks.push.apply($scope.feedbacks, result.feedbacks);
        $scope.feedbacksCount = result.allrows;
        $scope.feedbacksCursor = result.nextcursor;
      })
      .finally(function() {
        $scope.loadingFeedbacks = false;
      });
  }
  $scope.dlogs = [];
  $scope.dlog_cursor = 0;
  function getAppDlog() {
    if ($scope.active_tab != 'dlog' || $scope.loadingDlogs || $scope.dlog_count == 0) return;
    if ($scope.dlog_count && ($scope.dlog_count == $scope.dlog_cursor)) return;
    $scope.loadingDlogs = true;
    $http.get(api.getDLog + '?appid=' + $routeParams.id, { params: { "page.count": 20, "page.cursor": $scope.dlog_cursor }})
      .then(function(result) {
        $scope.dlogs.push.apply($scope.dlogs, result.data.data.list);
        $scope.dlog_count =  result.data.data.page.total;
        $scope.dlog_cursor = result.data.data.page.next;
      })
      .finally(function() {
        $scope.loadingDlogs = false;
      });
  }

  function getAudioUrl(feedback) {
    if (!feedback.voiceurl) return;
    $http.get(api.getAudioUrl + '?path=' + feedback.voiceurl)
      .success(function(result) {
        if (result.ret != 0) return;
        feedback.voice_file_url = $sce.trustAsResourceUrl(result.data);
      });
  }

  function getMembers() {
    getAllMembers();
    getInvitedMembers();
  }

  $scope.members = [];
  $scope.fetching_members = false;
  $scope.members_cursor = 0;
  function getAllMembers() {
    if ($scope.active_tab != 'members' || $scope.fetching_members || $scope.members_count == 0) return;
    if ($scope.members_count && ($scope.members_count == $scope.members_cursor)) return;
    $scope.fetching_members = true;
    $http.get(api.findallmember + '?appid=' + $routeParams.id, { params: { "page.count": 20, "page.cursor": $scope.members_cursor }})
      .then(check)
      .then(function(result) {
        $scope.members = _.sortBy($scope.members.concat(result.list), 'usertype');
        $scope.members_cursor = result.page.next;
        $scope.members_count = result.page.total;
      })
      .finally(function() { $scope.fetching_members = false; });
  }

  function getInvitedMembers() {
    $http.get(api.getinvitedmember + '?appid=' + $routeParams.id)
      .then(check)
      .then(function(result) { $scope.inviteds = result; });
  }

  $scope.inviting = false;
  function invite(people, selected) {
    if ($scope.inviting) return;
    var users = _.filter(people, function(person) { return validateEmail(person.email); });
    if (selected) users = _.filter(users, function(user) { return user.selected; });
    if (users.length == 0) return toaster.pop('error', '未选中成员', '请点击成员进行选择');
    $scope.inviting = true;
    $http.post(api.addmember, {
      appid: $routeParams.id,
      users: users
    })
    .success(function(result) {
      if (result.ret != 0) return toaster.pop('error', '邀请失败', '请检查邮件格式是否正确');
      toaster.pop('success', '邀请成功', '已经发送邀请函');
      $scope.getMembers();
      $scope.section = 'members';
      $scope.toinvites = [{ email: '', usertype: 2 }];
    })
    .error(function() {
      toaster.pop('error', '邀请失败', '请检查邮件格式是否正确');
    })
    .finally(function() {
      $scope.inviting = false;
      $scope.selectInviteMode = false;
    });
  }

  $scope.all_testers = [];
  $scope.fetching_testers = false;
  $scope.testers_cursor = 0;
  function getTesters() {
    if ($scope.fetching_testers || $scope.testers_count == 0) return;
    if ($scope.testers_count && ($scope.testers_count == $scope.testers_cursor)) return;
    $scope.fetching_testers = true;
    $http.get(api.getTesters + '?appid=' + $routeParams.id, { params: { "page.count": 50, "page.cursor": $scope.testers_cursor }})
      .then(check)
      .then(function(result) {
        $scope.all_testers.push.apply($scope.all_testers, result.list);
        $scope.testers_cursor = result.page.next;
        $scope.testers_count = result.page.total;
      })
    .finally(function() { $scope.fetching_testers = false; });
  }

  function check(response) {
    if (response.data.ret != 0) return $q.reject(response.data);
    return response.data.data;
  }


});
