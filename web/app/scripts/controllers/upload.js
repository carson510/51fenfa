'use strict';

angular.module('takoApp')
.controller('UploadCtrl', function($rootScope, $scope, $mdDialog, $http, $q, $location, session, toaster, api, util) {

  window.__$uploadScope = $scope;

  if (!session.loggedIn()) return $location.path('/signin');

  $scope.parsed = false;
  $scope.parsing = false;
  $scope.uploading = false;

  $scope.cancel = cancel;
  $scope.submit = submit;

  $scope.$on('$destroy', function() { window.onbeforeunload = undefined; });
  // https://developer.mozilla.org/en-US/docs/Web/Events/beforeunload
  // window.onbeforeunload = function(e) {
  //   var message = "关闭窗口会中断上传，确定要关闭吗？";
  //   if (!$scope.uploading) return;
  //   e.returnValue = message;  // Gecko and Trident
  //   return message;           // Gecko and WebKit
  // }

  $scope.showAddingGroup = function(ev) {
    // Appending dialog to document.body to cover sidenav in docs app
    var confirm = $mdDialog.prompt()
    .title('创建应用组')
    .placeholder('请输入应用组的名称')
    .ariaLabel('应用组')
    .targetEvent(ev)
    .ok('确认')
    .cancel('取消');
    $mdDialog.show(confirm).then(function(result) {
      $scope.addGroup(result)
    }, function() {
    });
  };

  $scope.addGroup = function (name) {
    if (name != "") {
      $http.post(api.addGroup + "?name=" + name)
      .then(util.check)
      .then(function (data) {
        getGroups();
      });
    }
  };

  function getGroups() {
    $http.get(api.getGroups)
    .then(util.check)
    .then(function(data) {
      $scope.appgroups = data;
      if (data && data.length > 0) {
        $scope.app.groupid = data[0].id;
      }
    });
  };

  function initApp() {
    $scope.app = { public: false, note: '' };
  };

  $http.get(api.storage_default_get)
  .then(util.check)
  .then(function(storage) {
    initUploader(storage);
  });

  function initUploader(storage) {
    $scope.storage = storage;
    if (storage.type == "qiniu") {
      $scope.uploader = Qiniu.uploader({
        runtimes: 'html5,flash,html4',      // 上传模式,依次退化
        browse_button: 'pickfiles',         // 上传选择的点选按钮，**必需**
        uptoken : storage.token, // uptoken 是上传凭证，由其他程序生成
        get_new_uptoken: false,             // 设置上传文件的时候是否每次都重新获取新的 uptoken
        domain: storage.bucket,     // bucket 域名，下载资源时用到，**必需**
        //container: 'dropzone',             // 上传区域 DOM ID，默认是 browser_button 的父元素，
        flash_swf_url: 'scripts/vendor/plupload/Moxie.swf',  //引入 flash,相对路径
        silverlight_xap_url: 'scripts/vendor/plupload/Moxie.xap',
        max_retries: 3,                     // 上传失败最大重试次数
        dragdrop: true,                     // 开启可拖曳上传
        drop_element: 'dropzone',          // 拖曳上传区域元素的 ID，拖曳文件或文件夹后可触发上传
        //chunk_size: '4mb',                  // 分块上传时，每块的体积
        auto_start: false,                   // 选择文件后自动上传，若关闭需要自己绑定事件触发上传,
        multi_selection: false,
        //filters : [
        //  {title : "App files", extensions : "apk,ipa"},
        //  {title : "Image files", extensions : "jpg,jpeg,gif,png"}
        //],
        init: {
          FilesAdded: function (up, files) {
            plupload.each(files, function(file) {
              onFileAdded(file);
            });
          },
          BeforeUpload: function (up, file) {
            up.settings.url = "http://" + storage.uploaddomain;
          },
          UploadProgress: function (up, file) {
            $scope.progress = up.total.percent;
            $scope.$apply();
          },
          FileUploaded: function (up, file, info) {
            onFileUploaded(up, file, info)
          },
          Error: function (up, err, errTip) {
            toaster.pop('error', '上传失败', errTip);
            //up.stop();
          },
          UploadComplete: function (up, files) {
            onUploadComplete(up, files);
          },
          Key: function(up, file) {
            var key;
            if (isAPK(file.name) || isIPA(file.name)) {
              key = $scope.app.packageurl = $scope.storage.key + "/" + uuid.v4() + getExtension(file.name);
            } else {
              key = $scope.app.logourl = $scope.storage.key + "/" + uuid.v4() + getExtension(file.name);
            }
            return key;
          }
        }
      });
    } else if (storage.type == "fileserver") {
      getDiskUsage();
      $scope.uploader = new plupload.Uploader({
        url : "http://" + storage.uploaddomain + api.upload,
        browse_button: 'pickfiles',
        drop_element: 'dropzone',
        //file_data_name: 'uploadfile',
        multi_selection: false,
        flash_swf_url: 'scripts/vendor/plupload/Moxie.swf',
        silverlight_xap_url: 'scripts/vendor/plupload/Moxie.xap',
        //chunk_size: '4mb',
        //filters : [
        //  {title : "App files", extensions : "apk,ipa"},
        //  {title : "Image files", extensions : "jpg,jpeg,gif,png"}
        //],
        init: {
          FilesAdded: function(up, files) {
            plupload.each(files, function(file) {
              onFileAdded(file);
            });
          },
          UploadProgress: function (up, file) {
            $scope.progress = up.total.percent;
            $scope.$apply();
          },
          FileUploaded: function (up, file, info) {
            onFileUploaded(up, file, info)
          },
          Error: function (up, err, errTip) {
            toaster.pop('error', '上传失败', errTip);
            //up.stop();
          },
          UploadComplete: function (up, files) {
            onUploadComplete(up, files);
          }
        }
      });
      $scope.uploader.init();
    } else if (storage.type == "ksyun") {
      $scope.uploader = new plupload.Uploader({
        url: "http://" + storage.uploaddomain + "/" + storage.bucket,
        browse_button: 'pickfiles',
        drop_element: 'dropzone',
        multi_selection: false,
        flash_swf_url: 'scripts/vendor/plupload/Moxie.swf',
        silverlight_xap_url: 'scripts/vendor/plupload/Moxie.xap',
        init: {
          FilesAdded: function(up, files) {
            plupload.each(files, function(file) {
              onFileAdded(file);
            });
          },
          BeforeUpload: function (up, file) {
            var key;
            if (isAPK(file.name) || isIPA(file.name)) {
              key = $scope.app.packageurl = $scope.storage.key + "/" + uuid.v4() + getExtension(file.name);
            } else {
              key = $scope.app.logourl = $scope.storage.key + "/" + uuid.v4() + getExtension(file.name);
            }
            up.setOption("multipart_params", {
              "acl": "public-read",
              "signature" : storage.signature,
              "KSSAccessKeyId": storage.accesskey,
              "policy": storage.policy,
              "key": key
            });
          },
          UploadProgress: function (up, file) {
            $scope.progress = up.total.percent;
            $scope.$apply();
          },
          FileUploaded: function (up, file, info) {
            onFileUploaded(up, file, info)
          },
          Error: function (up, err, errTip) {
            toaster.pop('error', '上传失败', errTip);
            //up.stop();
          },
          UploadComplete: function (up, files) {
            onUploadComplete(up, files);
          }
        }
      });
      $scope.uploader.init();
    } else if (storage.type == "aliyun") {
      $scope.uploader = new plupload.Uploader({
        url: "http://" + storage.uploaddomain,
        browse_button: 'pickfiles',
        drop_element: 'dropzone',
        multi_selection: false,
        flash_swf_url: 'scripts/vendor/plupload/Moxie.swf',
        silverlight_xap_url: 'scripts/vendor/plupload/Moxie.xap',
        init: {
          FilesAdded: function(up, files) {
            plupload.each(files, function(file) {
              onFileAdded(file);
            });
          },
          BeforeUpload: function (up, file) {
            var key;
            if (isAPK(file.name) || isIPA(file.name)) {
              key = $scope.app.packageurl = $scope.storage.key + "/" + uuid.v4() + getExtension(file.name);
            } else {
              key = $scope.app.logourl = $scope.storage.key + "/" + uuid.v4() + getExtension(file.name);
            }
            up.setOption("multipart_params", {
              'key' : key,
              'policy': storage.policy,
              'OSSAccessKeyId': storage.accesskey,
              'success_action_status' : '200', //让服务端返回200,不然，默认会返回204
              'signature': storage.signature
            });
          },
          UploadProgress: function (up, file) {
            $scope.progress = up.total.percent;
            $scope.$apply();
          },
          FileUploaded: function (up, file, info) {
            onFileUploaded(up, file, info)
          },
          Error: function (up, err, errTip) {
            toaster.pop('error', '上传失败', errTip);
            //up.stop();
          },
          UploadComplete: function (up, files) {
            onUploadComplete(up, files);
          }
        }
      });
      $scope.uploader.init();
    }
  };

  function getDiskUsage() {
    $http.get("http://" + $scope.storage.uploaddomain + api.diskusage)
        .then(util.check)
        .then(function(data) {
          $scope.diskusage = data;
        });
  }

  function onFileAdded(file) {
    if ($scope.addlogo) {
      $scope.addlogo = false;
      return;
    }
    if (!isIPA(file.name) && !isAPK(file.name)) {
      clearQueue();
      return swal('上传错误', '只支持上传 iOS(.ipa) 和 Android(.apk) 游戏包', 'error');
    }
    initApp();
    $scope.file = fileinfo(file.name);
    $scope.parsing = true;
    $scope.app.size =file.size;
    var reader = new FileReader();
    reader.onload = onload;
    reader.readAsArrayBuffer(file.getNative());
  };

  function onload(e) {
    var zip = new JSZip(e.target.result);
    $scope.zip = zip;
    var infoplist = zip.file(/^(([^\\\?\/\*\|<>:"]+\/){2})Info\.plist/);
    var resoucesArsc = zip.file(/resources.arsc/);
    var androidManifest = zip.file(/AndroidManifest.xml/);
    parse(infoplist, resoucesArsc, androidManifest);
  }

  function parse(infoplist, resoucesArsc, androidManifest) {
    var data;
    // iOS
    if ($scope.file.platform === 1) {
      if (infoplist.length == 0) {
        clearQueue();
        $scope.parsing = false;
        showError();
        return;
      }
      data = {
        platform: $scope.file.platform,
        plist: infoplist.length && base64encode(infoplist[0].asUint8Array())
      }
    }

    // Android
    if ($scope.file.platform === 2) {
      if (androidManifest.length == 0) {
        $scope.parsed = false;
        showError('解析失败');
        return;
      }
      data = {
        platform: $scope.file.platform,
        arsc: resoucesArsc.length && base64encode(resoucesArsc[0].asUint8Array()),
        manifest: androidManifest && base64encode(androidManifest[0].asUint8Array())
      }
    }

    $http.post(api.parseapp, data)
    .then(function(response) {
      if (response.data.ret == 0) {
        _.extend($scope.app, response.data.data || {});
        $scope.parsed = true;
        previewLogo();
        getGroups();
      } else if (response.data.ret == 31) {
        showError('用户已过期');
      } else {
        showError('解析失败')
      }
    })
    .finally(function() { $scope.parsing = false; });
  }

  function previewLogo() {
    var logo = $scope.zip.file(new RegExp($scope.app.logo));
    if (!logo.length) return;
    $scope.app.logo = logo[0].name;
    $scope.app.logotype = logotype($scope.app.logo);
    $scope.app.logodata = logo[0].asUint8Array();
    $scope.app.logoDataUrl = 'data:' + $scope.app.logotype + ';base64,' + base64encode($scope.app.logodata);
    // http://stackoverflow.com/questions/13950865/javascript-render-png-stored-as-uint8array-onto-canvas-element-without-data-uri
  }

  function onFileUploaded(up, file, info) {
    var strJson;
    if (typeof info == "string") {
      strJson = info;
    } else {
      strJson = info.response;
    }
    var result = JSON.parse(strJson)
    if (result != null && result.ret && result.ret != 0) {
      $scope.uploading = false;
      //up.stop();
      up.total.uploaded -= 1;
      up.total.failed += 1;
      toaster.pop('error', '上传失败', result.message);
    } else {
        if ($scope.storage.type == "fileserver") {
          if (isAPK(file.name) || isIPA(file.name)) {
             $scope.app.packageurl = result.data;
          } else {
            $scope.app.logourl = result.data;
          }
        }
    }
  };

  function onUploadComplete(up, files) {
    if (up.total.failed > 0) {
      $scope.uploading = false;
      showError("上传失败");
      return
    }
    if (!$scope.app.packageurl) return;
    $http.post(api.addapp, {
      ispublic: $scope.app.public,
      release_note: $scope.app.note,
      groupid: $scope.app.groupid,
      storageid: $scope.storage.id,
      package: {
        url: $scope.app.packageurl,
        logo: $scope.app.logourl,
        size: $scope.app.size,
        appname: $scope.app.appname,
        package: $scope.app.package,
        bundleid: $scope.app.bundleid,
        version: $scope.app.version,
        platform: $scope.app.platform,
        buildnumber: $scope.app.buildnumber
      }
    }).success(function(result) {
      if (result.ret != 0) return showError('上传失败');
      $location.path('dash');
    }).error(function() {
      return showError('上传失败');
    });
  }

  function clearQueue() {
    $scope.uploader.splice(0, $scope.uploader.files.length);
  }

  function showError(title) {
    clearQueue();
    $scope.parsing = false;
    $scope.uploading = false;
    return swal(title, '请及时与管理联系, 给您带来的不便, 敬请谅解', 'error');
  }

  function cancel() {
    initApp();
    $scope.parsed = false;
    clearQueue();
  }

  function submit() {
    $scope.uploading = true;
    $scope.progress = 0;
    $scope.speed = 0;
    var file = new mOxie.File(null, $scope.app.logoDataUrl);
    file.name = $scope.app.logo; // you need to give it a name here (required)
    $scope.addlogo = true;
    $scope.uploader.addFile(file);
    //uploader.addFile(new File([new Blob([$scope.app.logodata.buffer], { type: $scope.app.logotype })], $scope.app.logo), $scope.app.logo);
    $scope.uploader.start();

    // var item = $scope.uploader.queue[0];
    // item.alias = 'uploadfile';
    // item.url = uploadUrl;
    // item.onSuccess = function(response, status, headers) {
    //   if (response.ret != 0)  {
    //     $scope.uploader.cancelAll();
    //     showError('上传失败安装包失败');
    //   } else {
    //     $scope.app.packageurl = response.data;
    //   }
    // }
    // item.onError = function(response, status, headers) {
    //     $scope.uploader.cancelAll();
    //     showError('上传失败安装包失败');
    // }
    // // Add a item to the queue
    // var logoitem = new FileUploader.FileItem(
    //   $scope.uploader,
    //   new File([new Blob([$scope.app.logodata.buffer], { type: $scope.app.logotype })], $scope.app.logo),
    //   { alias: "uploadfile", url : uploadUrl }
    // );
    // logoitem.onSuccess = function(response, status, headers) {
    //   if (response.ret != 0)  {
    //     showError('上传失败应用图标包失败');
    //   } else {
    //     $scope.app.logourl = response.data;
    //     create()
    //   }
    // }
    // logoitem.onError = function(response, status, headers) {
    //   showError('上传失败应用图标包失败');
    // }
    // $scope.uploader.queue.push(logoitem);

    // var totalSize = 0;
    // for (var i = 0; i < uploader.files.length; i++) {
    //   totalSize += uploader.files[i].size;
    // }
    // var preUploadedSize = 0;
    // var preUploadedTime = new Date();
    // $scope.uploader.onProgressAll = function(progress) {
    //     $scope.progress = progress;
    //     var uploadedSize = totalSize * progress / 100;
    //     var uploadedTime = new Date();
    //     var elapsed = (uploadedTime.getTime() - preUploadedTime.getTime());
    //     if (elapsed > 0) {
    //         var speed = (uploadedSize - preUploadedSize) * 1000.0 / elapsed;
    //         if (speed > 1024) {
    //             $scope.speed = speed;
    //             preUploadedSize = uploadedSize;
    //             preUploadedTime = uploadedTime;
    //         }
    //     }
    // }
    // $scope.uploader.onErrorItem = function(item, response, status, headers) {
    //   showError("上传失败")
    // }
    // $scope.uploader.uploadAll()
  }

  function getExtension(filename) { return filename.substring(filename.lastIndexOf(".")); }
  function isIPA(filename) { return /(\.|\/)(ipa)$/i.test(filename); }
  function isAPK(filename) { return /(\.|\/)(apk)$/i.test(filename); }
  function fileinfo(filename) {
    var result = {
      name: filename,
      isIPA: isIPA(filename),
      isAPK: isAPK(filename)
    };

    result.platform = (function() {
      if (this.isIPA) return 1;
      if (this.isAPK) return 2;
    }).call(result);

    result.uploadtype = (function() {
      if (this.isIPA) return 3;
      if (this.isAPK) return 4;
    }).call(result);

    return result;
  }

  function logotype(filename) {
    if (/(\.|\/)(png)$/i.test(filename)) return "image/png";
    if (/(\.|\/)(ico)$/i.test(filename)) return "image/x-icon";
    if (/(\.|\/)(gif)$/i.test(filename)) return "image/gif";
    if (/(\.|\/)(bmp)$/i.test(filename)) return "image/bmp";
    if (/(\.|\/)(jpeg|jpg|jpe)$/i.test(filename)) return "image/jpeg";
    return "image/png";
  }

  function base64encode(input) {
    var keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    var output = "";
    var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
    var i = 0;

    while (i < input.length) {
      chr1 = input[i++];
      chr2 = i < input.length ? input[i++] : Number.NaN; // Not sure if the index
      chr3 = i < input.length ? input[i++] : Number.NaN; // checks are needed here

      enc1 = chr1 >> 2;
      enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
      enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
      enc4 = chr3 & 63;

      if (isNaN(chr2)) {
        enc3 = enc4 = 64;
      } else if (isNaN(chr3)) {
        enc4 = 64;
      }

      output += keyStr.charAt(enc1) + keyStr.charAt(enc2) + keyStr.charAt(enc3) + keyStr.charAt(enc4);
    }
    return output;
  }

});