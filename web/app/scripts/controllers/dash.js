'use strict';

angular.module('takoApp')
.controller('DashCtrl', function($scope, $rootScope, $mdDialog, $http, $location, session, api, toaster, util) {
  window.__dash = $scope;
  if (!session.loggedIn()) return $location.path('/signin');

  $scope.apps = [];
  $scope.checkedApps = [];
  $scope.appsCursor = 0;
  $scope.loading = false;
  $scope.searchMyApps = searchMyApps;
  $scope.newSearchMyApps = newSearchMyApps;
  $scope.getMyApps = getMyApps;

  $scope.test_apps = []
  $scope.test_apps_cursor = 0;
  $scope.loading_test_apps = false;

  $scope.view = function(app) { $location.path('item/' + app.id); };

  $scope.appnameFilter = function(app) {
    if ($scope.appname === '') return true;
    var target = new RegExp($scope.appname, 'i');
    return app.appname.search(target) > -1 || (app.pinyin && app.pinyin.search(target) > -1);
  }

  $scope.platformFilter = function(app) {
    if ($scope.platform === 0) return true;
    return $scope.platform === app.platform;
  };

  function searchMyApps() {
    if ($scope.appgroups) {
      return getMyApps();
    }
    $http.get(api.getGroups)
      .then(check)
      .then(function(data) {
        $scope.appgroups = data;
        if (data && data.length > 0) {
          $scope.groupid = data[0].id;
          $scope.move2groupid = data[0].id;
        }
        getMyApps();
      });
  };

  function newSearchMyApps() {
    $scope.apps = [];
    $scope.appsCursor = 0;
    $scope.appsCount = -1;
    getMyApps();
  }

  function getMyApps() {
    if ($scope.loading) return;
    if ($scope.appsCount && ($scope.appsCount == $scope.appsCursor)) return;
    $scope.loading = true;
    $http.get(api.getmyapps, { params: { count: 20, cursor: $scope.appsCursor, gid: $scope.groupid } })
      .success(function(result) {
        if (result.ret == 24) return $location.path('/signin');
        if (result.ret != 0) return;
        $scope.appsCount = result.data.allrows;
        $scope.appsCursor = result.data.nextcursor;
        $scope.apps.push.apply($scope.apps, result.data.apps);
      })
      .finally(function() {
        $scope.loading = false;
      });
  }

  $scope.toEditApp = function(event, index) {
    $scope.editingapp = $scope.apps[index];
    $scope.editingapp.appname_new = $scope.editingapp.appname;
    $scope.editing = true;
  };

  $scope.editApp = function() {
    if ($scope.editingapp.appname_new == $scope.editingapp.appname) {
      $scope.editing = false;
      return
    }
    $http.post(api.modifyappinfo, {
      appid:         $scope.editingapp.id,
      appname:       $scope.editingapp.appname_new
      //appdesc:       $scope.app.appdesc,
      //logourl:       $scope.app.logourlpath,
      //password:      $scope.app.password,
      //releasenote:   $scope.app.releaseversion.releasenote
    }).success(function(result) {
      if (result.ret != 0) {
        toaster.pop('error', '修改失败', '修改名称失败');
      } else {
        $scope.editing = false;
        $scope.editingapp.appname = $scope.editingapp.appname_new;
        toaster.pop('success', '修改成功', '修改名称成功');
      }
    });
  };

  $scope.checkedAll = function(checked) {
    angular.forEach($scope.apps, function(app) {
      if (app.checked != checked) {
        app.checked = checked;
      }
    });
    $scope.onChecked();
  };

  $scope.onChecked = function(){
    var text = ""
    var isCheckedAll = true;
    for(var i=0 ; i < $scope.apps.length; i++) {
      if(!$scope.apps[i].checked) {
        isCheckedAll = false;
      } else {
        text += $scope.apps[i].appname + " |http://" + $rootScope.serverhost + "/"+ $scope.apps[i].uri + "|\n";
      }
    }
    if (isCheckedAll) {
      $scope.chkall = true;
    } else {
      $scope.chkall = false;
    }
    document.getElementById("uricopier").setAttribute("data-clipboard-text", text);
  };

  $scope.moveGroup = function(groupid) {
    var appids = "";
    for(var i=0 ; i < $scope.apps.length; i++) {
      if($scope.apps[i].checked) {
        appids += $scope.apps[i].id + ","
      }
    }
    if (appids.length > 0) {
      $http.put(api.moveGroups + '?appids=' + appids + '&targetgroupid=' + groupid, {}).success(function(result) {
        $scope.movingGroup = false;
        if (result.ret != 0) return;
        newSearchMyApps();
      });
    }
  }

  $scope.deleteApps = function(event) {
    event.preventDefault();
    var checkedApps = $scope.checkedApps;
    swal({
      title: '确认永久删除选中应用?',
      type: 'warning',
      showCancelButton: true,
      cancelButtonText: "取消",
      confirmButtonText: "确认"
    }, function(confirmed) {
      if (confirmed) {
        var appids = "";
        for(var i=0 ; i < $scope.apps.length; i++) {
          if($scope.apps[i].checked) {
            appids += $scope.apps[i].id + ","
          }
        }
        if (appids.length > 0) {
          $http.delete(api.deleteApps + '?appids=' + appids, {}).then(util.check).then(function(data) {
            newSearchMyApps();
          });
        }
      }
    });
  }

  function check(response) {
    if (response.data.ret != 0) return $q.reject(response.data);
    return response.data.data;
  }

  var clipboard = new Clipboard('.uricopier');
  clipboard.on('success', function(e) {
    //console.info('Action:', e.action);
    //console.info('Text:', e.text);
    //console.info('Trigger:', e.trigger);
    e.clearSelection();
  });

  clipboard.on('error', function(e) {
    //console.error('Action:', e.action);
    //console.error('Trigger:', e.trigger);
  });
});
