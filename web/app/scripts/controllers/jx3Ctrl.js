'use strict';

angular.module('takoApp')
.controller('jx3Ctrl', function($window) {
  angular.element('#jx3AppClassify').css('min-height', angular.element(window).height());
  angular.element($window).on('resize', function() {
    angular.element('#jx3AppClassify').css('min-height', angular.element(window).height());
  }).trigger('resize');
});
