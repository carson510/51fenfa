'use strict';

angular.module('takoApp')
.controller('UploadsCtrl', function($rootScope, $scope, $mdDialog, $http, $q, $location, session, toaster, api, util) {

  window.__$uploadScope = $scope;

  if (!session.loggedIn()) return $location.path('/signin');

  //console.log(uuid.v4())

  $scope.parsed = false;
  $scope.parsing = false;
  $scope.uploading = false;

  $scope.cancel = cancel;
  $scope.submit = submit;
  $scope.removeApp = removeApp;

  $scope.$on('$destroy', function() { window.onbeforeunload = undefined; });

  $scope.showAddingGroup = function(ev) {
    // Appending dialog to document.body to cover sidenav in docs app
    var confirm = $mdDialog.prompt()
    .title('创建应用组')
    .placeholder('请输入应用组的名称')
    .ariaLabel('应用组')
    .targetEvent(ev)
    .ok('确认')
    .cancel('取消');
    $mdDialog.show(confirm).then(function(result) {
      $scope.addGroup(result)
    }, function() {
    });
  };

  $scope.addGroup = function (name) {
    if (name != "") {
      $http.post(api.addGroup + "?name=" + name)
      .then(util.check)
      .then(function (data) {
        getGroups();
      });
    }
  };

  function getGroups() {
    $http.get(api.getGroups)
    .then(util.check)
    .then(function(data) {
      $scope.appgroups = data;
      if (data && data.length > 0) {
        $scope.appgroupid = data[0].id;
      }
    });
  };

  function initApp() {
    $scope.apps = [];
    $scope.addlogo = false;
    $scope.parsedCount = 0;
    $scope.fileCount = 0;
    $scope.progress = 0;
  };

  getGroups();
  initApp();

  $http.get(api.storage_default_get)
  .then(util.check)
  .then(function(storage) {
    initUploader(storage);
  });

  function initUploader(storage) {
    $scope.storage = storage;
    if (storage.type == "fileserver") {
      getDiskUsage();
      $scope.uploader = new plupload.Uploader({
        url : "http://" + storage.uploaddomain + api.upload,
        browse_button: 'pickfiles',
        drop_element: 'dropzone',
        flash_swf_url: 'scripts/vendor/plupload/Moxie.swf',
        silverlight_xap_url: 'scripts/vendor/plupload/Moxie.xap',
        init: {
          FilesAdded: function(up, files) {
            plupload.each(files, function(file) {
              onFileAdded(file);
            });
          },
          UploadProgress: function (up, file) {
            $scope.progress = up.total.percent;
            $scope.$apply();
          },
          FileUploaded: function (up, file, info) {
            onFileUploaded(up, file, info)
          },
          Error: function (up, err) {
            if (err.file.app) {
              err.file.app.isError = true;
            }
          },
          UploadComplete: function (up, files) {
            onUploadComplete(up, files);
          }
        }
      });
      $scope.uploader.init();
    } else if (storage.type == "qiniu") {
      $scope.uploader = Qiniu.uploader({
        runtimes: 'html5,flash,html4',      // 上传模式,依次退化
        browse_button: 'pickfiles',         // 上传选择的点选按钮，**必需**
        uptoken : storage.token, // uptoken 是上传凭证，由其他程序生成
        get_new_uptoken: false,             // 设置上传文件的时候是否每次都重新获取新的 uptoken
        domain: storage.bucket,     // bucket 域名，下载资源时用到，**必需**
        //container: 'dropzone',             // 上传区域 DOM ID，默认是 browser_button 的父元素，
        flash_swf_url: 'scripts/vendor/plupload/Moxie.swf',  //引入 flash,相对路径
        silverlight_xap_url: 'scripts/vendor/plupload/Moxie.xap',
        max_retries: 3,                     // 上传失败最大重试次数
        dragdrop: true,                     // 开启可拖曳上传
        drop_element: 'dropzone',          // 拖曳上传区域元素的 ID，拖曳文件或文件夹后可触发上传
        //chunk_size: '4mb',                  // 分块上传时，每块的体积
        auto_start: false,                   // 选择文件后自动上传，若关闭需要自己绑定事件触发上传,
        //filters : [
        //  {title : "App files", extensions : "apk,ipa"},
        //  {title : "Image files", extensions : "jpg,jpeg,gif,png"}
        //],
        init: {
          FilesAdded: function (up, files) {
            plupload.each(files, function(file) {
              onFileAdded(file);
            });
          },
          BeforeUpload: function (up, file) {
            up.settings.url = "http://" + storage.uploaddomain;
          },
          UploadProgress: function (up, file) {
            $scope.progress = up.total.percent;
            $scope.$apply();
          },
          FileUploaded: function (up, file, info) {
            onFileUploaded(up, file, info)
          },
          Error: function (up, err) {
            if (err.file.app) {
              err.file.app.isError = true;
            }
          },
          UploadComplete: function (up, files) {
            onUploadComplete(up, files);
          },
          Key: function(up, file) {
            var key = "";
            if (isAPK(file.name) || isIPA(file.name)) {
              key = file.app.packageurl = $scope.storage.key + "/" + uuid.v4() + getExtension(file.name);
            } else {
              key = file.app.logourl = $scope.storage.key + "/" + uuid.v4() + getExtension(file.name);
            }
            return key;
          }
        }
      });
    } else if (storage.type == "ksyun") {
      $scope.uploader = new plupload.Uploader({
        url: "http://" + storage.uploaddomain + "/" + storage.bucket,
        browse_button: 'pickfiles',
        drop_element: 'dropzone',
        flash_swf_url: 'scripts/vendor/plupload/Moxie.swf',
        silverlight_xap_url: 'scripts/vendor/plupload/Moxie.xap',
        init: {
          FilesAdded: function(up, files) {
            plupload.each(files, function(file) {
              onFileAdded(file);
            });
          },
          BeforeUpload: function (up, file) {
            var key = "";
            if (isAPK(file.name) || isIPA(file.name)) {
              key = file.app.packageurl = $scope.storage.key + "/" + uuid.v4() + getExtension(file.name);
            } else {
              key = file.app.logourl = $scope.storage.key + "/" + uuid.v4() + getExtension(file.name);
            }
            up.setOption("multipart_params", {
              "acl": "public-read",
              "signature" : storage.signature,
              "KSSAccessKeyId": storage.accesskey,
              "policy": storage.policy,
              "key": key
            });
          },
          UploadProgress: function (up, file) {
            $scope.progress = up.total.percent;
            $scope.$apply();
          },
          FileUploaded: function (up, file, info) {
            onFileUploaded(up, file, info)
          },
          Error: function (up, err) {
            if (err.file.app) {
              err.file.app.isError = true;
            }
          },
          UploadComplete: function (up, files) {
            onUploadComplete(up, files);
          }
        }
      });
      $scope.uploader.init();
    } else if (storage.type == "aliyun") {
      $scope.uploader = new plupload.Uploader({
        url: "http://" + storage.uploaddomain,
        browse_button: 'pickfiles',
        drop_element: 'dropzone',
        flash_swf_url: 'scripts/vendor/plupload/Moxie.swf',
        silverlight_xap_url: 'scripts/vendor/plupload/Moxie.xap',
        init: {
          FilesAdded: function(up, files) {
            plupload.each(files, function(file) {
              onFileAdded(file);
            });
          },
          BeforeUpload: function (up, file) {
            var key = "";
            if (isAPK(file.name) || isIPA(file.name)) {
              key = file.app.packageurl = $scope.storage.key + "/" + uuid.v4() + getExtension(file.name);
            } else {
              key = file.app.logourl = $scope.storage.key + "/" + uuid.v4() + getExtension(file.name);
            }
            up.setOption("multipart_params", {
              'key' : key,
              'policy': storage.policy,
              'OSSAccessKeyId': storage.accesskey,
              'success_action_status' : '200', //让服务端返回200,不然，默认会返回204
              'signature': storage.signature
            });
          },
          UploadProgress: function (up, file) {
            $scope.progress = up.total.percent;
            $scope.$apply();
          },
          FileUploaded: function (up, file, info) {
            onFileUploaded(up, file, info)
          },
          Error: function (up, err) {
            if (err.file.app) {
              err.file.app.isError = true;
            }
          },
          UploadComplete: function (up, files) {
            onUploadComplete(up, files);
          }
        }
      });
      $scope.uploader.init();
    }
  };

  function getDiskUsage() {
    $http.get("http://" + $scope.storage.uploaddomain + api.diskusage)
        .then(util.check)
        .then(function(data) {
          $scope.diskusage = data;
        });
  }

  function onFileAdded(file) {
    if ($scope.addlogo) {
      return;
    }
    if (!isIPA(file.name) && !isAPK(file.name)) {
      clearQueue();
      return swal('上传错误', '只支持上传 iOS(.ipa) 和 Android(.apk) 游戏包', 'error');
    }
    $scope.fileCount++;
    var app = {}
    //app.uuid = uuid.v4();
    file.app = app;
    app.fileinfo = fileinfo(file.name);
    app.file = file;
    $scope.parsed = true;
    $scope.parsing = true;
    app.size =file.size;
    var reader = new FileReader();
    reader.onload = (function(a) {
      return function(e) {
        try {
          onload(e, a);
        } catch (e) {
          a.isError = true;
          $scope.parsedCount++;
          $scope.apps.push(app)
        }
      };
    })(app);
    reader.readAsArrayBuffer(file.getNative());
  };

  function onload(e, app) {

    var zip = new JSZip(e.target.result);
    var infoplist = zip.file(/^(([^\\\?\/\*\|<>:"]+\/){2})Info\.plist/);
    var resoucesArsc = zip.file(/resources.arsc/);
    var androidManifest = zip.file(/AndroidManifest.xml/);
    var data;
    // iOS
    if (app.fileinfo.platform === 1) {
      if (infoplist.length == 0) {
        app.isError = true;
        $scope.parsedCount++;
        $scope.apps.push(app)
        return;
      }
      data = {
        platform: app.fileinfo.platform,
        plist: infoplist.length && base64encode(infoplist[0].asUint8Array())
      }
    }

    // Android
    if (app.fileinfo.platform === 2) {
      if (resoucesArsc.length == 0 || androidManifest.length == 0) {
        app.isError = true;
        $scope.parsedCount++;
        $scope.apps.push(app)
        return;
      }
      data = {
        platform: app.fileinfo.platform,
        arsc: resoucesArsc.length && base64encode(resoucesArsc[0].asUint8Array()),
        manifest: androidManifest && base64encode(androidManifest[0].asUint8Array())
      }
    }

    $http.post(api.parseapp, data)
    .then(function(response) {
      if (response.data.ret == 0) {
        _.extend(app, response.data.data || {});
        previewLogo(zip, app);
      } else if (response.data.ret == 31) {
        showError('用户已过期');
      }
      $scope.parsedCount++;
      $scope.apps.push(app)
      if ($scope.parsedCount == $scope.uploader.files.length) {
        $scope.parsing = false;
      }
    });
  }

  function previewLogo(zip, app) {
    var logo = zip.file(new RegExp(app.logo));
    if (!logo.length) return;
    app.logo = logo[0].name;
    app.logotype = logotype(app.logo);
    app.logodata = logo[0].asUint8Array();
    app.logoDataUrl = 'data:' + app.logotype + ';base64,' + base64encode(app.logodata);
    // http://stackoverflow.com/questions/13950865/javascript-render-png-stored-as-uint8array-onto-canvas-element-without-data-uri
    app.isReady = true;
  }

  function onFileUploaded(up, file, info) {
    var strJson;
    if (typeof info == "string") {
      strJson = info;
    } else {
      strJson = info.response;
    }
    var result = JSON.parse(strJson)
    if (result != null && result.ret && result.ret != 0) {
      $scope.uploading = false;
      //up.stop();
      up.total.uploaded -= 1;
      up.total.failed += 1;
      toaster.pop('error', '上传失败', result.message);
    } else {

      var app = file.app;

      if ($scope.storage.type == "fileserver") {
        if (isAPK(file.name) || isIPA(file.name)) {
          app.packageurl = result.data;
        } else {
          app.logourl = result.data;
        }
      }

      if (app.packageurl && app.file.status == plupload.DONE && app.logofile.status == plupload.DONE) {
        $http.post(api.addapp, {
          ispublic: app.public,
          release_note: app.note,
          groupid: $scope.appgroupid,
          storageid: $scope.storage.id,
          package: {
            url: app.packageurl,
            logo: app.logourl,
            size: app.size,
            appname: app.appname,
            package: app.package,
            bundleid: app.bundleid,
            version: app.version,
            platform: app.platform,
            buildnumber: app.buildnumber
          }
        }).success(function (result) {
          if (result.ret != 0) {
            app.isError = true;
          } else {
            app.isSuccess = true;
          }
          app.isSaved = true;
          checkAll();
        }).error(function () {
          app.isError = true;
          toaster.pop('error', '保存失败', '');
          checkAll();
        });
      }
    }
  };

  function checkAll() {
    if ($scope.uploader.total.failed > 0) {
      return
    }
    for (var i = 0; i < $scope.apps.length; i++) {
      if($scope.apps[i].isSaved == null) {
        return
      }
    }
    $location.path('dash');
  }

  function onUploadComplete(up, files) {
    if (up.total.failed > 0) {
      $scope.uploading = false;
      showError("上传有失败");
      return
    }
  }

  function removeApp (index) {
    var app = $scope.apps[index];
    $scope.uploader.removeFile(app.file.id);
    if (app.logofile) {
      $scope.uploader.removeFile(app.logofile.id);
    }
    $scope.apps.splice(index, 1);
    $scope.parsedCount--;
    $scope.fileCount--;
    if ($scope.fileCount == 0) {
      cancel();
    }
  }

  function clearQueue() {
    $scope.apps = [];
    $scope.uploader.splice(0, $scope.uploader.files.length);
  }

  function showError(title) {
    clearQueue();
    $scope.parsing = false;
    $scope.uploading = false;
    return swal(title, '请及时与管理联系, 给您带来的不便, 敬请谅解', 'error');
  }

  function cancel() {
    $scope.uploader.stop();
    initApp();
    $scope.parsed = false;
    clearQueue();
  }

  function submit() {
    $scope.uploading = true;
    $scope.speed = 0;
    var apps = $scope.apps;
    $scope.addlogo = true;
    for (var i = 0; i < apps.length; i++) {
      var app = apps[i];
      if (app.isError) {
        $scope.uploader.removeFile(app.file.id);
        $scope.uploader.removeFile(app.logofile.id);
        continue;
      }
      var file = new mOxie.File(null, app.logoDataUrl);
      file.name = app.logo; // you need to give it a name here (required)
      var pfile = new plupload.File(file);
      pfile.app = app;
      app.logofile = pfile;
      $scope.uploader.addFile(pfile);
    }
    //uploader.addFile(new File([new Blob([$scope.app.logodata.buffer], { type: $scope.app.logotype })], $scope.app.logo), $scope.app.logo);
    if ($scope.uploader.files.length == 0) {
      swal("没有有效的文件", '请及时与管理联系, 给您带来的不便, 敬请谅解', 'error');
      $scope.uploading = false;
    } else {
      $scope.uploader.start();
    }
  }

  function getExtension(filename) { return filename.substring(filename.lastIndexOf(".")); }
  function isIPA(filename) { return /(\.|\/)(ipa)$/i.test(filename); }
  function isAPK(filename) { return /(\.|\/)(apk)$/i.test(filename); }
  function fileinfo(filename) {
    var result = {
      name: filename,
      isIPA: isIPA(filename),
      isAPK: isAPK(filename)
    };

    result.platform = (function() {
      if (this.isIPA) return 1;
      if (this.isAPK) return 2;
    }).call(result);

    result.uploadtype = (function() {
      if (this.isIPA) return 3;
      if (this.isAPK) return 4;
    }).call(result);

    return result;
  }

  function logotype(filename) {
    if (/(\.|\/)(png)$/i.test(filename)) return "image/png";
    if (/(\.|\/)(ico)$/i.test(filename)) return "image/x-icon";
    if (/(\.|\/)(gif)$/i.test(filename)) return "image/gif";
    if (/(\.|\/)(bmp)$/i.test(filename)) return "image/bmp";
    if (/(\.|\/)(jpeg|jpg|jpe)$/i.test(filename)) return "image/jpeg";
    return "image/png";
  }

  function base64encode(input) {
    var keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    var output = "";
    var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
    var i = 0;

    while (i < input.length) {
      chr1 = input[i++];
      chr2 = i < input.length ? input[i++] : Number.NaN; // Not sure if the index
      chr3 = i < input.length ? input[i++] : Number.NaN; // checks are needed here

      enc1 = chr1 >> 2;
      enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
      enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
      enc4 = chr3 & 63;

      if (isNaN(chr2)) {
        enc3 = enc4 = 64;
      } else if (isNaN(chr3)) {
        enc4 = 64;
      }

      output += keyStr.charAt(enc1) + keyStr.charAt(enc2) + keyStr.charAt(enc3) + keyStr.charAt(enc4);
    }
    return output;
  }

});