'use strict';

angular.module('takoApp')
.controller('SurveyCtrl', function($scope, $http, $routeParams, api) {
  window.__$surveyScope = $scope;
  $scope.question_types = [
    { label: '单行文本', value: 'text'   },
    { label: '多行文本', value: 'ptext'  },
    { label: '下拉选项', value: 'list'   },
    { label: '单选',    value: 'radio'   },
    { label: '范围',    value: 'slider'  },
    { label: '多选',    value: 'checkbox'},
    { label: '表格',    value: 'grid'    }
  ];

  getForms();


  $scope.editing_form = false;
  $scope.createForm = function() {
    $scope.editing_form = true;
    $scope.current_form = { name: "未命名问卷" };
  };

  $scope.editForm = function(form, index) {
    $scope.editing_form = true;
    $scope.current_form = form;
    getForm($scope.current_form.id);
  };

  $scope.editQuestion = function(question, index) {
    if (question.editing) return;
    _.each($scope.current_form.items, function(item) { if (item.editing) item.editing = false;  });
    question.editing = true;
  };

  $scope.saveQuestion = function(question, $event) {
    $event.stopPropagation();
    question.editing = false;
  };

  $scope.removeQuestion = function(question, index) {
    $scope.current_form.items.splice(index, 1);
  };

  $scope.createOption = function(value, options, event) {
    options.push({ text: value || "选项 " + (options.length + 1) });
    angular.element(event.target).blur();
  };

  $scope.removeOption = function(option, index, options) {
    if (options.length < 2) return;
    options.splice(index, 1);
  };

  function getForms() {
    $http.get(api.getForms + '?appid=' + $routeParams.id)
      .success(function(result) {
        if (result.ret != 0) return;
        $scope.forms = result.data.forms;
      });
  }

  function getForm(id, index) {
    $http.get(api.getForm + id + '?item=true')
      .success(function(result) {
        if (result.ret != 0) return;
        $scope.current_form = result.data;
      });
  }
});
