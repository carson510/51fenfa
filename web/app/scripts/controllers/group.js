'use strict';

angular.module('takoApp')
.controller('GroupCtrl', function($scope, $rootScope, $mdDialog, $http, $location, session, api, toaster) {
  window.__dash = $scope;
  if (!session.loggedIn()) return $location.path('/signin');

   getGroups();

  function getGroups() {
    $http.get(api.getGroups)
      .then(check)
      .then(function(data) {
        $scope.groups = data;
      });
  };

  $scope.toEditGroup = function(event, index) {
    $scope.editinggroup = $scope.groups[index];
    $scope.editinggroup.name_new = $scope.editinggroup.name;
    $scope.editing = true;
  };

  $scope.editGroup = function() {
    if ($scope.editinggroup.name_new == $scope.editinggroup.name) {
      $scope.editing = false;
      return
    }
    $http.put(api.editGroup, {
      id:         $scope.editinggroup.id,
      name:       $scope.editinggroup.name_new
    }).success(function(result) {
      if (result.ret != 0) {
        toaster.pop('error', '修改失败', '修改名称失败');
      } else {
        $scope.editing = false;
        $scope.editinggroup.name = $scope.editinggroup.name_new;
        toaster.pop('success', '修改成功', '修改名称成功');
      }
    });
  };

  $scope.deleteGroup = function(event, index) {
    event.preventDefault();
    var group = $scope.groups[index];
    swal({
      title: '确认永久删除应用组['+group.name+']及其所有应用?',
      type: 'warning',
      showCancelButton: true,
      cancelButtonText: "取消",
      confirmButtonText: "确认"
    }, function(confirmed) {
      if (confirmed) {
        $http.delete(api.deleteGroup + '/' + group.id, {}).success(function(result) {
          if (result.ret != 0) return;
          getGroups()
        });
      }
    });
  }

  function check(response) {
    if (response.data.ret != 0) return $q.reject(response.data);
    return response.data.data;
  }
});
