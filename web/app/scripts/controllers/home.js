'use strict';

angular.module('takoApp')
.controller('HomeCtrl', function($scope, $location, $window, session) {

  $scope.create = function() {
    session.loggedIn() ? $location.path('/release') : $location.path('/signin/release');
  };
  
  $scope.store = function() {
    $window.scroll(0, 0);
    $location.path('store');
  }
  
});