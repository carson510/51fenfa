'use strict';

angular.module('takoApp')
.controller('NavbarCtrl', function($scope, $http, $cookies, $location, $window, session, api) {

  $scope.loggedIn = session.loggedIn();

  $scope.path = $location.path();



  setUsername();

  $scope.goto2 = function(path) {
    if (!path) return;
    if (path === '/dash' && !$scope.loggedIn) path = '/signin';
    collapse();
    if ($location.path().indexOf("/#/") > 0) {
      if (path.substr(0, 1) == "/") {
        path = "/#" + path;
      } else {
        path = "/#/" + path;
      }
    }
    $location.path(path);
  };

  $scope.logout = function() {
    session.logout();
    $location.path('/');
  }

  $scope.$on('logged in', function(event, data) {
    $scope.loggedIn = true;
    setUsername();
  });

  $scope.$on('logged out', function() {
    $scope.loggedIn = false;
  });

  $scope.$watch(
    function() { return $location.path(); },
    function(path) {
      $scope.path = path;
      $scope.session = path.search(/login|signin|signup|jx3/i) > -1;
      // var excludePass = ';/store;/dash;/group;/storage;/user;/release;';
      // if(!$scope.session){
        // if(path !== '/' && path.split('/').length ==2 && excludePass.indexOf(';'+path+';')==-1){
        //   $scope.session = true;
        //   //angular.element('#main').toggleClass('container', !(path === '/' || $scope.session));
        // }
      // }
      // if(path !== '/' && path.split('/').length ==2 && excludePass.indexOf(';'+path+';')==-1){
      //   $scope.session = true;
      //   //angular.element('#main').toggleClass('container', !(path === '/' || $scope.session));
      // }else{
      //
      //   angular.element('#main').toggleClass('container', !(path === '/' || $scope.session));
      // }
      angular.element('#main').toggleClass('container', !(path === '/' || $scope.session));
    }
  );

  function collapse() {
    var $el = angular.element('.navbar-toggle');
    if ($el.attr('aria-expanded') === 'true') $el.click();
  }
  function setUsername() { $scope.nickname = $cookies.get('nickname') || null; }

});
