'use strict';

angular.module('takoApp')
.controller('LoginCtrl', function($scope, $http, $location, $routeParams, toaster, Registration, api) {
  if ($routeParams.email) $scope.email = $routeParams.email;

  $scope.login = function($event) {
    if ($event) $event.preventDefault()
    $http.post(api.login, _.pick($scope, 'email', 'password', 'serverhost')).then(ok)
  }

  function ok(response) {
    if (response.data.ret === 0)  return pass(response.data.data);
    if (response.data.ret === 25) return inactive();
    if (response.data.ret === 30) return toaster.pop('error', '登录失败', '系统检测到该帐号上传了非法应用，已被禁用，如要找回请加QQ群向管理员申诉。（QQ群：278901064）');
    return toaster.pop('error', '登录失败', response.data.message);
  }

  function pass(data) {
    var dest = '/dash';
    if($location.search().path){
      dest = '/'+ $location.search().path;
    }
    if ($routeParams.to) dest = $routeParams.to;
    if ($routeParams.email && $routeParams.role == 'tester') dest = 'tester/confirm/true';
    $location.url(dest);
    $scope.$root.$broadcast('logged in', data);
  }

  function inactive() {
    swal({
      title: '登录失败',
      text: "请先去您的注册邮箱确认账号",
      type: 'error',
      showCancelButton: true,
      cancelButtonText: "关闭",
      confirmButtonText: "重新发送确认邮件"
    }, function(confirmed) { if (confirmed) Registration.resend($scope.email); });
  }

});
