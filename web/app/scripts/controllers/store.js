'use strict';

angular.module('takoApp')
.controller('StoreCtrl', function($scope, $http, $location, api) {

  $scope.getApps = getApps;
  $scope.apps = [];
  $scope.appsCursor = 0;
  $scope.loading = false;

  function getApps() {
    if ($scope.loading || $scope.appsCount == 0) return;
    if ($scope.appsCount && ($scope.appsCount == $scope.appsCursor)) return;
    $scope.loading = true;
    $http.get(api.getapps, { params: { count: 50, cursor: $scope.appsCursor }})
      .success(function(result) {
        $scope.apps.push.apply($scope.apps, result.data.apps);
        $scope.appsCount = result.data.allrows;
        $scope.appsCursor = result.data.nextcursor;
      })
      .finally(function() {
        $scope.loading = false;
      });
  };
});
