'use strict';

angular.module('takoApp')
.controller('UserCtrl', function($scope, $route, $http, toaster, api) {

    getUser();

    function getUser() {
      $http.get(api.getMe)
        .then(check)
        .then(function(result) {
          $scope.user = result;
          $scope.user.imagepath = $scope.user.image
        });
    }

    function check(response) {
      if (response.data.ret != 0) return $q.reject(response.data);
      return response.data.data;
    }

    $scope.updateUser = function() {
      $http.put(api.editMe, {
        mobile:       $scope.user.mobile,
        nickname:     $scope.user.nickname,
        info : {
          image:      $scope.user.imagepath,
          realname:   $scope.user.realname,
          gender:     $scope.user.gender,
          address:    $scope.user.address
        }
      }).success(function(result) {
        if (result.ret != 0) toaster.pop('error', '修改失败', '修改失败');
        else {
          toaster.pop('success', '修改成功', '修改成功');
          $scope.$root.$broadcast('logged in');
          getUser();
        }
      });
    }
});
