'use strict';

angular.module('takoApp')
.controller('FeedbackCtrl', function($scope, $http, $modalInstance, FileUploader, toaster, app, api) {
console.log(app);
  $scope.app = app;
  $scope.imgurls = [];

  $scope.uploader = new FileUploader({
    queueLimit: 9,
    removeAfterUpload: true,
    filters: [{
      name: 'image',
      fn: function(item) { return /\.(png|gif|jpg|jepg)$/i.test(item.name); }
    }]
  });

  $scope.cancel = function() { $modalInstance.dismiss('cancel'); };

  $scope.submit = function() {
    upload();
    $scope.cancel();
  }

  function upload(index) {
    var i = index || 0;
    var item = $scope.uploader.queue[i];
    if (!item) return create();
    var data = new FormData();
    data.append('uploadfile', item._file);
    $http.post(api.upload +'?type=1', data, {
        transformRequest: angular.identity,
        headers: { 'Content-Type': undefined }
      })
      .success(function(result) {
        if (result.ret != 0) return;
        $scope.imgurls.push(result.data);
      })
      .finally(function() {
        upload(i + 1);
      });
  }

  var $root = $scope.$root;
  function create() {
    $http.post(api.newfeedback, {
      versionid: $scope.app.releaseversion.id,
      note: $scope.note,
      imgurls: $scope.imgurls
    }).success(function(result) {
      if (result.ret != 0) return toaster.pop('error', '提交失败', '请重试');
      toaster.pop('success', '提交成功');
      $root.$broadcast('feedback.created', result.data);
    });
  }

});
