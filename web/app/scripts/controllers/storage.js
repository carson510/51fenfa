'use strict';

angular.module('takoApp')
.controller('StorageCtrl', function($scope, $rootScope, $mdDialog, $http, $location, session, api, toaster, util) {
  window.__dash = $scope;
  if (!session.loggedIn()) return $location.path('/signin');

  $scope.addingStorage = { };
  $scope.storage_adding = false;

  getStorages();
  getStorageTypes();

  function getStorageTypes() {
    $http.get(api.storage_types)
      .then(util.check)
      .then(function(data) {
        $scope.types = data.kvs;
        $scope.addingStorage.type = data.default;
        setName(data.default);
      });
  };

  function setName(key) {
    for(var item in $scope.types) {
      if ($scope.types[item].key == key) {
        $scope.addingStorage.name = $scope.types[item].val;
        break;
      }
    }
  }

  $scope.onStorageTypeChanged = function (key) {
    setName(key);
  };

  function getStorages() {
    $http.get(api.storage_my)
        .then(util.check)
        .then(function(data) {
          $scope.storages = data==null?[]:data;
        });
  };

  $scope.toAddStorage = function() {
    $scope.storage_adding = true;
  };

  $scope.addStorage = function() {
    $http.post(api.storage_add, {
      type: $scope.addingStorage.type,
      name: $scope.addingStorage.name,
      endpoint: $scope.addingStorage.endpoint,
      uploaddomain: $scope.addingStorage.uploaddomain,
      downloaddomain: $scope.addingStorage.downloaddomain,
      bucket: $scope.addingStorage.bucket,
      accesskey: $scope.addingStorage.accesskey,
      secretkey: $scope.addingStorage.secretkey,
      default: $scope.addingStorage.default
    }).then(util.check)
      .then(function(data) {
        getStorages();
        $scope.storage_adding = false;
        toaster.pop('success', '新增成功', '新增存储路径成功');
    });
  };

  $scope.setDefault = function(id) {
    $http.put(api.storage_default_set  + id)
        .then(util.check)
        .then(function(data) {
          getStorages();
          toaster.pop('success', '设置', '设置默认存储路径成功');
        });
  };

  $scope.deleteStorage = function(event, index) {
    event.preventDefault();
    var storage = $scope.storages[index];
    swal({
      title: '确认永久删除存储路径['+storage.name+']及其所有应用?',
      type: 'warning',
      showCancelButton: true,
      cancelButtonText: "取消",
      confirmButtonText: "确认"
    }, function(confirmed) {
      if (confirmed) {
        $http.delete(api.storage_delete + storage.id, {}).success(function(result) {
          if (result.ret != 0) return;
          $scope.storages.splice(index, 1);
        });
      }
    });
  }
});
