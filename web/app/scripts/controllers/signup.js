'use strict';

angular.module('takoApp')
.controller('SignupCtrl', function($scope, $http, $location, $routeParams, toaster, api) {

  if ($routeParams.token) {
    $http.get(api.getinvitedemail + '?token=' + $routeParams.token)
      .success(function(result) {
        if (!result.data) return;
        $scope.email = result.data;
        angular.element('#email').prop('disabled', true);
      });
  }

  $scope.agreed = true;

  $scope.signup = function($event) {
    if ($event) $event.preventDefault();
    if (!$scope.form.$valid || !$scope.agreed) return;
    register();
  }

  function register() {
    var data = _.pick($scope, 'email', 'password');
    if ($routeParams.token) data.token = $routeParams.token;

    $http.post(api.register, data)
      .success(function(result) {
        if (result.ret != 0) return toaster.pop('error', '注册失败', result.message);

        if (result.ret == 0) {
          if ($routeParams.token) toaster.pop('success', '注册成功');
          else toaster.pop('success', '注册成功', '请登录邮箱 ' + $scope.email + '，激活账号');
          $location.path('/signin');
        }
    });
  }

});
