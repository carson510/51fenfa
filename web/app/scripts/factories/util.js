'use strict';

angular.module('takoApp')
.factory('util', function($q, toaster) {
  return {
    check: function (response) {
      if (response.data.ret != 0) {
        var msg = response.data.message
        if ( msg == null || msg == "") {
          msg = "请及时联系系统管理员处理,不便之处敬请谅解.";
        }
        toaster.pop('error', '操作失败', msg);
        return $q.reject(response.data);
      }
      return response.data.data;
    },
  }
});
