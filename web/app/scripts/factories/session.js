'use strict';

angular.module('takoApp')
.factory('session', function($cookies, $location, $rootScope) {
  return {
    loggedIn: function() {
      return $cookies.get('token');
    },
    logout: function() {
      $cookies.remove('token'), $cookies.remove('userid'), $cookies.remove('nickname'), $cookies.remove('serverhost');
      $rootScope.$broadcast('logged out');
    }
  }
});