'use strict';

angular.module('takoApp')
.factory('log', function($http, api) {
  return {
    download: function(versionid) {
      return $http.post(api.createdownloadlog, { appversionid: versionid });
    }
  }
});
