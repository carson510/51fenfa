'use strict';

angular.module('takoApp')
.factory('Registration', function($http, toaster, api) {
  return {
    resend: function(email) {
      $http.post(api.registermail, { email: email })
        .then(function(response) {
          if (response.data.ret == 0) toaster.pop('success', '发送成功', '请登录邮箱 ' + email + '，激活账号');
          else toaster.pop('error', '发送失败', response.data.message);
        });
    }
  }
});
