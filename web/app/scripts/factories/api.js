'use strict';

angular.module('takoApp')
.factory('api', function() {

  var _url = function (u) {
    if (u.charAt(0) == '/') {
      return "/service" + u;
//      return u;
    } else {
      return "/service/" + u;
//      return "/" + u;
    }
  };

  return {
    url: function (u) {
      return _url(u)
    },
    getmyapps: _url('getmyapps'),
    gettaskapps: _url('gettaskapps'),
    deleteApp: _url('app/delete'),
    deleteApps: _url('apps/delete'),
    uploadConfig: _url('upload/config'),
    upload: _url('upload'),
    diskusage: _url('diskusage'),
    newfeedback: _url('newfeedback'),
    removemember: _url('removemember'),
    removeInvite: _url('invite/remove'),
    feedbackStatusUpdate: _url('feedback/status/update'),
    editMailMsg: _url('app/tester/mailmsg/edit'),
    dspPermission: _url('dsp/permission'),
    dspAddtester: _url('dsp/addtester'),
    inviteTester: _url('task/tester/invite'),
    deleteComment: _url('app/feedback/comment/delete'),
    getFeedbackInfo: _url('app/feedback/'),
    deleteLike: _url('app/feedback/like/delete/'),
    feedbackLike: _url('app/feedback/like'),
    feedbackComment: _url('app/feedback/comment'),
    getAppInfo: _url('getappinfo'),
    addGroup: _url('app/group/add'),
    getGroups: _url('app/groups'),
    moveGroups: _url('app/group/move'),
    editGroup: _url('app/group/edit'),
    deleteGroup: _url('app/group/delete'),
    isValid: _url('app/isvaliduri'),
    modifyappinfo: _url('modifyappinfo'),
    getappversions: _url('getappversions'),
    getFeedbacks: _url('app/feedbacks'),
    getAudioUrl: _url('ks3/url'),
    appVersionUrl: _url('app/version/url'),
    appVersionRelease: _url('app/version/release'),
    appVersionDelete: _url('app/version/delete'),
    findallmember: _url('findallmember'),
    getinvitedmember: _url('getinvitedmember'),
    addmember: _url('addmember'),
    getDspProjects: _url('dsp/projects'),
    getTesters: _url('task/tester/all'),
    login: _url('login'),
    getinvitedemail: _url('getinvitedemail'),
    register: _url('register'),
    getapps: _url('getapps'),
    getForms: _url('app/forms'),
    getForm: _url('app/form/'),
    getAuth: _url('ks3/auth'),
    parseapp: _url('parseapp'),
    addapp: _url('addapp'),
    getMe: _url('user/me'),
    editMe: _url('user/me/edit'),
    getmyrole: _url('getmyrole'),
    registermail: _url('registermail'),
    createdownloadlog: _url('createdownloadlog'),
    getDLog: _url('app/dlog'),

    storage_types: _url('constant/filestoragetype'),
    storage_my: _url('storage/my'),
    storage_add: _url('storage/add'),
    storage_default_set: _url('storage/default/set/'),
    storage_default_get: _url('storage/default/get'),
    storage_delete: _url('storage/delete/'),
  }
});
