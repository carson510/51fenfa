angular.module('takoApp')
.filter('filesize', function() {
  return function(size) {
    if(typeof size === 'number') {
      if (size <= 0) {
        return ""
      } else if (size < 1024) {
        return size + "B"
      } else if (size < 1024 * 1024) {
        return (size / 1024).toFixed(1) + "K"
      } else if (size < 1024 * 1024 * 1024) {
        return (size / 1024 / 1024).toFixed(1) + "M"
      } else {
        return (size / 1024 / 1024 / 1024).toFixed(1) + "G"
      }
    } else {
      return '未知'
    }
  };
});