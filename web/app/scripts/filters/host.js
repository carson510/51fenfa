angular.module('takoApp')
.filter('host', ['$location', function($location) {
  return function(path) {
    return 'http://tako.im/' + path;
  }; 
}]);