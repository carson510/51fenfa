angular.module('takoApp')
.filter('counter', function() {
  return function(number) {
    if (number == null) return '';
    return '(' + number + ')';
  };
});