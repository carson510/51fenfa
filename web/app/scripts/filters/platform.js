angular.module('takoApp')
  .filter('platform', function() {
    return function(platform) {
      if (platform == 1) return 'iOS'
      if (platform == 2) return 'Android'
    };
  });