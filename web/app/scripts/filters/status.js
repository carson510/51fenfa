angular.module('takoApp')
  .filter('downloadStatus', function() {
    return function(status) {
      if (status == 0) return '未下载'
      if (status == 1) return '下载中'
      if (status == 2) return '已完成'
      if (status == 3) return '已取消'
    };
  });

angular.module('takoApp')
    .filter('YesOrNo', function() {
        return function(status) {
            if (status) return '是'
            return '否'
        };
    });