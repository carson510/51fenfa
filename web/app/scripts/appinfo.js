window.onload = function() {

  var path;

  if (location.pathname.indexOf('appinfo.html') > -1) {
    path = '/service/openapi/app/uri/nxavtr';
  }else {
    path = '/service/openapi/app/uri' + location.pathname + '?v=' + Math.random();
  }

  FastClick.attach(document.body);
  var appinfo = document.querySelector('#appinfo');
  var appDisabled = document.querySelector('#appDisabled');
  var appPrivate = document.querySelector('#appPrivate');
  var userExpired = document.querySelector('#userExpired');
  var appInfoTmp = document.querySelector('#appInfoTmp');
  var notice = document.querySelector('#notice');
  var down1 = document.querySelector('#down1');
  $('#locationUrl').html(location.href);
  var logged = function() {
    return document.cookie.indexOf('token') > 0 && document.cookie.indexOf('userid') > 0 && document.cookie.indexOf('username') > 0;
  };

  var isAppleIOS = navigator.userAgent.match(/iPhone|iPad|iPod/i) ? true : false;
  var isSafari= navigator.userAgent.match(/Safari/i) ? true : false;

  $.ajax({
    type:  'GET',
    url: path,
    dataType: 'json',
    //timeout: 300,
    //context: $('body'),
    success: function(result) {
      if (result.ret == 30) {
        appDisabled.style.display = 'block';
      } else if (result.ret == 31) {
        userExpired.style.display = 'block';
      } else if (result.ret == 28) {
        location.href = "/web/404";
      } else if (result.ret == 0) {
        handleAppInfo(result.data);
      } else if (result.ret == 1004) {
        appinfo.style.display = 'none';
        $('.tako-confirm').css('height', $(window).height());
        appPrivate.style.display = 'block';
      }
    },
    error: function(xhr, type){

    }
  });
  function trim(str){
    var whitespace = "[\\x20\\t\\r\\n\\f]";
    var rtrim = new RegExp( "^" + whitespace + "+|((?:^|[^\\\\])(?:\\\\.)*)" + whitespace + "+$", "g" );
    return str == null ? "" :( str + "" ).replace( rtrim, "" );
  }

  function handleAppInfo(data) {
    appinfo.style.display = 'block';
    $('#appinfo').css('height', window.innerHeight);
    appDisabled.style.display = 'none';
    appPrivate.style.display = 'none';
    var app = data;


    app.qrurl = "http://" + location.host + location.pathname;

    if (trim(app.lanhost) && trim(app.releaseversion.package.lanurl)) {
      if (!trim(app.releaseversion.package.url)) {
        app.lan = true;
      } else {
        var hcurl = "http://" + app.lanhost + "/" + app.releaseversion.package.lanurl;
        $.ajax({
          type: 'HEAD',
          url: hcurl,
          timeout: 1000,
          success: function(result, status, headers){
            if (headers("content-type") != null && headers("content-type") == "application/octet-stream") {
              app.lan = true;
            }
          },
          error: function(){

          }
        });
      }
    }
    new QRCode(document.getElementById("qrcode"), {
      text: app.qrurl,
      width: 100,
      height: 100
    });
    
    var platformStr = app.platform==2 ? '<img src="../images/android.png" style="height:20px;vertical-align: middle;"/>' : '<img src="../images/iOS.png"  style="height:20px;vertical-align: middle;"/>';
    var time = new Date(app.releaseversion.createtime*1000).customFormat("#YYYY#/#MM#/#DD# #hh#:#mm#:#ss#");
    var appInfoTemplate =
      '<div class="logoimg" style="margin-bottom: 30px"><img style="width:120px; height:120px" src="'+ app.logourl +'"/></div>'+
      '<div class="" style="text-align: center; color: #fff; margin-bottom: 35px">'+
        '<p class="p1" style="font-size: 18px; margin-bottom: 12px">'+
          app.appname +'('+ (app.releaseversion.package.size/(1024*1024)).toFixed(1) +' M)'+
        '</p>'+
        '<p class="p2">'+app.releaseversion.package.version +'(Build ' + app.releaseversion.package.buildnumber + ')</p>'+
        '<p class="p3">更新于 '+ time +'</p>'+
      '</div>'+
      '<div class="text-center">';
      appInfoTemplate += '<div id="preparing" style="margin-top:60px;text-align:center;color:#fff;display:none;"><p>正在准备安装...</p></div>'
      appInfoTemplate += '<div id="installing" style="margin-top:60px;text-align:center;color:#fff;display:none;"><p>应用正在安装，请按Home键回到桌面查看进度。</p></div>'
      if((app.platform == 1 && isAppleIOS && isSafari) || (app.platform == 2 && !isAppleIOS)) {
        appInfoTemplate += '<a id="down1" class="btn btn-primary inverse btn-lg" style="padding: 10px 35px" href="javascript:void(0);">'+ platformStr +'下载安装</a>';
      } else if (app.platform == 1) {
        appInfoTemplate += '<div style="margin-top:60px;text-align:center;color:#fff;"><p>请使用苹果手机的Safria浏览器打开以下网址进行安装:</p>' + 'http://' + location.host + '/' + app.uri + '</div>';
      } else {
        appInfoTemplate += '<div style="margin-top:60px;text-align:center;color:#fff;"><p>请使用安卓手机浏览器打开以下网址进行安装:</p>' + 'http://' + location.host + '/' + app.uri + '</div>';
      }
      appInfoTemplate += "</div>"

    appInfoTmp.innerHTML = appInfoTemplate;
    setTimeout(function(){
      $('#down1').click(function(){
          download('', app);
      });
    }, 100);
    $('.footer').css('display', 'block');
    var isWeChat = navigator.userAgent.search(/MicroMessenger/i) > -1;
    if (isWeChat && ((app.platform == 1 && isAppleIOS) || (app.platform == 2 && !isAppleIOS))) {
      notice.style.display = 'block';
      return;
    }

  };

  $('.gotolink').click(function(e){
    var path = $(e.target).attr('data-link');
    var isIndex = $(e.target).attr('data-isIndex') == 'true' ? true : false;
    goto(path, isIndex);
  });

  function goto(path, isIndex) {
    var privatePath = '?' + 'path=' + (location.pathname.substring(1, location.pathname.length));
    if (isIndex) {
      location.href = path;
    } else {
      var isWeChat = navigator.userAgent.search(/MicroMessenger/i) > -1;
      if (isWeChat) {
        notice.style.display = 'block';
        return setTimeout(function() { notice.style.display = 'none'; }, 1000 * 10);
      }
      location.href = path + privatePath;
    }
  };

  function download(msg, app) {
    var query = {id: app.releaseversion.id};
    var queryUrl = '/service/app/url?id=' + app.id + "&downloadlogid=" + app.downloadlogid;
    var isWeChat = navigator.userAgent.search(/MicroMessenger/i) > -1;
    if (isWeChat) {
      notice.style.display = 'block';
      // $('#notice').fadeIn(100);
      return setTimeout(function () {
        notice.style.display = 'none';
      }, 1000 * 10);
    }
    if (app.password == 'true') {
      query.password = prompt(msg || '请输入下载密码', ' ');
      if (!query.password) return;
      if (query.password == null) return;
      query.password = trim(query.password);
      queryUrl += '&password=' + query.password;
    }
    var result = "";

    if (app.lan) {
      query.lan = true;
    }

    var result = "";
    $.ajax({
      type: 'GET',
      url: queryUrl,
      dataType: 'json',
      async: false,
      success: function (r) {
        result = r;
      }
    });

    if (result.ret == 0) {
      if (app.lan) {
        $('#innerNetwork').show();
        setTimeout(function () {
          $('#innerNetwork').hide();
        }, 3000);
      }
      var url = result.data;
      if (app.platform == 2) {
        document.getElementById("downloadURL").src = url;
      } else {
        $('#down1').hide();
        $('#preparing').show();
        recheck(app);
        location.href = url;
      }
    } else if (app.password == 'true') {
      download('密码错误，请重新输入', app);
    } else {
      alert(result.message);
    }
  };

  function checkIfIOSInstalling(app) {
    $.ajax({
      type:  'GET',
      url: "/service/app/dlog/installing?downloadlogid=" + app.downloadlogid,
      dataType: 'json',
      success: function(result) {
        if (result.ret == 0) {
          $('#preparing').hide();
          $('#installStarted').show();
        } else {
          recheck(app);
        }
      },
      error: function(xhr, type){
        recheck(app);
      }
    });
  }

  var checkCount = 0;

  function recheck(app) {
    checkCount++;
    if (checkCount > 20) {
      checkCount = 0;
      $('#preparing').hide();
      $('#installing').hide();
      $('#installNotStarted').show();
    } else {
      setTimeout(function () {
        checkIfIOSInstalling(app);
      }, 500);
    }
  }

  function checkStatus(response) {
    if (response.status >= 200 && response.status < 300) {
      return response
    } else {
      var error = new Error(response.statusText);
      error.response = response
      throw error
    }
  }

  function parseJSON(response) {
    return response.json()
  }

  Date.prototype.customFormat = function(formatString){
    var YYYY,YY,MMMM,MMM,MM,M,DDDD,DDD,DD,D,hhh,hh,h,mm,m,ss,s,ampm,AMPM,dMod,th;
    var dateObject = this;
    YY = ((YYYY=dateObject.getFullYear())+"").slice(-2);
    MM = (M=dateObject.getMonth()+1)<10?('0'+M):M;
    MMM = (MMMM=["January","February","March","April","May","June","July","August","September","October","November","December"][M-1]).substring(0,3);
    DD = (D=dateObject.getDate())<10?('0'+D):D;
    DDD = (DDDD=["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"][dateObject.getDay()]).substring(0,3);
    th=(D>=10&&D<=20)?'th':((dMod=D%10)==1)?'st':(dMod==2)?'nd':(dMod==3)?'rd':'th';
    formatString = formatString.replace("#YYYY#",YYYY).replace("#YY#",YY).replace("#MMMM#",MMMM).replace("#MMM#",MMM).replace("#MM#",MM).replace("#M#",M).replace("#DDDD#",DDDD).replace("#DDD#",DDD).replace("#DD#",DD).replace("#D#",D).replace("#th#",th);

    h=(hhh=dateObject.getHours());
    if (h==0) h=24;
    hh = h<10?('0'+h):h;
    AMPM=(ampm=hhh<12?'am':'pm').toUpperCase();
    mm=(m=dateObject.getMinutes())<10?('0'+m):m;
    ss=(s=dateObject.getSeconds())<10?('0'+s):s;
    return formatString.replace("#hhh#",hhh).replace("#hh#",hh).replace("#h#",h).replace("#mm#",mm).replace("#m#",m).replace("#ss#",ss).replace("#s#",s).replace("#ampm#",ampm).replace("#AMPM#",AMPM);
  }

};
