#!/bin/bash
#Shell Command for backup MongoDB everyday automatically by crontab

WEBMASTER=yaoshuangxi@kingsoft.com

HOSTNAME="121.14.28.232"
DATABASE="tako"
USERNAME="tako"
PASSWORD="5bAiwANMeIDaO"

DATE=`date '+%Y%m%d-%H%M'` #日期格式（作为文件名）
BACKUP_DIR=/usr/data/dbbak #备份文件存储路径
LOGFILE=/usr/data/dbbak/log/tako_$DATE.log #日记文件路径

ARCHIVE=tako_$DATE.tgz #压缩文件名
OPTIONS="-h $HOSTNAME -u $USERNAME -p $PASSWORD -d $DATABASE"

#mongodump －help
 
#判断备份文件存储目录是否存在，否则创建该目录
if [ ! -d $BACKUP_DIR ] ;
then
    mkdir -p "$BACKUP_DIR"
fi
 
#开始备份之前，将备份信息头写入日记文件
echo " " >> $LOGFILE
echo " " >> $LOGFILE
echo "———————————————–" >> $LOGFILE
echo "BACKUP DATE:" $(date +"%y-%m-%d %H:%M:%S") >> $LOGFILE
echo "———————————————– " >> $LOGFILE
 
#切换至备份目录
cd $BACKUP_DIR
#mongodump 命令备份制定数据库，并以格式化的时间戳命名备份目录
echo $OPTIONS >> $LOGFILE
mongodump $OPTIONS -o $BACKUP_DIR
#判断数据库备份是否成功
if [[ $? == 0 ]]; then
    #创建备份文件的压缩包
    tar -czvf $ARCHIVE -C $BACKUP_DIR/ tako >> $LOGFILE 2>&1
    #输入备份成功的消息到日记文件
    echo "[$ARCHIVE] Backup Successful!" >> $LOGFILE
    #删除原始备份文件，只需保 留数据库备份文件的压缩包即可
    rm -rf $BACKUP_DIR/tako
    #echo "Backup tako data sucessfully" | mail -s "Tako DB Backup[Success]" yaoshuangxi@kingsoft.com -c baiyinzu@kingsoft.com
else
    echo "Database Backup Fail!" >> $LOGFILE
    #Send email
    #echo "Backup tako data failed" | mail -s "Tako DB Backup[Failed]" yaoshuangxi@kingsoft.com -c baiyinzu@kingsoft.com
fi
#输出备份过程结束的提醒消息
echo "Backup Process Done"

#crontab -e
#/var/spool/cron/root
#00 19 * * * /usr/data/dbbak/bin/backup.sh
