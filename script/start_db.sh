[ "${0##*/}" = "$0" ] && { WHICHONE=$(which $0 || echo "${PWD}/$0" );BINDIR="${WHICHONE%/*}"; } || BINDIR=$(cd "${0%/*}"; pwd)
cd ${BINDIR}

nohup /opt/mongodb/bin/mongod --dbpath /root/xunce/database/data/ --logpath /root/xunce/database/log/log.log --auth &
