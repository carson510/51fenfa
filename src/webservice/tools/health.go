package tools
import (
	"net/http"
	"github.com/yaosxi/mgox"
	"encoding/json"
)

type Health struct {
	Status string `json:"status"`
}

func checkStatus(w http.ResponseWriter, r *http.Request) {
	h := Health{"UP"}
	b, _ := json.Marshal(h)
	w.Write(b)
}

func checkDB(w http.ResponseWriter, r *http.Request) {
	var h Health
	dao := mgox.Connect().ShareError()
	defer dao.Close()
	dao.Get().IgnoreNFE().Result(&h)
	if h.Status == "" {
		h.Status = "UP"
		dao.Insert(&h)
		h.Status = ""
		dao.Get().Result(&h)
	}
	if dao.LastError != nil {
		h.Status = dao.LastError.Error()
	}
	b, _ := json.Marshal(h)
	w.Write(b)
}

func StartHealthService() {
	go func() {
		http.HandleFunc("/health/status", checkStatus)
		http.HandleFunc("/health/status+db", checkDB)
		http.ListenAndServe(":23340", nil)
	}()
}