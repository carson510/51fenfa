package main

import (
	"fmt"
	"github.com/kimiazhu/log4go"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/context"
	"github.com/bitly/go-simplejson"
	"net/http"
	"os"
	"path"
	"strings"
	"webservice/constant"
	"webservice/controllers"
	"webservice/model"
	"webservice/service"
	"webservice/utils"
	"webservice/tools"
)

func main() {

	setupLogger()

	routers_for_filter()

	constant.InitConstants()

	//Constant
	beego.Router("/constant/:id", &controllers.ConstantController{}, "get:Get")
	beego.Router("/constant/filestoragetype", &controllers.ConstantController{}, "get:GetFileStorageType")

	//register
	//beego.Router("/register", &controllers.RegisterController{})
	beego.Router("/login", &controllers.LoginApiController{})
	beego.Router("/logout", &controllers.SessionController{}, "delete:Logout")

	//user
	beego.Router("/user/edit", &controllers.UserController{}, "put:Edit")
	beego.Router("/user/me/edit", &controllers.UserController{}, "put:EditMyInfo")
	beego.Router("/user/me", &controllers.UserController{}, "get:GetMyInfo")
	beego.Router("/user/disable/:id", &controllers.UserController{}, "put:Disable")

	// App
	beego.Router("/addapp", &controllers.AppController{})
	beego.Router("/app/query", &controllers.AppController{}, "get:QueryAppInfo")
	beego.Router("/getmyapps", &controllers.AppController{}, "get:GetMyApps")
	beego.Router("/app/isvaliduri", &controllers.AppController{}, "get:IsValidAppUri")
	beego.Router("/getappinfo", &controllers.AppController{}, "get:GetAppInfo")
	beego.Router("/app/:id", &controllers.AppController{}, "get:GetAppInfo")
	beego.Router("/modifyappinfo", &controllers.AppController{}, "post:ModifyAppInfo")
	beego.Router("/app/delete/:id", &controllers.AppController{}, "delete:DeleteApp")
	beego.Router("/apps/delete", &controllers.AppController{}, "delete:DeleteApps")
	beego.Router("/app/password/validate", &controllers.AppController{}, "get:ValidatePassword")
	beego.Router("/app/url", &controllers.AppController{}, "get:GetAppDownloadUrl")

	//Embedded file server
	beego.Router("/upload", &controllers.FileServerController{}, "post:Upload")
	beego.Router("/removefile", &controllers.FileServerController{}, "delete:RemoveFile")
	beego.Router("/diskusage", &controllers.FileServerController{}, "get:DiskUsage")

	routers_for_parsepackage()

	routers_for_checklogin()

	beego.Router("/app/dlog", &controllers.DownloadLogController{}, "get:GetAppLogs")
	beego.Router("/app/dlog/starting", &controllers.DownloadLogController{}, "get:Starting")
	//beego.Router("/app/dlog/started", &controllers.DownloadLogController{}, "get:Started")
	beego.Router("/app/dlog/completed", &controllers.DownloadLogController{}, "get:Completed")
	beego.Router("/app/dlog/installing", &controllers.DownloadLogController{}, "get:IsStarted")

	//App Group
	beego.Router("/app/groups", &controllers.AppGroupController{}, "get:GetGroups")
	beego.Router("/app/group/add", &controllers.AppGroupController{}, "post:AddGroup")
	beego.Router("/app/group/move", &controllers.AppGroupController{}, "put:MoveGroup")
	beego.Router("/app/group/edit", &controllers.AppGroupController{}, "put:EditGroup")
	beego.Router("/app/group/delete/:id", &controllers.AppGroupController{}, "delete:DeleteGroup")

	//FileStorage
	beego.Router("/storage/my", &controllers.FileStorageController{}, "get:GetMy")
	beego.Router("/storage/add", &controllers.FileStorageController{}, "post:Add")
	beego.Router("/storage/delete/:id", &controllers.FileStorageController{}, "delete:Delete")
	beego.Router("/storage/default/set/:id", &controllers.FileStorageController{}, "put:SetDefault")
	beego.Router("/storage/default/get", &controllers.FileStorageController{}, "get:GetDefault")

	//Open API
	beego.Router("/openapi/app/:id", &controllers.AppController{}, "get:GetAppInfo")
	beego.Router("/openapi/app/uri/:uri", &controllers.AppController{}, "get:Get")

	//Support API
	beego.Router("/support/payuser/add", &controllers.UserController{}, "get:AddPayUser")
	beego.Router("/support/payuser/edit", &controllers.UserController{}, "get:EditPayUser")

	beego.Errorhandler("404", func(w http.ResponseWriter, r *http.Request) {
		var result controllers.Response
		result.Message = constant.Info_Error_NotFound
		result.Ret = constant.Code_Error_NotFound
		result.Tag = "api"
		w.Write([]byte(result.FormatJsonString()))
	})

	tools.StartHealthService()

	beego.Run()
}

func setupLogger() {

	dir, err := os.Getwd()
	if err != nil {
		panic(err)
		return
	}
	dir = path.Join(dir, "log")
	exists, err := util.IsPathExists(dir)
	if err != nil {
		panic(err)
		return
	}
	if !exists {
		err := os.MkdirAll(dir, os.ModePerm)
		if err != nil {
			panic(err)
			return
		}
	}
}

func routers_for_filter() {

	var FilterUser = func(ctx *context.Context) {

		userid := ctx.GetCookie("userid")
		token := ctx.GetCookie("token")
		if userid == "" {
			userid = ctx.Input.Query("userid")
		}
		if token == "" {
			token = ctx.Input.Query("token")
		}
		if (userid == "" || token == "") && ctx.Input.RequestBody != nil {
			var postJson string
			postJson = fmt.Sprintf("%s", ctx.Input.RequestBody)
			js, err := simplejson.NewJson([]byte(postJson))
			if err == nil {
				if userid == "" {
					userid, _ = js.Get("userid").String()
				}
				if token == "" {
					token, _ = js.Get("token").String()
				}
			}
		}
		var session model.Session
		if token != "" {
			session = service.GetSessionInfoFromCache(token)
		}

		log4go.Info("Request URI: %s, from client: %v, email: %s, userid: %s", ctx.Request.URL.Path, util.GetClientInfo(ctx.Request), session.Email, session.UserId)

		if ctx.Request.URL.Path == "/healthcheck" {
			ctx.WriteString("ok")
			return
		}

		if ctx.Request.URL.Path != "/login" &&
		len(ctx.Request.URL.Path) != 5 &&
		len(ctx.Request.URL.Path) != 25 &&
		ctx.Request.URL.Path != "/getapps" &&
		ctx.Request.URL.Path != "/getappreleaseversion" &&
		ctx.Request.URL.Path != "/weblogin" &&
		ctx.Request.URL.Path != "/register" &&
		ctx.Request.URL.Path != "/webregister" &&
		ctx.Request.URL.Path != "/checkupdate" &&
		ctx.Request.URL.Path != "/attachlist" &&
		ctx.Request.URL.Path != "/upload" &&
		ctx.Request.URL.Path != "/removefile" &&
		ctx.Request.URL.Path != "/diskusage" &&
		ctx.Request.URL.Path != "/delattach" &&
		ctx.Request.URL.Path != "/mailactive" &&
		ctx.Request.URL.Path != "/registermail" &&
		ctx.Request.URL.Path != "/getinvitedemail" &&
		ctx.Request.URL.Path != "/invite/tester/confirm" &&
		ctx.Request.URL.Path != "/app/password/validate" &&
		ctx.Request.URL.Path != "/app/url" &&
		ctx.Request.URL.Path != "/app/dlog/starting" &&
		ctx.Request.URL.Path != "/app/dlog/installing" &&
		ctx.Request.URL.Path != "/getappversions" &&
		ctx.Request.URL.Path != "/tako/upgrade/latest" &&
		ctx.Request.URL.Path != "/tako/upgrade/check" &&
		!strings.HasPrefix(ctx.Request.URL.Path,  "/app/dlog/") &&
		!strings.HasPrefix(ctx.Request.URL.Path, "/app/ios/") &&
		!strings.HasPrefix(ctx.Request.URL.Path, "/app/install/") &&
		!strings.HasPrefix(ctx.Request.URL.Path, "/openapi/") &&
		!strings.HasPrefix(ctx.Request.URL.Path, "/support/") &&
		strings.Index(ctx.Request.RequestURI, "/createinvitecode") == -1 {

			if userid == "" || token == "" {
				writeIllegalSession(ctx)
				return
			}
			if session.Token == "" || session.UserId != userid {
				writeIllegalSession(ctx)
				return
			}
		}
	}

	beego.InsertFilter("/*", beego.BeforeExec, FilterUser)
}

func writeIllegalSession(ctx *context.Context) {
	var result controllers.Response
	result.Message = constant.Info_Error_SessionIllegal
	result.Ret = constant.Code_Error_SessionIllegal
	ctx.WriteString(result.FormatJsonString())
}

func writeError(ctx *context.Context, err error) {
	var result controllers.Response
	result.Message = err.Error()
	result.Ret = constant.Code_Error_OperationFailed
	ctx.WriteString(result.FormatJsonString())
}

func routers_for_parsepackage() {
	/*
		@brief:解析plist & manifest文件			| POST
		@param:platform 平台类型					| (放在body中)
		@param:plist  		info.plist文件内容	| (放在body中)
		@param:manifest  	manifest文件内容		| (放在body中)
		@param:arsc  		arsc文件内容			| (放在body中)
		@return:{"message":"ok","ret":0,"data":{...}}
	*/
	beego.Router("/parseapp", &controllers.ParsePackageController{})
}

func routers_for_checklogin() {
	beego.Router("/checklogin", &controllers.CheckLoginController{})
}
