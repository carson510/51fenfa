package config

import (
	"github.com/astaxie/beego"
)

// api
const API_VERSION_1 = "v1"

// Collection

const DataTableSession = "session"
const DataTableUser = "user"

//Session
//TODO not used
const SessionTimeout = 6 * 30 * 24 * 60 * 60 // 6 个月

//Cache
const CacheTimeOut = 1 * 60 * 60 * 24 //一天

// encrypt
const DES_KEY = "JME*#$KF" // des crypt key lenght must be 8 in xunce system

const USER_PWD_KEY = "KEGD^&#O"

const EmailDESkey = "rgfut435"
const EmalDESLV = "ewbh67no"

const DSPUrl = "https://spiapi.seasungame.com:9001/checkloginforsedt"

const EMAIL_FROM_DISPLAYNAME = "Tako内测服务"

const EMAIL_TYPE_HTML = "html"

func GetHost() string {
	return beego.AppConfig.String("Host")
}

func GetServiceRoot() string {
	return beego.AppConfig.String("ServiceRoot")
}

func GetPlistHost() string {
	return beego.AppConfig.DefaultString("PlistHost", "oss-cn-shenzhen-internal.aliyuncs.com")
}

func GetDownloadUrl() string {
	return beego.AppConfig.String("DownloadUrl")
}

func GetGridFsHttpRootPath() string {
	return GetServiceRoot() + "/"
}

func GetHttpFsRootPath() string {
	return GetServiceRoot() + "/"
}

func GetKS3UploadDir(dir string) string {
	if beego.AppConfig == nil {
		return dir + "/"
	}
	return beego.AppConfig.DefaultString("KS3UploadDir", "") + dir + "/"
}

func GetFileServer() string {
	if beego.AppConfig == nil {
		return "static"
	}
	return beego.AppConfig.DefaultString("FileServer", "static")
}
