package storage

import (
	"crypto/hmac"
	"crypto/sha1"
	"encoding/base64"
	"fmt"
	"github.com/nu7hatch/gouuid"
	"qiniupkg.com/api.v7/kodo"
	"time"
	"webservice/model"
	"webservice/constant"
	"strings"
	"qiniupkg.com/api.v7/kodocli"
	"github.com/kimiazhu/log4go"
	"path/filepath"
	"os"
	"github.com/ks3sdklib/aws-sdk-go/service/s3"
	"github.com/ks3sdklib/aws-sdk-go/aws"
	"github.com/ks3sdklib/aws-sdk-go/aws/credentials"
	"errors"
	"github.com/astaxie/beego/httplib"
	"github.com/aliyun/aliyun-oss-go-sdk/oss"
)

func makeHmac(key []byte, data []byte) []byte {
	hash := hmac.New(sha1.New, key)
	hash.Write(data)
	return hash.Sum(nil)
}

func iso8601utc(t time.Time) string {
	utc := t.UTC()
	fmt.Println(utc.Year())
	return fmt.Sprintf("%04d-%02d-%02dT%02d:%02d:%02d.000%s", utc.Year(), utc.Month(), utc.Day(), utc.Hour(), utc.Minute(), utc.Second(), "Z")
}

var StorageHelper storageHelper

type storageHelper struct {
}


func (this *storageHelper) NewKey() string {
	u, _ := uuid.NewV4()
	return time.Now().Format("20060102") + "/" + u.String()
}

func (this *storageHelper) GetQiniuToken(bucket, accessKey, secretKey string) string {
	//创建一个Client
	c := kodo.New(0, &kodo.Config{AccessKey: accessKey, SecretKey: secretKey})
	//设置上传的策略
	policy := &kodo.PutPolicy{
		Scope: bucket,
		ReturnBody: `{
            "name": $(fname),
            "size": $(fsize),
            "hash": $(etag)
        }`,
	}
	//生成一个上传token
	return c.MakeUptoken(policy)
}

func (this *storageHelper) UploadFileToQiniu(bucket, path, accessKey, secretKey string) error {

	key := this.NewKey() + filepath.Ext(path)

	uptoken := this.GetQiniuToken(bucket, accessKey, secretKey)

	//构建一个uploader
	uploader := kodocli.NewUploader(0, nil)

	var ret kodo.PutRet
	//设置上传文件的路径

	//调用PutFileWithoutKey方式上传，没有设置saveasKey以文件的hash命名
	err := uploader.PutFile(nil, &ret, uptoken, key, path, nil)
	//打印出错信息
	if err != nil {
		log4go.Error("io.Put failed:", err)
	}
	return err
}

func (this *storageHelper) DeleteFileFromQiniu(bucket, key, accessKey, secretKey string) error {
	//new一个Bucket管理对象
	c := kodo.New(0, &kodo.Config{AccessKey: accessKey, SecretKey: secretKey})
	p := c.Bucket(bucket)
	//调用Delete方法删除文件
	err := p.Delete(nil, key)
	//打印返回值以及出错信息
	if err != nil {
		if err.Error() == "no such file or directory" {
			return nil
		}
		log4go.Error("Delete failed:", err)
	}
	return err
}

func (this *storageHelper) GetSK3Signature(bucket, secretKey string) (string, string) {
	expiration := iso8601utc(time.Now().Add(5 * time.Minute))
	policyStr := `{"expiration": "` + expiration + `","conditions": [{"acl": "public-read" },{"bucket": "` + bucket + `" },["starts-with", "$key", ""],["starts-with", "$name", ""]]}`
	policy := base64.StdEncoding.EncodeToString([]byte(policyStr))
	signature := string(base64.StdEncoding.EncodeToString(makeHmac([]byte(secretKey), []byte(policy))))
	return policy, signature
}

//
//func (this *storageHelper) UploadFileToKS3(bucket, path, accessKey, secretKey string) error {
//	fi,err := os.Open(path)
//	if err != nil{
//		return err
//	}
//	defer fi.Close()
//
//	var clientMgmtOfHz = s3.New(&aws.Config{
//		Credentials:      credentials.NewStaticCredentials(accessKey, secretKey, ""),
//		Endpoint:         "kss.ksyun.com", // s3 上传地址
//		DisableSSL:       true,            //是否禁用https
//		LogLevel:         0,               //是否开启日志,0为关闭日志，1为开启日志
//		S3ForcePathStyle: false,           //是否强制使用path style方式访问
//		LogHTTPBody:      true,            //是否把HTTP请求body打入日志
//		Logger:           os.Stdout,       //打日志的位置
//	})
//	key := this.NewKey() + filepath.Ext(path)
//	params := &s3.PutObjectInput{
//		Bucket: aws.String(bucket),          // bucket名称
//		Key:    aws.String(key),             // object key
//		ACL:    aws.String("public-read"),    //权限，支持private(私有)，public-read(公开读)
//		Body:   fi, //要上传的内容
//	}
//	_, err = clientMgmtOfHz.PutObject(params)
//	return err
//}

func (this *storageHelper)  DeleteFileFromKS3(endpoint, bucket, key, accessKey, secretKey string) error {

	var clientMgmtOfHz = s3.New(&aws.Config{
		Region: "HANGZHOU",
		Credentials:      credentials.NewStaticCredentials(accessKey, secretKey, ""),
		Endpoint:         endpoint, // s3 上传地址
		DisableSSL:       true,            //是否禁用https
		LogLevel:         0,               //是否开启日志,0为关闭日志，1为开启日志
		S3ForcePathStyle: false,           //是否强制使用path style方式访问
		LogHTTPBody:      true,            //是否把HTTP请求body打入日志
		Logger:           os.Stdout,       //打日志的位置
	})

	params := &s3.DeleteObjectInput{
		Bucket: aws.String(bucket), // bucket名称
		Key:    aws.String(key),   // object key
	}
	output, err := clientMgmtOfHz.DeleteObject(params)
	if err != nil {
		return err
	}
	if output == nil || output.DeleteMarker == nil {
		return nil
	}
	if !*output.DeleteMarker {
		log4go.Error("delete ks3 path failed: %s", key)
		return errors.New("failed")
	}

	log4go.Info("deleted ks3 path: %s", key)

	return nil
}

func (this *storageHelper) GetAliyunSignature(secretKey string) (string, string) {
	policyStr := `{"expiration": "2050-01-01T12:00:00.000Z","conditions": [["content-length-range", 0, 2147483648]]}`
	policy := base64.StdEncoding.EncodeToString([]byte(policyStr))
	signature := string(base64.StdEncoding.EncodeToString(makeHmac([]byte(secretKey), []byte(policy))))
	return policy, signature
}

func (this *storageHelper) DeleteFileFromAliyun(endpoint, bucket, key, accessKey, secretKey string) error {
	client, err := oss.New(endpoint, accessKey, secretKey)
	if err != nil {
		return err
	}
	p, err := client.Bucket(bucket)
	if err != nil {
		return err
	}
	err = p.DeleteObject(key)
	if err != nil {
		return err
	}
	return err
}


func (this *storageHelper) DeleteFile(key string, fs model.FileStorage) error {
	if key == "" || strings.HasPrefix(key, "http://") || strings.HasPrefix(key, "https://")  {
		return nil
	}
	if fs.Type == constant.FileStorageType_FileServer {
		removeUrl := "http://" + fs.UploadDomain + "/service/removefile?filepath=" + key
		log4go.Info("deleting " + removeUrl)
		go func() {
			_, err := httplib.Delete(removeUrl).SendOut()
			if err != nil {
				log4go.Error(err)
			}
		}()
		return nil
	} else if fs.Type == constant.FileStorageType_Qiniu {
		return this.DeleteFileFromQiniu(fs.Bucket, key, fs.AccessKey, fs.SecretKey)
	} else if fs.Type == constant.FileStorageType_KSyun {
		return this.DeleteFileFromKS3(fs.UploadDomain, fs.Bucket, key, fs.AccessKey, fs.SecretKey)
	}else if fs.Type == constant.FileStorageType_Aliyun {
		return this.DeleteFileFromAliyun(fs.Endpoint, fs.Bucket, key, fs.AccessKey, fs.SecretKey)
	}
	return nil
}

func  (this *storageHelper) SetConfig(fs *model.FileStorage) {
	if fs.Type != "" && fs.Type != constant.FileStorageType_FileServer {
		fs.Key = time.Now().Format("20060102")
		if fs.Type == constant.FileStorageType_Qiniu {
			fs.Token = this.GetQiniuToken(fs.Bucket, fs.AccessKey, fs.SecretKey)
		} else if fs.Type == constant.FileStorageType_KSyun {
			fs.Policy, fs.Signature = this.GetSK3Signature(fs.Bucket, fs.SecretKey)
		}else if fs.Type == constant.FileStorageType_Aliyun {
			fs.Policy, fs.Signature = this.GetAliyunSignature(fs.SecretKey)
		}
	}
}

func (this *storageHelper) GetDownloadUrl(host, key string) string {

	log4go.Debug("Get download url: domain=%s, key=%s", host, key)

	if strings.HasPrefix(key, "http://") || strings.HasPrefix(key, "https://")  {
		return key
	}
	if key == "" {
		return key
	}
	return "http://" + host + "/" + key
}
