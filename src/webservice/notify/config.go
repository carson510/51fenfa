package notify

import (
	"encoding/xml"
	"io/ioutil"
	"webservice/utils"
)

type Notification struct {
	Email EmailNotification `xml:"email"`
}

type EmailNotification struct {
	Smtp     EmailSmtp      `xml:"smtp"`
	Messages []EmailMessage `xml:"message"`
}

type EmailSmtp struct {
	Host     string `xml:"host"`
	Account  string `xml:"account"`
	Password string `xml:"password"`
}

type EmailMessage struct {
	Name    string `xml:"name,attr"`
	Lang    string `xml:"lang,attr"`
	Subject string `xml:"subject"`
	Content string `xml:"content"`
	File    string `xml:"file"`
}

var parsed = false
var notification Notification

func parseConfig() {
	if parsed {
		return
	}
	contents, err := ioutil.ReadFile("conf/notification.xml")
	if err != nil {
		panic(err)
	}
	err = xml.Unmarshal(contents, &notification)
	parsed = true
}

func GetEmailSmtp() (host string, account string, password string) {
	parseConfig()
	return notification.Email.Smtp.Host, notification.Email.Smtp.Account, notification.Email.Smtp.Password
}

func GetEmailContent(name string, lang string, values map[string]string) (subject string, content string) {
	parseConfig()
	for _, message := range notification.Email.Messages {
		if message.Name == name && (lang == "" || lang == message.Lang) {
			if message.Content == "" {
				if message.File != "" {
					fileContent, _ := ioutil.ReadFile("conf/" + message.File)
					message.Content = string(fileContent)
				}
			}
			return util.RepleaceVariable(message.Subject, values), util.RepleaceVariable(message.Content, values)
		}
	}
	return "", ""
}
