package notify

import (
	"bytes"
	"github.com/astaxie/beego"
	"net/smtp"
	"strings"
	"webservice/config"
	"webservice/utils"
)

/*
 *  user : example@example.com login smtp server user
 *  password: xxxxx login smtp server password
 *  host: smtp.example.com:port   smtp.163.com:25
 *  to: example@example.com;example1@163.com;example2@sina.com.cn;...
 *  subject:The subject of mail
 *  body: The content of mail
 *  mailtyoe: mail type html or text
 */

func sendMail(user, password, host, to, dispalyName, subject, body, mailtype string) error {

	//for test
	//to = "carson510@126.com"

	hp := strings.Split(host, ":")
	auth := smtp.PlainAuth("", user, password, hp[0])
	var content_type string
	if mailtype == "html" {
		content_type = "Content-Type: text/" + mailtype + "; charset=UTF-8"
	} else {
		content_type = "Content-Type: text/plain" + "; charset=UTF-8"
	}

	encodeSubject := "=?UTF-8?B?" + util.Base64Encode(subject) + "?="
	encodeDispalyName := "=?UTF-8?B?" + util.Base64Encode(dispalyName) + "?="

	var msg bytes.Buffer
	msg.WriteString("To: " + to)
	msg.WriteString("\r\n")
	msg.WriteString("From: " + encodeDispalyName + "<" + user + ">")
	msg.WriteString("\r\n")
	msg.WriteString("Subject: " + encodeSubject)
	msg.WriteString("\r\n")
	msg.WriteString(content_type)
	msg.WriteString("\r\n\r\n")
	msg.WriteString(body)
	send_to := strings.Split(to+beego.AppConfig.String("NotifiyTestEmail"), ";")
	err := smtp.SendMail(host, auth, user, send_to, msg.Bytes())
	return err
}

func SendEmail(to string, name string, values map[string]string) error {
	host, account, password := GetEmailSmtp()
	subject, content := GetEmailContent(name, "", values)
	return sendMail(account, password, host, to, config.EMAIL_FROM_DISPLAYNAME, subject, content, config.EMAIL_TYPE_HTML)
}
