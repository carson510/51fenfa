package test

import (
	"github.com/stretchr/testify/assert"
	"webservice/model"
	"webservice/service"
	"testing"
)

func TestAddTaskMember(t *testing.T) {
	tm := model.TaskMember{}
	tm.AppId = "55bb1c0ee138230ee7000004"
	tm.UserId = "55b6f570e138237ac6000001"
	err := service.AddTaskMember(tm)
	if err == nil {
		t.Error("Add failed")
	} else {
		t.Error(err)
	}
}

func TestGetTaskAppIdsByUserId(t *testing.T) {
	appids, err := service.GetTaskAppIdsByUserId("55b6f570e138237ac6000001")
	if err != nil {
		t.Error(err)
	} else {
		assert.Equal(t, 1, len(appids), "The count should be 1.")
	}
}
