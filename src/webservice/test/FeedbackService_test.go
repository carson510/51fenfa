package test_test

import (
	"gopkg.in/mgo.v2/bson"
	"webservice/model"
	"webservice/service"
	"testing"
)

var userid = "56020d30cca30520a4000001"
var userid2 = "56020da4cca30520a4000004"

var fbid = "5604e63fcca30520b4000008"
var fbid2 = "5604e644cca30520b400000a"
var fbid3 = "5604e64acca30520b400000c"

func TestFeedbackLike(t *testing.T) {
	service.FeedbackService.Like(fbid, userid)
	service.FeedbackService.Like(fbid, userid2)
	service.FeedbackService.Like(fbid2, userid)
	service.FeedbackService.Like(fbid2, userid2)
	service.FeedbackService.Like(fbid3, userid)
	service.FeedbackService.Like(fbid3, userid2)
}

func TestFeedbackCancelLike(t *testing.T) {
	service.FeedbackService.CancelLike(fbid, userid)
	service.FeedbackService.CancelLike(fbid, userid2)
	service.FeedbackService.CancelLike(fbid2, userid)
	service.FeedbackService.CancelLike(fbid2, userid2)
	service.FeedbackService.CancelLike(fbid3, userid)
	service.FeedbackService.CancelLike(fbid3, userid2)
}

func TestFeedbackComment(t *testing.T) {
	service.FeedbackService.Comment(&model.FeedbackComment{FeedbackId: fbid, UserId: userid, Comment: "Comment1"})
	service.FeedbackService.Comment(&model.FeedbackComment{FeedbackId: fbid, UserId: userid2, Comment: "Comment2"})

	service.FeedbackService.Comment(&model.FeedbackComment{FeedbackId: fbid2, UserId: userid, Comment: "Comment3"})
	service.FeedbackService.Comment(&model.FeedbackComment{FeedbackId: fbid2, UserId: userid2, Comment: "Comment4"})

	service.FeedbackService.Comment(&model.FeedbackComment{FeedbackId: fbid3, UserId: userid, Comment: "Comment5"})
	service.FeedbackService.Comment(&model.FeedbackComment{FeedbackId: fbid3, UserId: userid2, Comment: "Comment6"})
}

func TestFeedbackDeleteComment(t *testing.T) {
	service.FeedbackService.DeleteComment(&model.FeedbackComment{Id: bson.ObjectIdHex("5604ed63cca3051908000001"), FeedbackId: fbid, UserId: userid})
	service.FeedbackService.DeleteComment(&model.FeedbackComment{Id: bson.ObjectIdHex("5604ed63cca3051908000002"), FeedbackId: fbid, UserId: userid2})

	service.FeedbackService.DeleteComment(&model.FeedbackComment{Id: bson.ObjectIdHex("5604ed63cca3051908000003"), FeedbackId: fbid2, UserId: userid})
	service.FeedbackService.DeleteComment(&model.FeedbackComment{Id: bson.ObjectIdHex("5604ed63cca3051908000004"), FeedbackId: fbid2, UserId: userid2})

	service.FeedbackService.DeleteComment(&model.FeedbackComment{Id: bson.ObjectIdHex("5604ed63cca3051908000005"), FeedbackId: fbid3, UserId: userid})
	service.FeedbackService.DeleteComment(&model.FeedbackComment{Id: bson.ObjectIdHex("5604ed63cca3051908000006"), FeedbackId: fbid3, UserId: userid2})
}
