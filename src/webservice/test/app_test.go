package test

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"testing"
)

var url_str string = "http://127.0.0.1:9999/addapp"

//func TestAddApp(t *testing.T) {
//	body := `{"ispublic":true,"userid":"55af48d21048ca2b88000001","token":"36176ee2c01d4bab4d2580ec44814bf1","package":{"appname":"XgsdkDemo","bundleid":"","package":"org.cocos2dx.hellocpp","url":"/static/app/android/2ac8cd83-4191-4e3f-6ce2-cdc143db7281.apk","logo":"/static/app/logo/bb023958-4de1-4196-60c8-c9323cb6ac9f.jpg","version":"1.0","platform":2,"buildnumber":1},"release_note":"test"}`
//	resp, err := http.Post(url_str, "application/json", strings.NewReader(body))
//	if err != nil {
//		t.Error("vist failed")
//	}
//	defer resp.Body.Close()
//	bdy, err := ioutil.ReadAll(resp.Body)
//	fmt.Println(string(bdy))
//}

//func TestGetApps(t *testing.T) {
//	url_str = "http://121.14.30.229/xunce/getapps?userid=55ae0785e13823072b000001&token=50fc0f31a8764a575539506ebd073e11&usertype=0"
//	resp, err := http.Get(url_str)
//	if err != nil {
//		t.Error("vist failed")
//	}
//	defer resp.Body.Close()
//	bdy, err := ioutil.ReadAll(resp.Body)
//	fmt.Println(string(bdy))
//}

func TestAddAppMember(t *testing.T) {
	url_str = "http://127.0.0.1:9999/addmember"
	body := `{"token":"36176ee2c01d4bab4d2580ec44814bf1","userid":"55af48d21048ca2b88000001","appid":"55b87dcd1048ca2ff8000002","usertype":1,"users":["258627934@qq.com"]}`
	resp, err := http.Post(url_str, "application/json", strings.NewReader(body))
	if err != nil {
		t.Error("vist failed")
	}
	defer resp.Body.Close()
	bdy, err := ioutil.ReadAll(resp.Body)
	fmt.Println(string(bdy))
}

//func TestRemoveMember(t *testing.T) {
//	url_str = "http://121.14.30.229/xunce/removemember"
//	body := `{"token":"50fc0f31a8764a575539506ebd073e11","userid":"55ae0785e13823072b000001","appid":"55b5906de1382368fe000002","memberids":["55b087a9e1382326cf00000c"]}`
//	resp, err := http.Post(url_str, "application/json", strings.NewReader(body))
//	if err != nil {
//		t.Error("vist failed")
//	}
//	defer resp.Body.Close()
//	bdy, err := ioutil.ReadAll(resp.Body)
//	fmt.Println(string(bdy))
//}

//func TestGetAllMember(t *testing.T) {
//	url_str = "http://121.14.30.229/xunce/findallmember?userid=55ae0785e13823072b000001&token=50fc0f31a8764a575539506ebd073e11&appid=55b5906de1382368fe000002&usertype=-1"
//	resp, err := http.Get(url_str)
//	if err != nil {
//		t.Error("vist failed")
//	}
//	defer resp.Body.Close()
//	bdy, err := ioutil.ReadAll(resp.Body)
//	fmt.Println(string(bdy))
//}

//func TestAddAppComment(t *testing.T) {
//	url_str = "http://127.0.0.1:9999/newfeedback"
//	body := `{"token":"36176ee2c01d4bab4d2580ec44814bf1","userid":"55af48d21048ca2b88000001","versionid":"55b87f051048ca0ce4000002","note":"test","imgurls":["http://www.sj88.com/attachments/bd-aldimg/1204/124/2.jpg","http://images.enet.com.cn/2013/5/3/1367452933174.jpg"]}`
//	resp, err := http.Post(url_str, "application/json", strings.NewReader(body))
//	if err != nil {
//		t.Error("vist failed")
//	}
//	defer resp.Body.Close()
//	bdy, err := ioutil.ReadAll(resp.Body)
//	fmt.Println(string(bdy))
//}

//func TestLogin(t *testing.T) {
//	url_str = "http://127.0.0.1:9999/login"
//	body := `{"email":"huzhiqiang02@hotmail.com","password":"v+a10ONe8Yo="} `
//	resp, err := http.Post(url_str, "application/json", strings.NewReader(body))
//	if err != nil {
//		t.Error("vist failed")
//	}
//	defer resp.Body.Close()
//	bdy, err := ioutil.ReadAll(resp.Body)
//	fmt.Println(string(bdy))
//}

//func TestSendVerificationEmail(t *testing.T) {
//	url_str = "http://127.0.0.1:9999/registermail"
//	body := `{"email":"xunchetesting@163.com"} `
//	resp, err := http.Post(url_str, "application/json", strings.NewReader(body))
//	if err != nil {
//		t.Error("vist failed")
//	}
//	defer resp.Body.Close()
//	bdy, err := ioutil.ReadAll(resp.Body)
//	fmt.Println(string(bdy))
//}

//func TestRegister(t *testing.T) {
//	url_str = "http://127.0.0.1:9999/register"
//	body := `{"email":"huzhiqiang05@hotmail.com","password":"v+a10ONe8Yo="} `
//	resp, err := http.Post(url_str, "application/json", strings.NewReader(body))
//	if err != nil {
//		t.Error("vist failed")
//	}
//	defer resp.Body.Close()
//	bdy, err := ioutil.ReadAll(resp.Body)
//	fmt.Println(string(bdy))
//}

//func TestCheckLogin(t *testing.T) {
//	url_str = "http://127.0.0.1:9999/checklogin"
//	body := `{"userid":"55b883453c9a561250000001","token":"d0549c7f34ae47b249b36eddd5efbec0"} `
//	resp, err := http.Post(url_str, "application/json", strings.NewReader(body))
//	if err != nil {
//		t.Error("vist failed")
//	}
//	defer resp.Body.Close()
//	bdy, err := ioutil.ReadAll(resp.Body)
//	fmt.Println(string(bdy))
//}

//func TestDownloadLog(t *testing.T) {
//	url_str = "http://127.0.0.1:9999/createdwonloadlog"
//	body := `{"userid":"55b883453c9a561250000001","token":"d0549c7f34ae47b249b36eddd5efbec0","appversionid":"55b8b6063c9a560f8c000002"}`
//	resp, err := http.Post(url_str, "application/json", strings.NewReader(body))
//	if err != nil {
//		t.Error("vist failed")
//	}
//	defer resp.Body.Close()
//	bdy, err := ioutil.ReadAll(resp.Body)
//	fmt.Println(string(bdy))
//}
