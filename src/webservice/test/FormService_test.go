package test

import (
	"gopkg.in/mgo.v2/bson"
	"log"
	"strconv"
	"webservice/model"
	"webservice/service"
	"webservice/utils"
	"testing"
	"github.com/yaosxi/mgox"
)

// local
var appid = "55eff0b2e138231c8200000c"
var userid = "55ec05f2cca3050680000004"
var userid2 = "55ec0917cca30518b8000004"
var userid3 = "55ec096acca30518b8000007"

func TestRemoveAllForms(t *testing.T) {
	mgox.New().Remove(model.FormItemAnswerCollection, bson.M{})
	mgox.New().Remove(model.FormItemResponseCollection, bson.M{})
	mgox.New().Remove(model.FormItemCollection, bson.M{})
	mgox.New().Remove(model.FormCollection, bson.M{})
}

func createItem(formRes *model.FormResponse, formid string, textIdOfName string, ptextIdOfSotry string, radioIdOfGender string, checkboxIdOfColor string, listIdOfAge string, sliderIdOfScore string, gridIdOfSchool string, _userid string) {

	service.FormService.Prerespond(formid, _userid, formRes)

	var item_textIdOfName model.FormItemResponse
	item_textIdOfName.ItemId = textIdOfName
	item_textIdOfName.QuestionType = model.QuestionType_Text
	item_textIdOfName.ItemAnswers = []model.FormItemAnswer{model.FormItemAnswer{Answer: "carson"}}
	formRes.ItemResponses = append(formRes.ItemResponses, item_textIdOfName)

	var item_ptextIdOfSotry model.FormItemResponse
	item_ptextIdOfSotry.ItemId = ptextIdOfSotry
	item_ptextIdOfSotry.QuestionType = model.QuestionType_PText
	item_ptextIdOfSotry.ItemAnswers = []model.FormItemAnswer{model.FormItemAnswer{Answer: "my story\nmy story"}}
	formRes.ItemResponses = append(formRes.ItemResponses, item_ptextIdOfSotry)

	var item_radioIdOfGender model.FormItemResponse
	item_radioIdOfGender.ItemId = radioIdOfGender
	item_radioIdOfGender.QuestionType = model.QuestionType_Radio
	item_radioIdOfGender.ItemAnswers = []model.FormItemAnswer{model.FormItemAnswer{Answer: "Male"}}
	formRes.ItemResponses = append(formRes.ItemResponses, item_radioIdOfGender)

	var item_checkboxIdOfColor model.FormItemResponse
	item_checkboxIdOfColor.ItemId = checkboxIdOfColor
	item_checkboxIdOfColor.QuestionType = model.QuestionType_Checkbox
	item_checkboxIdOfColor.ItemAnswers = []model.FormItemAnswer{model.FormItemAnswer{Answer: "Green"}, model.FormItemAnswer{Answer: "Blue"}, model.FormItemAnswer{Answer: "Indigo"}}
	formRes.ItemResponses = append(formRes.ItemResponses, item_checkboxIdOfColor)

	var item_listIdOfAge model.FormItemResponse
	item_listIdOfAge.ItemId = listIdOfAge
	item_listIdOfAge.QuestionType = model.QuestionType_List
	item_listIdOfAge.ItemAnswers = []model.FormItemAnswer{model.FormItemAnswer{Answer: "18~25"}}
	formRes.ItemResponses = append(formRes.ItemResponses, item_listIdOfAge)

	var item_sliderIdOfScore model.FormItemResponse
	item_sliderIdOfScore.ItemId = sliderIdOfScore
	item_sliderIdOfScore.QuestionType = model.QuestionType_Slider
	item_sliderIdOfScore.ItemAnswers = []model.FormItemAnswer{model.FormItemAnswer{Answer: "4"}}
	formRes.ItemResponses = append(formRes.ItemResponses, item_sliderIdOfScore)

	var item_gridIdOfSchool model.FormItemResponse
	item_gridIdOfSchool.ItemId = gridIdOfSchool
	item_gridIdOfSchool.QuestionType = model.QuestionType_Grid
	item_gridIdOfSchool.ItemAnswers = []model.FormItemAnswer{
		model.FormItemAnswer{BaseOn: "Art", Answer: "A"},
		model.FormItemAnswer{BaseOn: "History", Answer: "A"},
		model.FormItemAnswer{BaseOn: "Music", Answer: "A"},
		model.FormItemAnswer{BaseOn: "Biography", Answer: "A"},
		model.FormItemAnswer{BaseOn: "Maths", Answer: "A"},
	}
	formRes.ItemResponses = append(formRes.ItemResponses, item_gridIdOfSchool)
}

func TestEditForm(t *testing.T) {

	var form model.Form
	form.AppId = appid
	form.Name = "Form Test"
	form.Desc = "This is a from description"
	form.Shuffle = false
	form.Resubmit = false
	form.ResponseText = "Your response has been recorded."
	service.FormService.EditForm(&form, userid)

	form.Name = "Form Test2"
	form.Desc = "This is a from description2"
	form.Shuffle = false
	form.ResponseText = "Your response has been recorded2."
	form.Resubmit = false
	err := service.FormService.EditForm(&form, userid)
	if err != nil {
		t.Error(err)
	}

	var textIdOfName, ptextIdOfSotry, radioIdOfGender, checkboxIdOfColor, listIdOfAge, sliderIdOfScore, duplicateSliderIdOfScore, gridIdOfSchool string

	var item model.FormItem
	item.AppId = appid
	item.FormId = form.Id.Hex()
	item.Title = "What's your name"
	item.Desc = "Your name"
	item.Required = true
	item.Shuffle = false
	item.GotoOnAnswer = false
	item.Type = "text"
	item.Text = &model.QuestionText{MinLength: 10, MaxLength: 100}
	err = service.FormService.EditItem(&item, userid)
	if err != nil {
		t.Error(err)
		return
	}
	item.Text = nil
	textIdOfName = item.Id.Hex()

	item.Id = bson.ObjectId("")
	item.Title = "What's your story"
	item.Desc = "Your story"
	item.Type = "ptext"
	item.Text = &model.QuestionText{MinLength: 10, MaxLength: 100}
	err = service.FormService.EditItem(&item, userid)
	if err != nil {
		t.Error(err)
		return
	}
	item.Text = nil
	ptextIdOfSotry = item.Id.Hex()

	item.Id = bson.ObjectId("")
	item.Title = "What's your gender"
	item.Desc = "Your gender"
	item.Type = "radio"
	item.Radio = &model.QuestionRadio{Options: []model.FormItemOption{
		model.FormItemOption{Text: "Male"},
		model.FormItemOption{Text: "Female"},
	}}
	err = service.FormService.EditItem(&item, userid)
	if err != nil {
		t.Error(err)
		return
	}
	item.Radio = nil
	radioIdOfGender = item.Id.Hex()

	item.Id = bson.ObjectId("")
	item.Title = "What color do you like?"
	item.Desc = "From seven colors"
	item.Type = "checkbox"
	item.Checkbox = &model.QuestionCheckbox{Options: []model.FormItemOption{
		model.FormItemOption{Text: "Red"},
		model.FormItemOption{Text: "Orange"},
		model.FormItemOption{Text: "Yellow"},
		model.FormItemOption{Text: "Green"},
		model.FormItemOption{Text: "Blue"},
		model.FormItemOption{Text: "Indigo"},
		model.FormItemOption{Text: "Violet"},
	}}
	err = service.FormService.EditItem(&item, userid)
	if err != nil {
		t.Error(err)
		return
	}
	item.Checkbox = nil
	checkboxIdOfColor = item.Id.Hex()

	item.Id = bson.ObjectId("")
	item.Title = "How old are you?"
	item.Desc = "Your age"
	item.Type = "list"
	item.List = &model.QuestionList{Options: []model.FormItemOption{
		model.FormItemOption{Text: "1~17"},
		model.FormItemOption{Text: "18~25"},
		model.FormItemOption{Text: "26~30"},
		model.FormItemOption{Text: "31~35"},
		model.FormItemOption{Text: "35~"},
	}}
	err = service.FormService.EditItem(&item, userid)
	if err != nil {
		t.Error(err)
		return
	}
	item.List = nil
	listIdOfAge = item.Id.Hex()

	item.Id = bson.ObjectId("")
	item.Title = "Your score, please?"
	item.Desc = "Your score"
	item.Type = "slider"
	item.Slider = &model.QuestionSlider{MinValue: 1, MaxValue: 5}
	err = service.FormService.EditItem(&item, userid)
	if err != nil {
		t.Error(err)
		return
	}
	item.Slider = nil
	sliderIdOfScore = item.Id.Hex()

	item.Id = bson.ObjectId("")
	item.Title = "Your score, please?"
	item.Desc = "Your score"
	item.Type = "slider"
	item.Slider = &model.QuestionSlider{MinValue: 1, MaxValue: 5}
	err = service.FormService.EditItem(&item, userid)
	if err != nil {
		t.Error(err)
		return
	}
	item.Slider = nil
	duplicateSliderIdOfScore = item.Id.Hex()

	item.Id = bson.ObjectId("")
	item.Title = "How about your school record?"
	item.Desc = "Your school record"
	item.Type = "grid"
	item.Grid = &model.QuestionGrid{
		Row: []model.FormItemOption{
			model.FormItemOption{Text: "Art"},
			model.FormItemOption{Text: "History"},
			model.FormItemOption{Text: "Music"},
			model.FormItemOption{Text: "Biography"},
			model.FormItemOption{Text: "Maths"},
		},
		Column: []model.FormItemOption{
			model.FormItemOption{Text: "A"},
			model.FormItemOption{Text: "B"},
			model.FormItemOption{Text: "C"},
			model.FormItemOption{Text: "D"},
		}}
	err = service.FormService.EditItem(&item, userid)
	if err != nil {
		t.Error(err)
		return
	}
	item.Grid = nil
	gridIdOfSchool = item.Id.Hex()

	err = service.FormService.DeleteItem(duplicateSliderIdOfScore)
	if err != nil {
		t.Error(err)
		return
	}
	//move slider after text
	err = service.FormService.MoveItemAfter(sliderIdOfScore, textIdOfName)
	if err != nil {
		t.Error(err)
		return
	}
	//move text after list
	err = service.FormService.MoveItemAfter(textIdOfName, listIdOfAge)
	if err != nil {
		t.Error(err)
		return
	}
	//move gride to top
	err = service.FormService.MoveItemAfter(gridIdOfSchool, "")
	if err != nil {
		t.Error(err)
		return
	}

	var formRes model.FormResponse
	createItem(&formRes, form.Id.Hex(), textIdOfName, ptextIdOfSotry, radioIdOfGender, checkboxIdOfColor, listIdOfAge, sliderIdOfScore, gridIdOfSchool, userid)
	err = service.FormService.Respond(&formRes, userid)
	if err != nil {
		t.Error(err)
		return
	}

	var formRes2 model.FormResponse
	createItem(&formRes2, form.Id.Hex(), textIdOfName, ptextIdOfSotry, radioIdOfGender, checkboxIdOfColor, listIdOfAge, sliderIdOfScore, gridIdOfSchool, userid2)
	err = service.FormService.Respond(&formRes2, userid2)
	if err != nil {
		t.Error(err)
		return
	}

	var formRes3 model.FormResponse
	createItem(&formRes3, form.Id.Hex(), textIdOfName, ptextIdOfSotry, radioIdOfGender, checkboxIdOfColor, listIdOfAge, sliderIdOfScore, gridIdOfSchool, userid3)
	err = service.FormService.Respond(&formRes3, userid3)
	if err != nil {
		t.Error(err)
		return
	}

	var form2 model.Form
	err = service.FormService.GetForm(form.Id.Hex(), true, userid, &form2)
	if err != nil {
		t.Error(err)
	} else {
		log.Println("Item Count: " + strconv.Itoa(len(form2.Items)))
	}

	log.Println("form id: " + form.Id.Hex())
}

func TestGetFormWithItems(t *testing.T) {
	var form model.Form
	err := service.FormService.GetForm("55fa60f8cca3053300000001", true, userid, &form)
	if err != nil {
		t.Error(err)
	} else {
		log.Println(util.InterfaceUtil.ToJsonString(form))
	}
}
