package test

import (
	"encoding/base64"
	"fmt"
	"webservice/config"
	"webservice/utils"
	"testing"
)

func TestDES(t *testing.T) {
	key := []byte(config.EmailDESkey)
	lv := []byte(config.EmalDESLV)
	//baseurl:= "http://www.xunche.net/#/"
	param := "emailactivation?user=huzhiqiang@kingsoft.com&key=ewqfrsdfsd"

	srcbytes := []byte(param)
	destbytes, err := util.DESEncode(srcbytes, key, lv)
	if err == nil {
		deststr := string(destbytes)
		urlstr := util.UrlEncode(deststr)
		fmt.Println(urlstr)

		recurlstr, err := util.UrlDecode(urlstr)
		if err == nil {
			recdstbytes := []byte(recurlstr)
			srcbytes, err := util.DESDecode(recdstbytes, key, lv)
			if err == nil {
				srcstr := string(srcbytes)
				fmt.Println(srcstr)
			} else {
				fmt.Println("DESDecode error!")
				fmt.Println(err)
			}
		} else {
			fmt.Println("URLDecode error!")
			fmt.Println(err)
		}

	} else {
		fmt.Println("encode error!")
		fmt.Println(err)
	}
}

func TestPass(t *testing.T) {
	pass := "123456"

	UESR_PWD_IV := []byte{0x13, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF}
	encodeByte, _ := util.DESEncode([]byte(pass), []byte(config.USER_PWD_KEY), UESR_PWD_IV)
	fmt.Println("encript pass:" + string(encodeByte))
	newPass := base64.StdEncoding.EncodeToString(encodeByte)
	fmt.Println("encode pass:" + newPass)

	inputpass := "v+a10ONe8Yo="
	encrypt_password_data, _ := base64.StdEncoding.DecodeString(inputpass)
	password_data, _ := util.DESDecode(encrypt_password_data, []byte(config.DES_KEY), UESR_PWD_IV)
	fmt.Println("decode pass:" + string(password_data))

}
