package test

import (
	"github.com/stretchr/testify/assert"
	"webservice/model"
	"webservice/service"
	"testing"
)

func TestGetTaskAppsByUserId(t *testing.T) {
	apps, _, _, err := service.GetTaskAppsByUserId("55b6f570e138237ac6000001", model.Platform_Android, 0, 20, "")
	if err != nil {
		t.Error(err)
	} else {
		assert.Equal(t, 1, len(apps), "The count should be 1.")
	}
}
