package test

import (
    "path/filepath"
    "testing"
    "strings"
    "fmt"
    "time"
)

func Test1(t *testing.T) {
    fmt.Println(getFileExtensionName("http://www.test.com/abcd.apk"))
    fmt.Println(getFileExtensionName("http://www.test.com/abcd.apk?adasdasdasdf=asdfas=asdf"))
}

func getFileExtensionName(url string) string {
    fileExtension := filepath.Ext(url)
    if strings.Index(fileExtension, "?") > 0 {
        fileExtension = fileExtension[0:strings.Index(fileExtension, "?")]
    }
    return fileExtension
}

func Test2(t *testing.T) {
    fmt.Println(time.Now())
}