package constant

import (
	"testing"
	"github.com/kimiazhu/log4go"
)

func TestInsertConstant(t *testing.T) {
	var c Constant
	c.Id = KV_FileStorageType
	c.Name = "文件存储类型"
	c.KVs = []ConstantKV{ConstantKV{"1", "a", 1},ConstantKV{"2", "b", 2}}
	c.Insert()
}

func TestLoadConstant(t *testing.T) {
	var c Constant
	c.Load(KV_FileStorageType)
	log4go.Debug(c)
	log4go.Close()
}