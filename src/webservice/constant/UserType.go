package constant

type UserType int

const (
	UserType_AppOwner   = iota //应用拥有者
	UserType_AppAdmin          // 管理员
	UserType_TeamMember        //应用开发组成员
	UserType_TestMember        //参与测试人员

//	UserType_ALL = -1
)
