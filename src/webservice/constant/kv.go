package constant

import (
	"github.com/kimiazhu/log4go"
	"github.com/yaosxi/mgox"
	"sort"
	"time"
)

const KV_FileStorageType string = "FileStorageType"
const FileStorageType_FileServer string = "fileserver"
const FileStorageType_Qiniu string = "qiniu"
const FileStorageType_Aliyun string = "aliyun"
const FileStorageType_KSyun string = "ksyun"

var ConstantCollectionName = new(Constant)

type Constant struct {
	Id           string      `json:"id" bson:"_id"`
	Name         string      `json:"name"`
	Default      string      `json:"default"`
	KVs          ConstantKVs `json:"kvs,omitempty"`
	FirstCreated time.Time   `json:"firstcreated"`
	dao          *mgox.Dao   `json:"-" bson:"-"`
	loaded       bool        `json:"-" bson:"-"`
}

type ConstantKVs []ConstantKV

type ConstantKV struct {
	Key string `json:"key"`
	Val string `json:"val"`
	Idx int    `json:"idx"`
}

func (this *Constant) Exist() (bool, error) {
	if this.dao == nil {
		this.dao = mgox.New().Connect()
		defer this.dao.Close()
	}
	return this.dao.Get(this.Id).Exist(ConstantCollectionName)
}

func (this *Constant) Load(id ...string) error {
	if len(id) > 0 {
		this.Id = id[0]
	}
	if this.dao == nil {
		this.dao = mgox.New().Connect()
		defer this.dao.Close()
	}
	err := this.dao.Get(this.Id).IgnoreNFE().Result(this)
	if err == nil {
		sort.Sort(this.KVs)
	}
	this.loaded = true
	return err
}

func (this *Constant) Insert() error {
	if this.dao == nil {
		this.dao = mgox.New().Connect()
		defer this.dao.Close()
	}
	exist, err := this.Exist()
	if err != nil {
		return err
	}
	if !exist {
		return this.dao.Insert(this)
	} else {
		log4go.Warn("the constant '%s' has been existed.", this.Id)
	}
	return nil
}

func (this *Constant) GetVal(key string) string {
	if !this.loaded {
		this.Load()
	}
	for _, kv := range this.KVs {
		if kv.Key == key {
			return kv.Val
		}
	}
	return ""
}

func (slice ConstantKVs) Len() int {
	return len(slice)
}

func (slice ConstantKVs) Less(i, j int) bool {
	return slice[i].Idx < slice[j].Idx
}

func (slice ConstantKVs) Swap(i, j int) {
	slice[i], slice[j] = slice[j], slice[i]
}

func InitConstants() {
	var c Constant
	c.Id = KV_FileStorageType
	c.Name = "文件存储类型"
	c.Default = "qiniu"
	c.KVs = []ConstantKV{
		ConstantKV{FileStorageType_FileServer, "文件服务器", 1},
		ConstantKV{FileStorageType_Qiniu, "七牛云存储", 2},
		ConstantKV{FileStorageType_KSyun, "金山云存储", 3},
		ConstantKV{FileStorageType_Aliyun, "阿里对象存储", 4},
	}
	c.Insert()
}
