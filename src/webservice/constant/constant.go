package constant

type UploadFileType int

const (
	UnknowFileType = iota
	Screenshot
	AppIcon
	Package_iOS
	Package_Android
	Image
	Attachment
)
