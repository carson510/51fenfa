package util

import (
	"testing"
	"io/ioutil"
	"fmt"
)

func TestParseEmbeddedMobileProvision(t *testing.T) {
	b, err := ioutil.ReadFile("/Users/yaoshuangxi/Desktop/embedded.mobileprovision")
	if err != nil {
		fmt.Println(err)
	} else {
		udids := GetProvisionedDevices(string(b))
		for _, udid := range udids {
			fmt.Println(udid)
		}
	}
}
