package util

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/des"
	"errors"
	"fmt"
)

func AesEncrypt(origData, key []byte) ([]byte, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}
	blockSize := block.BlockSize()
	origData = PKCS7Padding(origData, blockSize)
	// origData = ZeroPadding(origData, block.BlockSize())
	blockMode := cipher.NewCBCEncrypter(block, key[:blockSize])
	crypted := make([]byte, len(origData))
	// 根据CryptBlocks方法的说明，如下方式初始化crypted也可以
	// crypted := origData
	blockMode.CryptBlocks(crypted, origData)
	return crypted, nil
}

func AesDecrypt(crypted, key []byte) ([]byte, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}
	blockSize := block.BlockSize()
	blockMode := cipher.NewCBCDecrypter(block, key[:blockSize])
	origData := make([]byte, len(crypted))
	// origData := crypted
	blockMode.CryptBlocks(origData, crypted)
	origData = UnPKCS7Padding(origData)
	// origData = ZeroUnPadding(origData)
	return origData, nil
}

func ZeroPadding(ciphertext []byte, blockSize int) []byte {
	padding := blockSize - len(ciphertext)%blockSize
	padtext := bytes.Repeat([]byte{0}, padding)
	return append(ciphertext, padtext...)
}

func ZeroUnPadding(origData []byte) []byte {
	length := len(origData)
	unpadding := int(origData[length-1])
	return origData[:(length - unpadding)]
}

func PKCS5Padding(ciphertext []byte, blockSize int) []byte {
	padding := blockSize - len(ciphertext)%blockSize
	padtext := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(ciphertext, padtext...)
}

func PKCS5UnPadding(origData []byte) []byte {
	length := len(origData)
	// 去掉最后一个字节 unpadding 次
	unpadding := int(origData[length-1])
	return origData[:(length - unpadding)]
}

/**
 *	PKCS7补码
 *	这里可以参考下http://blog.studygolang.com/167.html
 */
func PKCS7Padding(data []byte, blockSize int) []byte {
	fmt.Println(blockSize)
	blockSize = 16
	padding := blockSize - len(data)%blockSize
	padtext := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(data, padtext...)

}

/**
 *	去除PKCS7的补码
 */
func UnPKCS7Padding(data []byte) []byte {
	length := len(data)
	// 去掉最后一个字节 unpadding 次
	unpadding := int(data[length-1])
	return data[:(length - unpadding)]
}

/**
AES Encode: mode: CBC
	the src is the Plaintext bytes to encode
	the key length must be 16、24、32,
	and the iv length must equal block size: 16,
	return Ciphertext bytes
**/
func AESEncode(src, key, iv []byte) ([]byte, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}
	blocklen := block.BlockSize()
	if blocklen != len(iv) {
		return nil, errors.New("IV length must equal block size")
	}

	src = PKCS5Padding(src, blocklen)
	cbc := cipher.NewCBCEncrypter(block, iv)
	dst := make([]byte, len(src))
	cbc.CryptBlocks(dst, src)
	return dst, nil
}

/**
AES Decode: mode: CBC
	the src is the Ciphertext bytes to decode
	the key length must be 16、24、32,
	and the iv length must equal block size: 16,
	return Plaintext bytes
**/
func AESDecode(src, key, iv []byte) ([]byte, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}
	blocklen := block.BlockSize()
	if blocklen != len(iv) {
		return nil, errors.New("IV length must equal block size")
	}
	cbc := cipher.NewCBCDecrypter(block, iv)
	dst := make([]byte, len(src))
	cbc.CryptBlocks(dst, src)
	return PKCS5UnPadding(dst), nil
}

/**
DES Encrypt: mode: CBC
	the src is the Plaintext bytes to decode
	the key length must be 8,
	and the iv length must equal block size: 8,
	return Ciphertext bytes
**/
func DESEncode(src, key, iv []byte) ([]byte, error) {
	block, err := des.NewCipher(key)
	if err != nil {
		return nil, err
	}
	blocklen := block.BlockSize()
	if blocklen != len(iv) {
		return nil, errors.New("IV length must equal block size")
	}
	src = PKCS5Padding(src, blocklen)
	cbc := cipher.NewCBCEncrypter(block, iv)
	dst := make([]byte, len(src))
	cbc.CryptBlocks(dst, src)
	return dst, nil
}

/**
DES Decrypt: mode: CBC
	the src is the Ciphertext bytes to decode
	the key length must be 8,
	and the iv length must equal block size: 8,
	return Plaintext bytes
**/
func DESDecode(src, key, iv []byte) ([]byte, error) {
	block, err := des.NewCipher(key)
	if err != nil {
		return nil, err
	}
	blocklen := block.BlockSize()
	if blocklen != len(iv) {
		return nil, errors.New("IV length must equal block size")
	}
	cbc := cipher.NewCBCDecrypter(block, iv)
	dst := make([]byte, len(src))
	cbc.CryptBlocks(dst, src)
	return PKCS5UnPadding(dst), nil
}
