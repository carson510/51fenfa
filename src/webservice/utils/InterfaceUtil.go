package util

import "encoding/json"

type interfaceUtil struct {
}

var InterfaceUtil interfaceUtil

func (this *interfaceUtil) ToJsonString(v interface{}) string {
	j, _ := json.Marshal(v)
	return string(j)
}

func (this *interfaceUtil) FromJsonString(jsonStr string, v interface{}) error {
	return json.Unmarshal([]byte(jsonStr), v)
}
