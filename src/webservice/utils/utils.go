package util

import (
	"crypto/md5"
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"io"
	"net/url"
	"os"
	"strings"
	"webservice/config"
)

// url encode string, is + not %20
func UrlEncode(str string) string {
	s := url.QueryEscape(str)
	return s
}

// url decode string
func UrlDecode(str string) (string, error) {
	return url.QueryUnescape(str)
}

// base64 encode
func Base64Encode(str string) string {
	return base64.StdEncoding.EncodeToString([]byte(str))
}

// base64 decode
func Base64Decode(str string) (string, error) {
	s, e := base64.StdEncoding.DecodeString(str)
	return string(s), e
}

// Compress a string to a list of output symbols.
func LZWcompress(uncompressed string) []int {
	// Build the dictionary.
	dictSize := 256
	dictionary := make(map[string]int)
	for i := 0; i < 256; i++ {
		dictionary[string(i)] = i
	}

	w := ""
	result := make([]int, 0)
	for _, c := range []byte(uncompressed) {
		wc := w + string(c)
		if _, ok := dictionary[wc]; ok {
			w = wc
		} else {
			result = append(result, dictionary[w])
			// Add wc to the dictionary.
			dictionary[wc] = dictSize
			dictSize++
			w = string(c)
		}
	}

	// Output the code for w.
	if w != "" {
		result = append(result, dictionary[w])
	}
	return result
}

// Decompress a list of output ks to a string.
func LZWdecompress(compressed []int) string {
	// Build the dictionary.
	dictSize := 256
	dictionary := make(map[int]string)
	for i := 0; i < 256; i++ {
		dictionary[i] = string(i)
	}

	w := string(compressed[0])
	result := w
	for _, k := range compressed[1:] {
		var entry string
		if x, ok := dictionary[k]; ok {
			entry = x
		} else if k == dictSize {
			entry = w + w[:1]
		} else {
			panic(fmt.Sprintf("Bad compressed k: %d", k))
		}

		result += entry

		// Add w+entry[0] to the dictionary.
		dictionary[dictSize] = w + entry[:1]
		dictSize++

		w = entry
	}
	return result
}

func IsPathExists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err
}

func FindStrPos(s string, array []string) int {
	//fmt.Println("**** s")
	//fmt.Println(s)
	//fmt.Println(array)
	//fmt.Println("**** e")
	for p, v := range array {
		if v == s {
			return p
		}
	}
	return -1
}

func GUID() string {
	b := make([]byte, 32)
	if _, err := io.ReadFull(rand.Reader, b); err != nil {
		return ""
	}
	return base64.URLEncoding.EncodeToString(b)
}

// MD5 Encode
func Md5(src []byte) []byte {
	h := md5.New()
	h.Write(src)
	return h.Sum(nil)
}

func ActivateURLEncode(url string) (res string) {
	key := []byte(config.EmailDESkey)
	lv := []byte(config.EmalDESLV)

	srcbytes := []byte(url)
	destbytes, err := DESEncode(srcbytes, key, lv)
	if err == nil {
		deststr := string(destbytes)
		res = UrlEncode(deststr)
		fmt.Println(res)

	} else {
		fmt.Println("ActivateURLEncode encode error:")
		fmt.Println(err)
	}
	return
}

func ActivateURLDecode(url string) (res string) {
	key := []byte(config.EmailDESkey)
	lv := []byte(config.EmalDESLV)

	decodeUrl, err := UrlDecode(url)
	if err == nil {
		decodebytes := []byte(decodeUrl)
		urlbytes, err := DESDecode(decodebytes, key, lv)
		if err == nil {
			res = string(urlbytes)
			fmt.Println(res)
		} else {
			fmt.Println("DESDecode error!")
			fmt.Println(err)
		}
	} else {
		fmt.Println("URLDecode error!")
		fmt.Println(err)
	}

	return

}

func MD5WithSalt(pass string) (salt, newpass string) {
	salt = GUID()
	newpass = string(Md5([]byte(pass + salt)))
	return
}

func RepleaceVariable(str string, values map[string]string) string {
	for key, value := range values {
		str = strings.Replace(str, "$$"+key+"$$", value, -1)
	}
	return str
}

func GetDownloadFullUrl(url, name string) string {
	return CombineURL(config.GetHost(), url)
}

func CombineURL(host, url string) string {
	if strings.HasPrefix(url, "http://") || strings.HasPrefix(url, "https://") {
		return url
	}
	if !strings.HasPrefix(host, "http://") && !strings.HasPrefix(host, "https://") {
		host = "http://" + host
	}
	if strings.HasSuffix(host, "/") {
		if strings.HasPrefix(url, "/") {
			url = url[1:]
		}
	} else {
		if !strings.HasPrefix(url, "/") {
			url = "/" + url
		}
	}
	return host + url
}
