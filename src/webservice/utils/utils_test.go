package util

import (
	"fmt"
	//"github.com/astaxie/beego"
	"net/url"
	//"os"
	"bytes"
	"html"
	"testing"
	"text/template"
)

var oriurl = "user=huzhiqiang@kingsoft.com&key=ewqfrsdfsd"

func TestActivateURLEncodeDecode(t *testing.T) {
	encode := ActivateURLEncode(oriurl)
	fmt.Println(encode)
	decode := ActivateURLDecode(encode)
	fmt.Println(decode)

	m, _ := url.ParseQuery(decode)
	fmt.Println(m)
	fmt.Println(m["user"][0])
	fmt.Println(m["key"][0])

}

func TestTemplate(t *testing.T) {
	fmt.Println("TestTemplate")
	var data map[string]string
	data = make(map[string]string, 2)
	data["name"] = "huzhiqiang@hotmail.com"
	data["url"] = "www.sohu.com"

	var mailContent string = "<html><head></head><body><div><p>{{.name}}，你好！</p><p>请点击下面链接验证来完成讯测注册：</p><p><a href='{{.url}}''>{{.url}}</a></p><p>如果你没有注册讯测账号，请忽略此邮件。</p></div><br><br></body></html>"

	s1, err := template.New("test").Parse(mailContent)
	if err != nil {
		fmt.Println(err)
	} else {
		b := new(bytes.Buffer)
		err = s1.Execute(b, data)
		if err != nil {
			return
		}
		result := string(b.String())

		result = html.UnescapeString(result)

		fmt.Println(result)
	}

}

func TestMD5WithSalt(t *testing.T) {
	salt, newpass := MD5WithSalt("Password")
	fmt.Println("salt:" + salt)
	fmt.Println("Password:" + newpass)
	fmt.Println("old Password:" + string(Md5([]byte("Password"))))
}
