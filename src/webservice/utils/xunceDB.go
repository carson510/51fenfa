package util

import (
	"github.com/yaosxi/mgox"
	"gopkg.in/mgo.v2"
)

func WithDBContext(fn func(db *mgo.Database) error) error {

	dbSession, err := mgo.Dial(mgox.DBConfig.Host)
	if err != nil {
		return err
	}

	defer dbSession.Close()
	dbSession.SetMode(mgo.Monotonic, true)

	database := dbSession.DB(mgox.DBConfig.Database)
	loginErr := database.Login(mgox.DBConfig.Username, mgox.DBConfig.Password)

	if loginErr != nil {
		return loginErr
	}

	return fn(database)
}
