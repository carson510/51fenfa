package util

import (
	"strings"
)

func GetProvisionedDevices(emp string) []string {
	//lines := strings.Split(emp, "\n")
	//for _, line := range lines {
	//	println(line)
	//}

	idx := strings.Index(emp, `<key>ProvisionedDevices</key>`)
	if idx > 0 {
		emp = emp[idx:]
		idx = strings.Index(emp, "</array>")
		if idx > 0 {
			emp = emp[39:idx]
			emp = strings.Replace(emp, " ", "", -1)
			emp = strings.Replace(emp, "\t", "", -1)
			emp = strings.Replace(emp, "<string>", "", -1)
			emp = strings.Replace(emp, "</string>", "", -1)
			_udids := strings.Split(emp, "\n")
			udids := []string{}
			for _, udid := range _udids {
				if udid != "" {
					udids = append(udids, udid)
				}
			}
			return udids
		}
	}
	return []string{}
}
