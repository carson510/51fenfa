package util

import (
	"io/ioutil"
	"strings"
	"webservice/model"
	"time"
	"strconv"
	"webservice/storage"
)

var plist_template string

func GetPlistFromApp(app model.App, serviceHost, downloadHost string) string {

	if plist_template == "" {
		b, err := ioutil.ReadFile("conf/manifest.plist")
		if err != nil {
			panic(err)
			return ""
		}
		plist_template = string(b)
	}
	t := strconv.FormatInt(time.Now().UnixNano(), 10)
	if strings.HasSuffix(serviceHost, ":80") {
		serviceHost = strings.Replace(serviceHost, ":80", "", -1)
	}
	url := "http://" + serviceHost + "/service/app/dlog/starting?appid=" + app.Id.Hex() + "&downloadlogid=" + app.DownloadLogId + "&t=" + t

	logoUrl := storage.StorageHelper.GetDownloadUrl(downloadHost, app.LogoUrl)
	values := map[string]string{
		"software-package":  strings.Replace(url, "&", "&amp;", -1),
		"display-image":     logoUrl + "?t=" + t,
		"full-size-image":   logoUrl+ "?t=" + t,
		"bundle-identifier": app.PackageName,
		"bundle-version":    app.ReleaseVersion.Package.Version,
		"subtitle":          app.AppName,
		"title":             app.AppName,
	}

	return RepleaceVariable(plist_template, values)
}
