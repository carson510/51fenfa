package util

import (
	"fmt"
	"github.com/bitly/go-simplejson"
	"github.com/mssola/user_agent"
	"net/http"
	"strconv"
	"strings"
	"webservice/model"
	"github.com/kimiazhu/log4go"
)

// "[::1]:58292" => "[::1]"
func ipAddrFromRemoteAddr(s string) string {
	idx := strings.LastIndex(s, ":")
	if idx == -1 {
		return s
	}
	return s[:idx]
}

func GetIpAddress(r *http.Request) string {
	hdr := r.Header
	hdrRealIp := hdr.Get("X-Real-Ip")
	hdrForwardedFor := hdr.Get("X-Forwarded-For")
	remoteAddr := ipAddrFromRemoteAddr(r.RemoteAddr)
	//log4go.Debug("X-Forwarded-For: %s  X-Real-Ip: %s  RemoteAddr: %s", hdrForwardedFor, hdrRealIp, remoteAddr)
	if hdrRealIp == "" && hdrForwardedFor == "" {
		return remoteAddr
	}
	if hdrForwardedFor != "" {
		// X-Forwarded-For is potentially a list of addresses separated with ","

		parts := strings.Split(hdrForwardedFor, ",")
		for i, p := range parts {
			parts[i] = strings.TrimSpace(p)
		}
		// return first non-local address
		return parts[0]
	}
	return hdrRealIp
}

func GetLatestForwardIpAddress(r *http.Request) string {
	hdr := r.Header
	hdrRealIp := hdr.Get("X-Real-Ip")
	hdrForwardedFor := hdr.Get("X-Forwarded-For")
	remoteAddr := ipAddrFromRemoteAddr(r.RemoteAddr)
	log4go.Debug("X-Forwarded-For: %s  X-Real-Ip: %s  RemoteAddr: %s", hdrForwardedFor, hdrRealIp, remoteAddr)
	if hdrRealIp == "" && hdrForwardedFor == "" {
		return remoteAddr
	}
	if hdrForwardedFor != "" {
		// X-Forwarded-For is potentially a list of addresses separated with ","
		parts := strings.Split(hdrForwardedFor, ",")
		for i, p := range parts {
			parts[i] = strings.TrimSpace(p)
		}
		if len(parts) > 1 {
			return parts[len(parts) - 1]
		}
	}
	return ""
}

func GetClientInfo(request *http.Request) *model.Client {
	var client model.Client
	client.IP = GetIpAddress(request)
	if strings.ToLower(request.UserAgent()) == "tako" {
		client_info_str := fmt.Sprintf("%s", request.Header["Client-Info"])
		client_info_str = strings.TrimSpace(client_info_str)
		if strings.HasPrefix(client_info_str, "[") {
			r := []rune(client_info_str)
			client_info_str = string(r[1 : len(client_info_str)-1])
		}
		if strings.HasSuffix(client_info_str, "]") {
			r := []rune(client_info_str)
			client_info_str = string(r[0 : len(client_info_str)-1])
		}
		client_js, _ := simplejson.NewJson([]byte(client_info_str))
		client.Platform, _ = client_js.Get("platform").String()
		client.DeviceName, _ = client_js.Get("device_name").String()
		client.OsName, _ = client_js.Get("os_name").String()
		client.OsVersion, _ = client_js.Get("os_version").String()
		client.Name = "Tako"
		client.Version, _ = client_js.Get("client_version").String()
		client_build, _ := client_js.Get("client_build").Int()
		client.Build = strconv.Itoa(client_build)
	} else {
		ua := user_agent.New(request.UserAgent())
		client.Platform = ua.Platform()
		client.OsName = ua.OS()
		client.Name, client.Version = ua.Browser()
	}
	return &client
}
