package controllers

import (
	"webservice/service"
	"webservice/model"
	"webservice/storage"
)

type FileStorageController struct {
	BaseController
}

func (this *FileStorageController) secure(val string) string {
	if val == "" {
		return val
	}
	return "*"
}

func (this *FileStorageController) GetMy() {
	var storages []model.FileStorage
	err := service.GetMyFileStorage(this.GetUserId(), &storages)
	if err != nil {
		this.error(err)
		return
	}
	this.data(storages, "storages")
}

func (this *FileStorageController) Add() {
	var storage model.FileStorage
	if err := this.bind(&storage); err != nil {
		this.error(err)
		return
	}
	storage.UserId = this.GetUserId()
	this.error(service.AddFileStorage(&storage))
}

func (this *FileStorageController) Delete() {
	this.error(service.DeleteFileStorage(this.GetString(":id")))
}

func (this *FileStorageController) SetDefault() {
	this.error(service.SetDefaultFileStorage(this.GetUserId(), this.GetString(":id")))
}

func (this *FileStorageController) GetDefault() {
	var fs model.FileStorage
	err := service.GetDefaultFileStorage(this.GetUserId(), &fs)
	if err != nil {
		this.error(err)
		return
	}
	storage.StorageHelper.SetConfig(&fs)
	this.data(fs, "storage")
}
