package controllers

import (
	"fmt"
	"github.com/kimiazhu/log4go"
	"github.com/astaxie/beego"
	"github.com/bitly/go-simplejson"
	"github.com/yaosxi/mgox"
	"net/http"
	"strconv"
	"strings"
	"webservice/constant"
	"encoding/json"
	"qiniupkg.com/x/errors.v7"
)

type BaseController struct {
	beego.Controller
}

func (this *BaseController) GetRequestBody() (*simplejson.Json, error) {
	postJson := fmt.Sprintf("%s", this.Ctx.Input.RequestBody)
	log4go.Info(fmt.Sprintf("Request Body is: %s", postJson))
	js, err := simplejson.NewJson([]byte(postJson))
	if err != nil {
		log4go.Error("Error occur: ", err)
		return nil, err
	}
	return js, nil
}

func (this *BaseController) GetUserIdAndToken() (string, string) {
	return this.GetUserId(), this.GetToken()
}

func (this *BaseController) GetUserId() string {
	userid := this.Ctx.GetCookie("userid")
	if userid == "" {
		userid = this.Ctx.Input.Query("userid")
	}
	if userid == "" {
		var postJson string
		postJson = fmt.Sprintf("%s", this.Ctx.Input.RequestBody)
		js, err := simplejson.NewJson([]byte(postJson))
		if err == nil {
			userid, _ = js.Get("userid").String()
		}
	}
	return userid
}

func (this *BaseController) GetToken() string {
	token := this.Ctx.GetCookie("token")
	if token == "" {
		token = this.Ctx.Input.Query("token")
	}
	if token == "" {
		var postJson string
		postJson = fmt.Sprintf("%s", this.Ctx.Input.RequestBody)
		js, err := simplejson.NewJson([]byte(postJson))
		if err == nil {
			token, _ = js.Get("token").String()
		}
	}
	return token
}

func (this *BaseController) bind(data interface{}) error {
	var postJson string
	postJson = fmt.Sprintf("%s", this.Ctx.Input.RequestBody)
	if postJson == "" {
		return errors.New("empty body")
	}
	log4go.Debug(postJson)
	return json.Unmarshal([]byte(postJson), data)
}

func (this *BaseController) SessioinIllegal() {
	var result Response
	result.Message = constant.Info_Error_SessionIllegal
	result.Ret = constant.Code_Error_SessionIllegal
	this.Ctx.WriteString(result.FormatJsonString())
}

func (this *BaseController) NoPermissionError() {
	var result Response
	result.Message = constant.Info_Error_NoPermission
	result.Ret = constant.Code_Error_NoPermission
	this.Ctx.WriteString(result.FormatJsonString())
}

func (this *BaseController) ParamError(tag string) {
	var result Response
	result.Message = constant.Info_Error_ParamError
	result.Ret = constant.Code_Error_ParamError
	result.Tag = tag
	this.Ctx.WriteString(result.FormatJsonString())
}

func (this *BaseController) duplicate(tag string) {
	var result Response
	result.Message = constant.Info_Error_DuplicatedInfo
	result.Ret = constant.Code_Error_DuplicatedInfo
	result.Tag = tag
	this.Ctx.WriteString(result.FormatJsonString())
}

func (this *BaseController) nonexist(tag string) {
	var result Response
	result.Message = constant.Info_Error_NotFound
	result.Ret = constant.Code_Error_NotFound
	result.Tag = tag
	this.Ctx.WriteString(result.FormatJsonString())
}

func (this *BaseController) wrong(tag string) {
	var result Response
	result.Message = constant.Info_Error_Wrong
	result.Ret = constant.Code_Error_Wrong
	result.Tag = tag
	this.Ctx.WriteString(result.FormatJsonString())
}

func (this *BaseController) disabled(tag string) {
	var result Response
	result.Message = constant.Info_Error_Disabled
	result.Ret = constant.Code_Error_Disabled
	result.Tag = tag
	this.Ctx.WriteString(result.FormatJsonString())
}

//func (this *BaseController) error2(code int, msg string) {
//	log4go.Error("*************** ERROR ***************")
//	log4go.Error(fmt.Sprintf("Error: code = %d, message = %s", code, msg))
//	log4go.Error("*************** ERROR ***************")
//	resp := Response{Message: msg, Ret: code}
//	this.Ctx.WriteString(resp.FormatJsonString())
//}
//
//func (this *BaseController) error3(code int, msg string, tag string) {
//	log4go.Error("*************** ERROR ***************")
//	log4go.Error(fmt.Sprintf("Error: code = %d, message = %s, tag = %s", code, msg, tag))
//	log4go.Error("*************** ERROR ***************")
//	resp := Response{Message: msg, Ret: code, Tag: tag}
//	this.Ctx.WriteString(resp.FormatJsonString())
//}

func (this *BaseController) error(err error) {
	if err == nil {
		this.ok()
		return
	} else {
		log4go.Error("*************** ERROR ***************")
		log4go.Error(err)
		log4go.Error("*************** ERROR ***************")
	}

	errMsg := err.Error()
	if strings.HasPrefix(errMsg, strconv.Itoa(constant.Code_Error_DuplicatedInfo)+":") {
		this.duplicate(errMsg[len(strconv.Itoa(constant.Code_Error_DuplicatedInfo)):])
		return
	}

	if strings.HasPrefix(errMsg, strconv.Itoa(constant.Code_Error_NoPermission)+":") {
		this.NoPermissionError()
		return
	}

	var result Response
	result.Message = err.Error()
	if err.Error() == "not found" {
		result.Ret = constant.Code_Error_NotFound
	} else {
		result.Ret = constant.Code_Error_OperationFailed
	}
	this.Ctx.WriteString(result.FormatJsonString())
}

// 通用返回Response的方法。必须制定code和msg。
// 可选参数是Tag和返回Data对象。最多四个参数。
func (this *BaseController) response(code int, msg string, args ...interface{}) {
	var tag string
	var data interface{}
	switch len(args) {
	case 0:
	case 1:
		var ok bool
		if tag, ok = args[0].(string); ok {
			//tag is signed, do nothing else
		} else {
			tag = ""
		}
	case 2:
		data = args[1]
	default:
		log4go.Debug(fmt.Sprintf("with() received too many arguments %d", len(args)))
	}
	if code != constant.Code_Success {
		log4go.Error("*************** ERROR ***************")
		log4go.Error(fmt.Sprintf("Error: code = %d, message = %s, tag = %s", code, msg, tag))
		log4go.Error("*************** ERROR ***************")
	}
	resp := Response{Message: msg, Ret: code, Tag: tag, Data: data}
	this.Ctx.WriteString(resp.FormatJsonString())
}

// Deprecated -> data
func (this *BaseController) success(data interface{}) {
	this.data(data, "")
}

func (this *BaseController) dataIfNoError(err error, data interface{}, tag string) {
	if err != nil {
		this.error(err)
	} else {
		this.data(data, tag)
	}
}

func (this *BaseController) data(data interface{}, tag string) {
	this.pageList(data, nil, tag)
}

func (this *BaseController) pageList(list interface{}, p *mgox.Page, tag string) {
	var result Response
	result.Ret = constant.Code_Success
	result.Message = constant.Info_Success
	if list != nil {
		if p != nil && p.Count > 0 {
			returnData := make(map[string]interface{})
			returnData["list"] = list
			returnData["page"] = p
			result.Data = returnData
		} else {
			result.Data = list
		}
	}
	result.Tag = tag
	this.Ctx.WriteString(result.FormatJsonString())
}

func (this *BaseController) ok() {
	this.data(nil, "")
}

func (this *BaseController) expired(tag string) {
	var result Response
	result.Ret = constant.Code_Error_Expired
	result.Message = constant.Info_Error_Expired
	result.Tag = tag
	this.Ctx.WriteString(result.FormatJsonString())
}

func (this *BaseController) redirect(location string) {
	this.Redirect(location, http.StatusFound)
}
