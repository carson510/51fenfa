package controllers

import (
	"fileserver/service"
)

type FileServerController struct {
	BaseController
}

func (this *FileServerController) Upload() {
	service.Receiver(this.Ctx.ResponseWriter, this.Ctx.Request)
}

func (this *FileServerController) RemoveFile() {
	service.RemoveFile(this.Ctx.ResponseWriter, this.Ctx.Request)
}

func (this *FileServerController) DiskUsage() {
	service.DiskUsage(this.Ctx.ResponseWriter, this.Ctx.Request)
}