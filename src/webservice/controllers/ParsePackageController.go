package controllers

import (
	"archive/zip"
	"bytes"
	"encoding/base64"
	"fmt"
	"github.com/DHowett/go-plist"
	log "github.com/kimiazhu/log4go"
	"github.com/bitly/go-simplejson"
	"github.com/nu7hatch/gouuid"
	"io/ioutil"
	"os"
	"os/exec"
	"path"
	"reflect"
	"runtime"
	"strconv"
	"strings"
	"webservice/model"
	"webservice/utils"
	"webservice/service"
)

const UNZIP_APP_PACKAGE_TEMP_PATH = "static/app/parse_app_temp"

type ParsePackageController struct {
	BaseController
}

func (this *ParsePackageController) Get() {

}

func (this *ParsePackageController) Post() {

	userid := this.GetUserId()
	if userid != "" {
		expired, err := service.IsExpiredUesr(userid)
		if err != nil {
			this.error(err)
			return
		}
		if expired {
			this.expired("user")
			return
		}
	}

	var postJson string
	postJson = fmt.Sprintf("%s", this.Ctx.Input.RequestBody)
	if postJson == "" {
		this.ParamError("body")
		return
	}

	js, err := simplejson.NewJson([]byte(postJson))
	if err != nil {
		this.ParamError("body")
		return
	}

	platform, _ := js.Get("platform").Int()

	info_plist_str, _ := js.Get("plist").String()
	info_plist_data, _ := base64.StdEncoding.DecodeString(info_plist_str)

	manifest_str, _ := js.Get("manifest").String()
	manifest_data, _ := base64.StdEncoding.DecodeString(manifest_str)
	arsc_str, _ := js.Get("arsc").String()
	arsc_data, _ := base64.StdEncoding.DecodeString(arsc_str)

	var package_info model.PackageInfo

	_platform := model.PlatformType(platform)

	if _platform == model.Platform_iOS {
		package_info.Platform = model.Platform_iOS
		plistMap := make(map[string]interface{})

		_, err := plist.Unmarshal(info_plist_data, &plistMap)
		if err != nil {
			this.ParamError("body")
			return
		}

		if v, ok := plistMap["CFBundleDisplayName"]; ok {
			package_info.AppName = fmt.Sprintf("%s", v)
		}

		if v, ok := plistMap["CFBundleIdentifier"]; ok {
			package_info.BundleId = fmt.Sprintf("%s", v)
		}

		if v, ok := plistMap["CFBundleShortVersionString"]; ok {
			package_info.Version = fmt.Sprintf("%s", v)
			if v, ok = plistMap["CFBundleVersion"]; ok {
				package_info.BuildNumber, _ = strconv.Atoi(fmt.Sprintf("%s", v))
			}
		} else if v, ok := plistMap["CFBundleVersion"]; ok {
			package_info.Version = fmt.Sprintf("%s", v)
		}

		if v, ok := plistMap["CFBundleName"]; ok {
			package_info.BundleName = fmt.Sprintf("%s", v)
		}

		if v, ok := plistMap["MinimumOSVersion"]; ok {
			package_info.MiniOSVersion = fmt.Sprintf("%s", v)
		}

		if v, ok := plistMap["CFBundleExecutable"]; ok {
			package_info.Executable = fmt.Sprintf("%s", v)
		}

		if v, ok := plistMap["UIDeviceFamily"]; ok {

			ref := reflect.ValueOf(v)
			UIDeviceFamily := ref.Interface().([]interface{})

			deviceFamilyArray := make([]string, 0)
			for _, deviceFamily := range UIDeviceFamily {

				deviceFamilyString := fmt.Sprintf("%d", deviceFamily)
				if deviceFamilyString == "1" {
					deviceFamilyArray = append(deviceFamilyArray, "iPhone")
					deviceFamilyArray = append(deviceFamilyArray, "iPod Touch")
				}

				if deviceFamilyString == "2" {
					deviceFamilyArray = append(deviceFamilyArray, "iPad")
				}
			}

			package_info.DeviceFamily = deviceFamilyArray
		}

		app_logo_name := ""
		if v, ok := plistMap["CFBundleIconFiles"]; ok {
			ref := reflect.ValueOf(v)
			bundIcons := ref.Interface().([]interface{})

			if len(bundIcons) > 0 {
				app_logo_name = fmt.Sprintf("%s", bundIcons[len(bundIcons)-1])
			}
		}

		if app_logo_name == "" {
			if v, ok := plistMap["CFBundleIcons"]; ok {

				ref := reflect.ValueOf(v)
				bundleicons_map := ref.Interface().(map[string]interface{})

				if primaryIcon, ok := bundleicons_map["CFBundlePrimaryIcon"]; ok {

					ref := reflect.ValueOf(primaryIcon)
					icons := ref.Interface().(map[string]interface{})

					if bundleIcon, ok := icons["CFBundleIconFiles"]; ok {
						ref := reflect.ValueOf(bundleIcon)
						bundIcons := ref.Interface().([]interface{})

						if len(bundIcons) > 0 {
							app_logo_name = fmt.Sprintf("%s", bundIcons[len(bundIcons)-1])
						}
					}
				}
			}
		}

		package_info.Logo = app_logo_name

		if package_info.AppName == "" {
			package_info.AppName = package_info.BundleName
		}

		if package_info.AppPackage == "" {
			package_info.AppPackage = package_info.BundleId
		}

	} else if _platform == model.Platform_Android {

		apkPath := rebuildApkWithManifestAndResources(manifest_data, arsc_data)
		package_info, err = parseApkToPackage(apkPath)
		if err != nil {
			this.error(err)
			return
		}

		os.Remove(apkPath)

		package_info.Platform = model.Platform_Android

	}

	this.data(package_info, "package")
}

func parseApkWithAppt(apk string) (string, error) {

	execapp := "./3rd/"

	osType := runtime.GOOS
	if osType == "linux" {
		execapp += "aapt_linux"
	} else if osType == "darwin" {
		execapp += "aapt_draw64"
	} else if osType == "windows" {
		execapp += "aapt.exe"
	}

	cmd := exec.Command(execapp, "dump", "badging", apk)

	var out bytes.Buffer
	cmd.Stdout = &out
	err := cmd.Run()
	if err != nil {
		log.Error(err)
		return "", nil
	}

	return out.String(), nil
}

func parseApkInfo(apk string) (map[string]string, error) {
	result := make(map[string]string)

	apkinfo_string, err := parseApkWithAppt(apk)
	if err != nil || apkinfo_string == "" {
		return result, err
	}

	valueArray := strings.Split(apkinfo_string, "\n")

	for _, item := range valueArray {
		keyAndValue := strings.Split(item, ":")
		if keyAndValue[0] == "package" ||
		keyAndValue[0] == "application" ||
		keyAndValue[0] == "sdkVersion" {
			if len(keyAndValue) == 2 {
				result[keyAndValue[0]] = keyAndValue[1]
			}
		}
		if strings.HasPrefix(keyAndValue[0], "application-icon") {
			result["application-icon"] = keyAndValue[1]
		}
	}

	return result, nil
}

func removeSingleQuote(value string) string {
	return strings.Replace(strings.Replace(value, "'", "", -1), "\r", "", -1)
}

func parseApkToPackage(apk string) (model.PackageInfo, error) {
	var packageInfo model.PackageInfo

	apkInfoMap, err := parseApkInfo(apk)
	if err != nil {
		return packageInfo, err
	}

	packageValue := apkInfoMap["package"]
	applicationValue := apkInfoMap["application"]
	packageInfo.MiniSDKVersion = removeSingleQuote(apkInfoMap["sdkVersion"])
	packageInfo.Logo = removeSingleQuote(apkInfoMap["application-icon"])

	packageValueArray := strings.Split(packageValue, " ")
	for _, packageItemValue := range packageValueArray {
		packageItemValueArray := strings.Split(packageItemValue, "=")
		if len(packageItemValueArray) != 2 {
			continue
		}

		if packageItemValueArray[0] == "name" {
			packageInfo.AppPackage = removeSingleQuote(packageItemValueArray[1])
			continue
		}

		if packageItemValueArray[0] == "versionName" {
			packageInfo.Version = removeSingleQuote(packageItemValueArray[1])
			continue
		}

		if packageItemValueArray[0] == "versionCode" {
			packageInfo.BuildNumber, _ = strconv.Atoi(removeSingleQuote(packageItemValueArray[1]))
			continue
		}

	}

	applicationValueArray := strings.Split(applicationValue, " ")
	for _, applicationItemValue := range applicationValueArray {
		applicationItemValueArray := strings.Split(applicationItemValue, "=")
		if len(applicationItemValueArray) != 2 {
			continue
		}

		if applicationItemValueArray[0] == "label" {
			packageInfo.AppName = removeSingleQuote(applicationItemValueArray[1])
			continue
		}

		if packageInfo.Logo == "" && applicationItemValueArray[0] == "icon" {
			packageInfo.Logo = removeSingleQuote(applicationItemValueArray[1])
			continue
		}

	}

	return packageInfo, nil
}

func rebuildApkWithManifestAndResources(manifest, resources []byte) string {
	apkPath := ""

	u, _ := uuid.NewV4()

	dir, _ := os.Getwd()
	dir = path.Join(dir, UNZIP_APP_PACKAGE_TEMP_PATH)

	exists, _ := util.IsPathExists(dir)
	if !exists {

		err := os.MkdirAll(dir, os.ModePerm) // 生成多级目录
		if err != nil {
			return ""
		}
	}

	apkPath = path.Join(dir, u.String()+".apk")

	// Create a buffer to write our archive to.
	buf := new(bytes.Buffer)

	// Create a new zip archive.
	w := zip.NewWriter(buf)

	var files = []struct {
		Name string
		Body []byte
	}{
		{"AndroidManifest.xml", manifest},
		{"resources.arsc", resources},
	}

	for _, file := range files {
		f, err := w.Create(file.Name)
		if err != nil {
			log.Error(err)
		}
		_, err = f.Write(file.Body)
		if err != nil {
			log.Error(err)
		}
	}

	// Make sure to check the error on Close.
	err := w.Close()
	if err != nil {
		log.Error(err)
	}

	err = ioutil.WriteFile(apkPath, buf.Bytes(), 0777)

	return apkPath
}
