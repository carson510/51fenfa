package controllers

import (
	"encoding/json"
)

type Response struct {
	Message string      `json:"message"`
	Ret     int         `json:"ret"`
	Tag     string      `json:"tag,omitempty"`
	Data    interface{} `json:"data,omitempty"`
}

func (r *Response) FormatJsonString() string {
	result, err := json.Marshal(r)
	if err != nil {
		return ""
	}

	return string(result)
}
