package controllers

import (
	"net/http"
	"webservice/service"
	"time"
)

type CheckLoginController struct {
	BaseController
}

func (this *CheckLoginController) Post() {
	userid, token := this.GetUserIdAndToken()
	if userid == "" || token == "" {
		this.ParamError("")
		return
	}
	session := service.GetSessionInfoFromCache(token)
	if session.Token == "" || session.UserId != userid {
		this.SessioinIllegal()
	} else {
		service.CreateLoginLog(userid, this.Ctx.Request)
		expiration := time.Now()
		expiration = expiration.AddDate(1, 0, 0)
		userid_cookie := http.Cookie{Name: "userid", Value: session.UserId, Expires: expiration}
		token_cookie := http.Cookie{Name: "token", Value: session.Token, Expires: expiration}
		username_cookie := http.Cookie{Name: "username", Value: session.UserName, Expires: expiration}
		http.SetCookie(this.Ctx.ResponseWriter, &userid_cookie)
		http.SetCookie(this.Ctx.ResponseWriter, &token_cookie)
		http.SetCookie(this.Ctx.ResponseWriter, &username_cookie)
		this.success(nil)
	}

}
