package controllers

import (
	"github.com/yaosxi/mgox"
	"gopkg.in/mgo.v2/bson"
	"strings"
	"webservice/model"
	"time"
	"github.com/kimiazhu/log4go"
	"net/http"
	"webservice/service"
	"webservice/storage"
)

type DownloadLogController struct {
	BaseController
}

func (this *DownloadLogController) Starting() {
	//this.EnableRender = false
	appId := this.GetString("appid")
	dLogId := this.GetString("downloadlogid")
	log4go.Debug("starting donwload(id=%s): %s", dLogId, appId)

	dao := mgox.Connect().ShareError()
	defer dao.Close()
	var app model.App
	dao.GetById(appId).Result(&app)
	if dao.LastError != nil {
		this.error(dao.LastError)
		return
	}
	service.SetDownloadStartingTime(dLogId);
	url := storage.StorageHelper.GetDownloadUrl(service.GetStorageHost(app.StorageId), app.ReleaseVersion.Package.Url)
	//if strings.Index(url, "?") > 0 {
	//	url += "&"
	//} else {
	//	url += "?"
	//}
	//t := strconv.FormatInt(time.Now().UnixNano(), 10)
	//url += "t=" + t + "&downloadlogid=" + dLogId
	log4go.Debug("redirect to: " + url)
	this.redirect(url)
}
//func (this *DownloadLogController) Started() {
//
//	this.EnableRender = false
//
//	uri := this.GetString("uri")
//	dLogId := this.GetString("downloadlogid")
//	log4go.Debug("starting donwload(id=%s): %s", dLogId, uri)
//
//	if dLogId != "" {
//		if err := service.SetDownloadStartingTime(dLogId); err != nil {
//			this.error(err)
//			return
//		}
//	}
//
//	log4go.Debug("X-Accel-Redirect: " + uri + "?downloadlogid=" + dLogId)
//
//	cd := this.GetString("response-content-disposition")
//	log4go.Debug("Content-Disposition: " + cd)
//	if cd != "" {
//		this.Ctx.ResponseWriter.Header().Add("Content-Disposition", cd)
//	}
//	this.Ctx.ResponseWriter.Header().Add("Content-Type", "application/octet-stream")
//	this.Ctx.ResponseWriter.Header().Add("X-Accel-Redirect", uri + "?downloadlogid=" + dLogId)
//
//}

func (this *DownloadLogController) IsStarted() {
	dLogId := this.GetString("downloadlogid")
	log4go.Debug("downloadlogid=" + dLogId)
	var dlog model.DownloadLog
	err := mgox.New().GetById(dLogId).Result(&dlog)
	if err != nil {
		this.error(err)
		return
	}
	if dlog.Status == model.DownloadStatus_Begin {
		this.ok()
	} else {
		this.nonexist("")
	}
}

func (this *DownloadLogController) Completed() {

	this.EnableRender = false

	uri := this.GetString("uri")
	dLogId := this.GetString("downloadlogid")
	status := this.GetString("status")
	bytesent, _ := this.GetInt64("bytesent", 0)
	log4go.Debug("completed(%s,%s) donwload(id=%s): %s", status, bytesent, dLogId, uri)

	if dLogId == "" {
		this.ParamError("id")
		return
	}

	userId := this.GetUserId()

	dao := mgox.Connect(userId).ShareError()
	defer dao.Close()

	var dlog model.DownloadLog
	err := dao.Get(bson.ObjectIdHex(dLogId)).Result(&dlog)
	if err != nil {
		this.error(err)
		return
	}

	endTime := time.Now()

	if status == http.StatusText(http.StatusOK) {
		dlog.Status = model.DownloadStatus_End
		dao.Inc(model.AppCollectionName, bson.ObjectIdHex(dlog.AppId), "downloadcount", 1)
	} else {
		dlog.Status = model.DownloadStatus_Cancel
	}

	dlog.EndTime = &endTime
	if dlog.BeginTime != nil {
		dlog.Duration = endTime.Sub(*dlog.BeginTime)
		if bytesent > 0 {
			dlog.Speed = bytesent * int64(time.Second) / int64(dlog.Duration)
		}
	}
	dao.ReplaceDoc(&dlog)

	if dao.LastError != nil {
		this.error(dao.LastError)
		return
	}
	this.ok()
}

func (this *DownloadLogController) GetAppLogs() {
	appid := this.GetString("appid")
	if appid == "" {
		this.ParamError("appid")
		return
	}
	var p mgox.Page
	p.Cursor, _ = this.GetInt("page.cursor", -1)
	p.Count, _ = this.GetInt("page.count", 0)
	var logs []model.DownloadLog
	err := mgox.New().Find("appid", appid).Sort("-viewtime").Page(&p).Result(&logs)
	if err != nil {
		this.error(err)
		return
	}
	for i := range logs {
		if logs[i].Client.IP != "" {
			ips := strings.Split(logs[i].Client.IP, ".")
			if len(ips) == 4 {
				logs[i].Client.IP = ips[0] + "." + ips[1] + ".*.*"
			} else {
				logs[i].Client.IP = "*"
			}
		}
	}
	this.pageList(logs, &p, "logs")
}
