package controllers

import (
	"strconv"
	"webservice/model"
)

type CheckUpdateController struct {
	BaseController
}

func (this *CheckUpdateController) Get() {

	pid, _ := strconv.Atoi(this.Input().Get("pid"))
	client_version := this.GetString("version")

	if client_version == "" {
		this.ParamError("version")
		return
	}

	if pid != model.Platform_iOS || pid != model.Platform_Android {
		this.ParamError("pid")
		return
	}

	updateData := ""

	if pid == model.Platform_iOS {

	} else if pid == model.Platform_Android {

	}

	this.success(updateData)

}
