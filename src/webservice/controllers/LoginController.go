package controllers

import (
	"bytes"
	"crypto/tls"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/astaxie/beego"
	"github.com/bitly/go-simplejson"
	"github.com/nu7hatch/gouuid"
	"gopkg.in/mgo.v2/bson"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"webservice/config"
	"webservice/constant"
	"webservice/model"
	"webservice/service"
	"webservice/utils"
	"time"
	"github.com/kimiazhu/log4go"
)

type LoginApiController struct {
	beego.Controller
}

func (this *LoginApiController) validationLogin(email, password, serverhost string) (bool, Response, *model.Session) {

	var result Response

	if email == "" || password == "" {
		result.Message = constant.Info_Error_UserIsInVaild
		result.Ret = constant.Code_Error_UserIsInVaild
		return false, result, nil
	}

	var resultUser model.User
	err := service.GetUserByEmail(email, &resultUser)
	if err != nil && err.Error() != "not found" {
		log4go.Error("login err: ", err)
		result.Message = constant.Info_Error_UserIsInVaild
		result.Ret = constant.Code_Error_UserIsInVaild
		return false, result, nil
	}

	passed := true

	serverip := util.GetLatestForwardIpAddress(this.Ctx.Request)
	if serverhost != "" {
		idx := strings.Index(serverhost, ":")
		if idx > 0 {
			serverip = serverhost[0:idx]
		}
	}
	if resultUser.Id == "" { // User doesn't exist
		log4go.Warn("user %s doesn't exist", email)
		passed = false
	} else if resultUser.Password != password { // Password doesn't match
		log4go.Info("%s login failed with password", email)
		passed = false
	} else if serverip != "0.0.0.0" && resultUser.ServerIp != "" && resultUser.ServerIp != serverip { // IP doesn't match
		log4go.Info("%s login failed with ip - excepted: %s, actual: %s", email, resultUser.ServerIp, serverip)
		passed = false
	}

	if !passed {
		result.Message = constant.Info_Error_UserIsInVaild
		result.Ret = constant.Code_Error_UserIsInVaild
		return false, result, nil
	}

	result.Message = constant.Info_Success
	result.Ret = constant.Code_Success

	var resultSession model.Session
	service.GetSessionInfoByEmail(email, &resultSession)
	if resultSession.Token != "" {
		resultSession.LastAccessTime = time.Now().Unix()
		resultSession.ClientIP = ""

		service.UpdateSessionInfo(resultSession)
		return true, result, &resultSession
	} else {
		var session model.Session

		session.UserId = resultUser.Id.Hex()
		session.UserName = resultUser.Username
		session.Nickname = resultUser.Nickname
		session.Email = strings.ToLower(resultUser.Email)
		u, _ := uuid.NewV4()
		session.Token = u.String()
		session.Token = strings.Replace(session.Token, "-", "", -1)
		session.CreateTime = time.Now().Unix()
		session.LastAccessTime = time.Now().Unix()
		session.ClientIP = ""
		session.ServerHost = serverhost
		session.Id = bson.NewObjectId()

		service.AddSessionInfo(session)
		return true, result, &session
	}

}

func (this *LoginApiController) validateDSP(email, pass string) (res bool) {

	strarr := strings.Split(email, "@")
	id := strarr[0]
	fmt.Println("Check DSP :" + email + " , id:" + id)

	var parsm = make(url.Values)
	parsm.Set("loginid", id)
	parsm.Set("pwd", pass)

	res = false

	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := &http.Client{Transport: tr}
	resp, err := client.Post(config.DSPUrl, "application/x-www-form-urlencoded", bytes.NewBufferString(parsm.Encode()))
	//resp, err := http.Post(config.DSPUrl, "application/x-www-form-urlencoded",bytes.NewBufferString(parsm.Encode()))
	if err != nil {
		fmt.Println("validateDSP err info:" + id)
		fmt.Println(err)
	}

	if resp != nil {
		body, reserr := ioutil.ReadAll(resp.Body)
		fmt.Println("response info")
		fmt.Println(resp.StatusCode)

		if reserr == nil && body != nil {
			fmt.Println(string(body))
			var dat map[string]interface{}
			jsonerr := json.Unmarshal(body, &dat)
			if jsonerr == nil {
				success := dat["success"].(bool)
				if success {
					res = true
					fmt.Println("Check DCS success:" + id)
				}
			} else {
				fmt.Println("validateDSP, parse json failed")
				fmt.Println(jsonerr)
			}
		}
	}

	return
}

const (
	urls = "http://rldap.kingsoft.com:8000/admin_gateway.php?key="
	key  = "ldap.api"
)

func (this *LoginApiController) validateKOA(email, pass string) (error, string) {
	strarr := strings.Split(email, "@")
	id := strarr[0]
	tt := fmt.Sprintf("%d", time.Now().Unix())
	verify := hex.EncodeToString(util.Md5([]byte(key + "&&" + id + "&&" + tt)))
	key := url.QueryEscape(base64.StdEncoding.EncodeToString([]byte(id + "||" + pass + "||" + tt + "||" + verify)))
	resp, err := http.Get(urls + key)
	if err != nil {
		return err, ""
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err, ""
	}
	js, err := simplejson.NewJson([]byte(body))
	if err != nil {
		return err, ""
	}
	beego.Debug(js)
	code, _ := js.Get("code").Int()
	switch code {
	case 1:
		realname, _ := js.Get("data").GetIndex(0).String()
		return nil, realname[strings.Index(realname, "[")+1 : len(realname)-1]
	case -1:
		return errors.New("账号或密码错误"), ""
	default:
		return errors.New("系统错误:"), ""
	}
}

func (this *LoginApiController) Post() {

	var result Response
	var postJson string

	//	beego.Info(this.Ctx.Input.Request)
	//	beego.Info(string(this.Ctx.Input.RequestBody))

	postJson = fmt.Sprintf("%s", this.Ctx.Input.RequestBody)

	if postJson == "" {
		result.Message = constant.Info_Error_ParamError
		result.Ret = constant.Code_Error_ParamError
		this.Ctx.WriteString(result.FormatJsonString())
		return
	}

	js, err := simplejson.NewJson([]byte(postJson))

	if err != nil {
		result.Message = constant.Info_Error_ParamError
		result.Ret = constant.Code_Error_ParamError
		this.Ctx.WriteString(result.FormatJsonString())
		return
	}

	email, _ := js.Get("email").String()
	password, _ := js.Get("password").String()
	serverhost, _ := js.Get("serverhost").String()

	log4go.Debug("try to login: email=%s serverhost=%s", email, serverhost)


	if email == "" || password == "" {
		result.Message = constant.Info_Error_ParamError
		result.Ret = constant.Code_Error_ParamError
		this.Ctx.WriteString(result.FormatJsonString())
		return
	}

	email = strings.ToLower(email)

	/*retCode*/
	_, result, session := this.validationLogin(email, password, serverhost)

	if result.Ret == constant.Code_Success {

		service.CreateLoginLog(session.UserId, this.Ctx.Request)

		addSeesionToCookie(this, session)

		result.Message = constant.Info_Success
		result.Ret = constant.Code_Success
		result.Data = session

		this.Ctx.WriteString(result.FormatJsonString())

		return
	} else {
		//result.Message = constant.Info_Error_SessionIllegal
		//result.Ret = constant.Code_Error_SessionIllegal
		fmt.Println("login failed" + result.Message)

		this.Ctx.WriteString(result.FormatJsonString())
		return
	}
}

func addSeesionToCookie(this *LoginApiController, session *model.Session) {
	expiration := time.Now()
	expiration = expiration.AddDate(0, 1, 0)
	userid_cookie := http.Cookie{Name: "userid", Value: session.UserId, Path: "/", Expires: expiration}
	token_cookie := http.Cookie{Name: "token", Value: session.Token, Path: "/", Expires: expiration}
	nickname_cookie := http.Cookie{Name: "nickname", Value: url.QueryEscape(session.Nickname), Path: "/", Expires: expiration}
	//serverhost_cookie := http.Cookie{Name: "serverhost", Value:  session.ServerHost, Path: "/", Expires: expiration}
	http.SetCookie(this.Ctx.ResponseWriter, &userid_cookie)
	http.SetCookie(this.Ctx.ResponseWriter, &token_cookie)
	http.SetCookie(this.Ctx.ResponseWriter, &nickname_cookie)
	//http.SetCookie(this.Ctx.ResponseWriter, &serverhost_cookie)
	return
}
