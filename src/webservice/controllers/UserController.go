package controllers

import (
	"fmt"
	"github.com/bitly/go-simplejson"
	"net/http"
	"net/url"
	"webservice/model"
	"webservice/service"
	"time"
	"github.com/yaosxi/mgox"
	"gopkg.in/mgo.v2/bson"
)

type UserController struct {
	BaseController
}

func (this *UserController) GetMyInfo() {
	var user model.User
	err := mgox.New().Get(bson.ObjectIdHex(this.GetUserId())).Result(&user)
	if err != nil {
		this.error(err)
		return
	}
	this.data(user, "user")
}

func (this *UserController) EditMyInfo() {
	var postJson string
	postJson = fmt.Sprintf("%s", this.Ctx.Input.RequestBody)
	if postJson == "" {
		this.ParamError("")
		return
	}
	js, err := simplejson.NewJson([]byte(postJson))
	if err != nil {
		this.error(err)
		return
	}
	js.Set("id", this.GetUserId())
	err = service.UpdateUserInfo(js)
	if err == nil {
		service.RemoveUserFromCache(this.GetUserId())
		if val, ok := js.CheckGet("nickname"); ok {
			nickname, _ := val.String()
			expiration := time.Now()
			expiration = expiration.AddDate(1, 0, 0)
			cookie_nickname := http.Cookie{Name: "nickname", Value: url.QueryEscape(nickname), Path: "/", Expires: expiration}
			http.SetCookie(this.Ctx.ResponseWriter, &cookie_nickname)

		}
	}
	this.error(err)
}

func (this *UserController) Edit() {
	var postJson string
	postJson = fmt.Sprintf("%s", this.Ctx.Input.RequestBody)
	if postJson == "" {
		this.ParamError("")
		return
	}
	js, err := simplejson.NewJson([]byte(postJson))
	if err != nil {
		this.error(err)
		return
	}
	this.error(service.UpdateUserInfo(js))
}

func (this *UserController) Disable() {
	userid := this.GetUserId()
	if !service.IsAdminUser(userid) {
		this.NoPermissionError()
		return
	}
	userid = this.GetString(":id")
	if userid == "" {
		this.ParamError("id")
		return
	}
	this.error(service.DisableUser(userid))
}

func (this *UserController) AddPayUser() {
	this.error(service.AddPayUser(this.GetString("serverip"),this.GetString("email"),this.GetString("password"),this.GetString("expiredtime")))
}

func (this *UserController) EditPayUser() {
	this.error(service.EditPayUser(this.GetString("email"), this.GetString("password"), this.GetString("expiredtime")))
}
