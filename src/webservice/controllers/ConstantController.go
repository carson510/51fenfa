package controllers

import (
	"webservice/constant"
)

type ConstantController struct {
	BaseController
}

func (this *ConstantController) getById(id string) {
	var constant constant.Constant
	err := constant.Load(id)
	if err != nil {
		this.error(err)
		return
	}
	this.data(constant, "constant")
}

func (this *ConstantController) Get() {
	this.getById(this.GetString(":id"))
}

func (this *ConstantController) GetFileStorageType() {
	this.getById(constant.KV_FileStorageType)
}

