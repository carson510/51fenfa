package controllers

import (
    "webservice/model"
    "webservice/service"
    "fmt"
    "encoding/json"
)

type AppGroupController struct {
    BaseController
}

func (this *AppGroupController) AddGroup() {
    userid := this.GetUserId()
    groupName := this.GetString("name")
    this.error(service.AddAppGroup(userid, groupName))
}

func (this *AppGroupController) GetGroups() {
    userid := this.GetUserId()
    var groups []model.AppGroup
    if err := service.GetUserAppGroups(userid, &groups); err != nil {
        this.error(err)
        return
    }
    this.data(groups, "appgroups")
}

func (this *AppGroupController) MoveGroup() {
    appids := this.GetString("appids")
    targetGroupId := this.GetString("targetgroupid")
    this.error(service.ChangeAppsGroup(appids, targetGroupId))
}


func (this *AppGroupController) EditGroup() {
    var postJson string
    postJson = fmt.Sprintf("%s", this.Ctx.Input.RequestBody)
    if postJson == "" {
        this.ParamError("")
        return
    }
    var group model.AppGroup
    if err := json.Unmarshal([]byte(postJson), &group); err != nil {
        this.error(err)
        return
    }
    this.error(service.ChangeAppGroupName(group.Id.Hex(), group.Name))
}

func (this *AppGroupController) DeleteGroup() {
    id := this.GetString(":id")
    this.error(service.DeleteAppGroup(id))
}
