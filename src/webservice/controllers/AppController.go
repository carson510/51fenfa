package controllers

import (
	"encoding/json"
	"fmt"
	"github.com/kimiazhu/log4go"
	"github.com/astaxie/beego"
	"github.com/axgle/pinyin"
	"github.com/bitly/go-simplejson"
	"github.com/yaosxi/mgox"
	"gopkg.in/mgo.v2/bson"
	"strconv"
	"strings"
	"webservice/model"
	"webservice/service"
	"webservice/utils"
	"time"
	"github.com/aliyun/aliyun-oss-go-sdk/oss"
	"webservice/config"
	"webservice/storage"
)

type AppController struct {
	BaseController
}

func AddAppsExtInfo(apps []model.App) {
	for i := range apps {
		AddAppExtInfo(&apps[i])
	}
}

func AddAppExtInfo(app *model.App) {
	if app.ReleaseVersion.Package.Url != "" {
		app.ReleaseVersion.Package.Url = "*"
	}
	app.PinYin = strings.ToLower(pinyin.Convert(app.AppName))
	app.Password = strconv.FormatBool(app.Password != "")
}

func (this *AppController) GetMyApps() {

	var p mgox.Page
	p.Cursor, _ = this.GetInt("page.cursor", -1)
	p.Count, _ = this.GetInt("page.count", -1)
	if p.Cursor == -1 {
		p.Cursor, _ = this.GetInt("cursor", 0)
	}
	if p.Count == -1 {
		p.Count, _ = this.GetInt("count", 0)
	}

	userid, _ := this.GetUserIdAndToken()
	pid := this.GetString("pid")
	gid := this.GetString("gid")

	apps := []model.App{}
	query := bson.M{"disabled": false, "userid": userid}
	if pid != "" {
		query["platform"], _ = strconv.Atoi(pid)
	}
	if gid != "" {
		query["groupid"] = gid
	}
	err := mgox.New().Find(query).Sort("-lastreleased").Page(&p).Result(&apps)
	if err != nil {
		this.error(err)
		return
	}
	if len(apps) > 0 {
		AddAppsExtInfo(apps)
	}

	returnData := make(map[string]interface{})
	returnData["nextcursor"] = p.Next
	returnData["apps"] = apps
	returnData["allrows"] = p.Total
	//new page result
	returnData["page"] = p

	this.success(returnData)
}

/**
*  新增APP
 */
func (this *AppController) Post() {

	userid := this.GetUserId()
	if userid != "" {
		expired, err := service.IsExpiredUesr(userid)
		if err != nil {
			this.error(err)
			return
		}
		if expired {
			this.expired("user")
			return
		}
	}

	var postJson string
	postJson = fmt.Sprintf("%s", this.Ctx.Input.RequestBody)
	beego.Debug(postJson)
	if postJson == "" {
		this.ParamError("")
		return
	}

	js, _ := simplejson.NewJson([]byte(postJson))
	release_note, _ := js.Get("release_note").String()
	groupid := js.Get("groupid").MustString("")
	storageid := js.Get("storageid").MustString("")
	package_info_map, _ := js.Get("package").Map()
	postPackageJson, _ := json.Marshal(package_info_map)

	var postPackage model.PackageInfo
	json.Unmarshal([]byte(string(postPackageJson)), &postPackage)

	if postPackage.Platform == model.Platform_Unknow {
		filepath := postPackage.Filename
		if filepath == "" {
			filepath = postPackage.Url
		}
		if filepath == "" {
			filepath = postPackage.LanUrl
		}
		if strings.HasSuffix(filepath, ".apk") {
			postPackage.Platform = model.Platform_Android
		} else if strings.HasSuffix(filepath, ".ipa") {
			postPackage.Platform = model.Platform_iOS
		} else {
			this.ParamError("filename")
			return
		}
	}

	if postPackage.AppPackage != "" {
		postPackage.BundleId = postPackage.AppPackage
	} else if postPackage.BundleId != "" {
		postPackage.AppPackage = postPackage.BundleId
	}

	if postPackage.AppPackage == "" { //包名不能为空
		this.ParamError("packageName")
		return
	}

	if postPackage.AppName == "" {
		index := strings.LastIndex(postPackage.Filename, ".")
		if index > 0 {
			postPackage.AppName = postPackage.Filename[0:index]
		} else {
			postPackage.AppName = postPackage.Filename
		}
	}

	timeNow := time.Now()
	var app model.App
	app.PlatForm = postPackage.Platform
	app.PackageName = postPackage.AppPackage
	app.UserId = userid
	app.Priority = model.TaskPrioroty_Normal
	app.ShareFeedback = true
	app.CreateTime = timeNow.Unix()
	app.Id = bson.NewObjectId()
	app.Disabled = false
	if postPackage.BuildNumber == 0 {
		postPackage.BuildNumber = 1
	}
	app.GroupId = groupid
	if groupid != "" {
		app.GroupName = service.GetAppGroupName(groupid)
	}
	app.StorageId = storageid
	var appVersion model.AppVersion
	appVersion.CreateTime = timeNow.Unix()
	appVersion.Package = postPackage
	appVersion.ReleaseNote = release_note
	appVersion.UserId = userid

	app.AppName = postPackage.AppName
	app.IsShare = true
	app.IsPublic = true
	app.LogoUrl = postPackage.Logo
	app.LastReleased = timeNow.UnixNano()
	app.ReleaseVersion = appVersion
	app.AppUri = service.GetAppServiceInstance().GenURI()
	if err := mgox.New().Insert(&app); err != nil {
		this.error(err)
		return
	}

	this.data(app, "app")
}

func (this *AppController) Get() {

	uri := this.GetString(":uri")

	dao := mgox.Connect().ShareError()
	defer dao.Close()

	var app model.App
	dao.Get(bson.M{"appuri": strings.ToLower(uri)}).IgnoreNFE().Result(&app)
	if dao.LastError != nil {
		this.error(dao.LastError)
		return
	}

	if app.Id.Hex() == "" {
		this.nonexist("app")
		return
	}

	if app.Disabled {
		this.disabled("app")
		return
	}

	expired, err := service.IsExpiredUesr(app.UserId)
	if err != nil {
		this.error(err)
		return
	}
	if expired {
		this.expired("user")
		return
	}
	var user model.User
	err = dao.GetById(app.UserId).Result(&user)
	if err != nil {
		this.error(err)
		return
	}
	serverhost := this.GetString("serverhost")
	if serverhost != "" {
		idx := strings.Index(serverhost, ":")
		if idx > 0 {
			serverhost = serverhost[0:idx]
		}
	} else {
		serverhost = util.GetLatestForwardIpAddress(this.Ctx.Request)
	}
	if serverhost == "" {
		serverhost = this.Ctx.Request.Host
		idx := strings.Index(serverhost, ":")
		if idx > 0 {
			serverhost = serverhost[0:idx]
		}
	}
	if user.ServerIp != "" && user.ServerIp != serverhost {
		log4go.Info("cannot find app '%s' by ip, excepted: %s, actual: %s", uri, user.ServerIp, serverhost)
		this.nonexist("app")
		return
	}
	AddAppExtInfo(&app)
	app.LogoUrl = storage.StorageHelper.GetDownloadUrl(service.GetStorageHost(app.StorageId), app.LogoUrl)
	//insert download view log
	app.DownloadLogId, err = service.AddDownloadViewLog(this.Ctx.Request, this.GetUserId(), app)
	if err != nil {
		this.error(err)
		return
	}
	this.data(app, "app")
}

func (this *AppController) ValidatePassword() {
	id := this.GetString("id")
	if id == "" {
		this.ParamError("id")
		return
	}
	password := this.GetString("password")
	if password == "" {
		this.ParamError("password")
		return
	}
	var app model.App
	err := mgox.New().Get(bson.ObjectIdHex(id)).Result(&app)
	if err != nil {
		this.error(err)
		return
	}
	if password != app.Password {
		this.wrong("password")
		return
	}
	this.ok()
}

/**
* 获取APP的详细信息
 */
func (this *AppController) GetAppInfo() {

	appid := this.GetString("appid")
	if appid == "" {
		appid = this.GetString(":id")
	}
	if appid == "" {
		this.ParamError("appid")
		return
	}
	var app model.App
	err := mgox.New().Get(bson.ObjectIdHex(appid)).Result(&app)
	if err != nil {
		this.error(err)
		return
	}
	password := app.Password
	AddAppExtInfo(&app)
	app.LogoUrl = storage.StorageHelper.GetDownloadUrl(service.GetStorageHost(app.StorageId), app.LogoUrl)
	app.Password = password
	if !app.IsPublic {
		//TODO need to check access right
	}
	returnData := make(map[string]interface{})
	returnData["app"] = app
	this.success(returnData)
}

func (this *AppController) QueryAppInfo() {
	packagename := this.GetString("packagename")
	platform, _ := this.GetInt("platform")
	if packagename == "" || platform == 0 {
		this.ParamError("query")
		return
	}
	userid := this.GetUserId()
	var app model.App
	err := mgox.New().Get("platform", platform, "packagename", packagename, "userid", userid).Result(&app)
	if err != nil {
		this.error(err)
		return
	}
	this.data(app, "app")
}

func (this *AppController) IsValidAppUri() {
	appid := this.GetString("appid")
	appuri := this.GetString("appuri")
	if appid == "" {
		this.ParamError("appid")
		return
	}
	if len(appuri) != 4 {
		this.ParamError("uri")
		return
	}
	exist, err := mgox.New().Find(appid).Exist(model.AppCollectionName)
	if err != nil {
		this.error(err)
		return
	}
	if !exist {
		this.nonexist("appid")
		return
	}
	exist, err = mgox.New().Find("appuri", appuri, "_id", bson.M{"$ne": bson.ObjectIdHex(appid)}).Exist(model.AppCollectionName)
	if exist {
		this.duplicate("uri")
		return
	}
	this.success(nil)
}

func (this *AppController) ModifyAppInfo() {

	var postJson string
	postJson = fmt.Sprintf("%s", this.Ctx.Input.RequestBody)
	beego.Debug(postJson)
	if postJson == "" {
		this.ParamError("")
		return
	}

	js, _ := simplejson.NewJson([]byte(postJson))
	userid, _ := this.GetUserIdAndToken()
	appid, _ := js.Get("appid").String()
	if appid == "" {
		this.ParamError("appid")
		return
	}

	dao := mgox.Connect(userid)
	defer dao.Close()

	var app model.App
	err := dao.Get(bson.ObjectIdHex(appid)).Result(&app)
	if err != nil {
		this.error(err)
		return
	}
	if app.UserId != userid {
		this.NoPermissionError()
		return
	}

	fields := make(map[string]interface{})

	if val, ok := js.CheckGet("appname"); ok {
		if str, _ := val.String(); app.AppName != str {
			fields["appname"] = str
		}
	}
	if val, ok := js.CheckGet("uri"); ok {
		if str, _ := val.String(); app.AppUri != str {
			if len(str) != 4 {
				this.ParamError("uri")
				return
			}
			if str == "dash" {
				this.duplicate("uri")
				return
			}
			str = strings.ToLower(str)
			fields["appuri"] = str
			exist, err := dao.Find("appuri", str, "_id", bson.M{"$ne": bson.ObjectIdHex(appid)}).Exist(model.AppCollectionName)
			if err != nil {
				this.error(err)
				return
			}
			if exist {
				this.duplicate("uri")
				return
			}
		}
	}
	if val, ok := js.CheckGet("ispublic"); ok {
		if b, _ := val.Bool(); app.IsPublic != b {
			fields["ispublic"] = b
		}
	}
	if val, ok := js.CheckGet("isshare"); ok {
		if b, _ := val.Bool(); app.IsShare != b {
			fields["isshare"] = b
		}
		if b, _ := val.Bool(); b {
			fields["ispublic"] = true
		}
	}
	if val, ok := js.CheckGet("sharefeedback"); ok {
		if b, _ := val.Bool(); app.ShareFeedback != b {
			fields["sharefeedback"] = b
		}
	}
	if val, ok := js.CheckGet("password"); ok {
		if str, _ := val.String(); app.Password != str {
			fields["password"] = str
		}
	}
	if val, ok := js.CheckGet("releasenote"); ok {
		if str, _ := val.String(); app.ReleaseVersion.ReleaseNote != str {
			fields["releaseversion.releasenote"] = str
		}
	}
	if val, ok := js.CheckGet("appdesc"); ok {
		if str, _ := val.String(); app.AppDesc != str {
			fields["appdesc"] = str
		}
	}
	if val, ok := js.CheckGet("logourl"); ok {
		if str, _ := val.String(); app.LogoUrl != str {
			fields["logourl"] = str
			fields["releaseversion.package.logo"] = str
		}
	}
	if val, ok := js.CheckGet("imgurls"); ok {
		fields["imgurls"], _ = val.StringArray()
	}
	err = dao.Set(app, bson.ObjectIdHex(appid), fields)
	if err != nil {
		this.error(err)
		return
	}
	this.success(nil)
}

func (this *AppController) DeleteApp() {
	appid := this.GetString(":id")
	if appid == "" {
		this.ParamError("id")
		return
	}
	userid := this.GetUserId()
	var app model.App
	err := mgox.New().Get(bson.ObjectIdHex(appid)).Result(&app)
	if err != nil {
		this.error(err)
		return
	}
	if app.UserId != userid {
		this.NoPermissionError()
		return
	}
	err = service.GetAppServiceInstance().Delete(appid)
	if err != nil {
		this.error(err)
		return
	}
	this.success(nil)
}

func (this *AppController) DeleteApps() {
	appids := this.GetString("appids")
	this.error(service.GetAppServiceInstance().DeleteApps(appids))
}

/**
* Get app download url with password validation
 */
func (this *AppController) GetAppDownloadUrl() {
	id := this.GetString("id")
	if id == "" {
		this.ParamError("id")
		return
	}
	password := this.GetString("password")
	downloadlogid := this.GetString("downloadlogid")

	dao := mgox.Connect().ShareError()
	defer dao.Close()

	var app model.App
	dao.Get(bson.ObjectIdHex(id)).Result(&app)
	if dao.LastError != nil {
		this.error(dao.LastError)
		return
	}
	if password != app.Password {
		this.wrong("password")
		return
	}
	url := app.ReleaseVersion.Package.Url
	if app.PlatForm == model.Platform_iOS {
		client, err := oss.New(config.GetPlistHost(), "BC8xtBQ6WRSXskQp", "2KLpsDxnTt7OR9n7AJb4zHUw2HrIVn")
		if err != nil {
			this.error(err)
			return
		}
		bucket, err := client.Bucket("adp")
		if err != nil {
			this.error(err)
			return
		}
		app.DownloadLogId = downloadlogid
		user := service.GetUserById(app.UserId)
		if user.ServerIp == "" {
			user.ServerIp = util.GetLatestForwardIpAddress(this.Ctx.Request)
		}
		if user.ServerIp == "" {
			user.ServerIp = this.Ctx.Request.Host
		}
		log4go.Debug("server ip: " + user.ServerIp)
		manifest := util.GetPlistFromApp(app, user.ServerIp, service.GetStorageHost(app.StorageId))
		//log4go.Debug("\n" + manifest)
		date := time.Now().Format("20060102")
		err = bucket.PutObject("plist/" + date +"/" + downloadlogid, strings.NewReader(manifest))
		if err != nil {
			this.error(err)
			return
		}
		url = "itms-services://?action=download-manifest&url=https://adp.oss-cn-shenzhen.aliyuncs.com/plist/" + date + "/" + downloadlogid
	} else {
		url = storage.StorageHelper.GetDownloadUrl(service.GetStorageHost(app.StorageId), url)
		//fileExtension := filepath.Ext(url)
		//if strings.Index(fileExtension, "?") > 0 {
		//	fileExtension = fileExtension[0:strings.Index(fileExtension, "?")]
		//}
		//t := strconv.FormatInt(time.Now().UnixNano(), 10)
		//if strings.Index(url, "?") > 0 {
		//	url += "&" + t
		//} else {
		//	url += "?" + t
		//}
		//url += "t=" + t + "&response-content-disposition=attachment%3bfilename%3d" +
		//	app.AppName + "_v" + app.ReleaseVersion.Package.Version +
		//	"_b" + strconv.Itoa(app.ReleaseVersion.Package.BuildNumber) +
		//	fileExtension

		//set download started
		service.SetDownloadStartingTime(downloadlogid)
	}
	log4go.Debug(url)

	this.data(url, "url")
}
