package service

import (
	"webservice/model"
	"github.com/yaosxi/mgox"
	"gopkg.in/mgo.v2/bson"
	"strings"
	"github.com/kimiazhu/log4go"
)

func AddAppGroup(userId, groupName string) error {
	var group model.AppGroup
	group.UserId = userId
	group.Name = groupName
	exist, err := mgox.New().Find("userid", userId, "name", groupName).Exist(model.AppGroupCollectionName)
	if err == nil && !exist {
		return mgox.New().Insert(&group)
	}
	return err
}

func ChangeAppsGroup(appIds, groupId string) error {
	ids := strings.Split(appIds, ",")
	for _, id := range ids {
		if id == "" {
			continue
		}
		err := ChangeAppGroup(id, groupId)
		if err != nil {
			return err
		}
	}
	log4go.Info("successfully change groups: " + appIds + ", groupid: " + groupId)
	return nil
}

func ChangeAppGroup(appId, groupId string) error {
	log4go.Debug("move app[id=%s] to group[id=%s]", appId, groupId)
	groupName := GetAppGroupName(groupId)
	return mgox.New().Set(model.AppCollectionName, bson.ObjectIdHex(appId), "groupid", groupId, "groupname", groupName)
}

func ChangeAppGroupName(id, name string) error {
	dao := mgox.Connect().ShareError()
	defer dao.Close()
	dao.Set(model.AppCollectionName, bson.M{"groupid": id}, "groupname", name)
	return dao.Set(model.AppGroupCollectionName, bson.ObjectIdHex(id), "name", name)
}

func DeleteAppGroup(id string) error {
	dao := mgox.Connect().ShareError()
	defer dao.Close()
	var apps []model.App
	dao.Find("groupid", id).Result(&apps)
	if dao.LastError != nil {
		return dao.LastError
	}
	for _, app := range apps {
		GetAppServiceInstance().Delete(app.Id.Hex())
	}
	return dao.Remove(model.AppGroupCollectionName, bson.ObjectIdHex(id))
}

func GetAppGroupName(id string) string {
	var group model.AppGroup
	mgox.New().Get(bson.ObjectIdHex(id)).Result(&group)
	return group.Name
}

func GetUserAppGroups(userid string, groups *[]model.AppGroup) error {
	return mgox.New().Find("userid", userid).Sort("-firstcreated").Result(groups)
}