package service

import (
	"github.com/yaosxi/mgox"
	"gopkg.in/mgo.v2/bson"
	"net/http"
	"webservice/model"
	"webservice/utils"
)

func CreateLoginLog(userid string, request *http.Request) {
	loginLog := model.LoginLog{}
	loginLog.UserId = userid
	loginLog.IsLastLogin = true
	loginLog.IsPlatformLast = true
	loginLog.Client = *util.GetClientInfo(request)
	dao := mgox.Connect(userid)
	defer dao.Close()
	dao.Set(model.LoginLog{}, bson.M{"userid": userid, "client.platform": loginLog.Client.Platform, "isplatformlast": true}, "isplatformlast", false)
	dao.Set(model.LoginLog{}, bson.M{"userid": userid, "islastlogin": true}, "islastlogin", false)
	dao.Insert(&loginLog)
}
