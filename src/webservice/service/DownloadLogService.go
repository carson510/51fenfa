package service

import (
	"github.com/yaosxi/mgox"
	"webservice/model"
	"webservice/utils"
	"net/http"
	"time"
	"gopkg.in/mgo.v2/bson"
)

func AddDownloadViewLog(request *http.Request, userid string, app model.App) (string, error) {
	var log model.DownloadLog
	log.UserId = userid
	log.AppId = app.Id.Hex()
	log.Version = app.ReleaseVersion.Package.Version
	log.BuildNumber = app.ReleaseVersion.Package.BuildNumber
	log.Client = util.GetClientInfo(request)
	log.Status = model.DownloadStatus_View
	t := time.Now()
	log.ViewTime = &t
	err := mgox.New(userid).Insert(&log)
	return log.Id.Hex(), err
}

func SetDownloadStartingTime(dLogId string) error {
	dao := mgox.Connect().ShareError()
	defer dao.Close()
	var log model.DownloadLog
	dao.GetById(dLogId).Result(&log)
	if dao.LastError != nil {
		return dao.LastError
	}
	if log.BeginTime == nil {
		dao.Inc(model.AppCollectionName, bson.ObjectIdHex(log.AppId), "downloadcount", 1);
	}
	log.Status = model.DownloadStatus_Begin
	t := time.Now()
	log.BeginTime = &t
	log.Speed = 0
	log.Duration = 0
	return dao.ReplaceDoc(log)
}
