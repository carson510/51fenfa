package service

import (
	"github.com/yaosxi/mgox"
	"gopkg.in/mgo.v2/bson"
	"crypto/rand"
	"webservice/model"
	"strings"
	"github.com/kimiazhu/log4go"
	"webservice/storage"
)

type AppService struct {
}

var appService = AppService{}

func GetAppServiceInstance() *AppService {
	return &appService
}

var charTable = []rune("abcdefghijkmnpqrstuvwxyz23456789")

// RandStrN 返回长度为N的随机字符和数字组合,其中不包含容易被混淆的[0/1/o/l]四个字符
func RandStrN(n int) string {
	random := make([]byte, n)
	result := make([]rune, n)
	rand.Read(random[:])
	for i := 0; i < len(random); i++ {
		result[i] = charTable[uint(random[i] >> 3)]
	}
	return string(result)
}

func (this *AppService) GenURI() string {
	for {
		uri := strings.ToLower(RandStrN(6));
		var app model.App
		mgox.New().Get("appuri", uri).Result(&app)
		if app.Id.Hex() == "" {
			return uri
		}
	}
}

func deleteFile(url, storageid string) error {
	return storage.StorageHelper.DeleteFile(url, GetStorageById(storageid))
}

func (this *AppService) Delete(id string) error {
	dao := mgox.Connect().ShareError()
	defer dao.Close()
	var app model.App
	err := dao.GetById(id).Result(&app)
	if err != nil {
		log4go.Error(err)
		return err
	}
	if err = deleteFile(app.ReleaseVersion.Package.Url, app.StorageId); err != nil {
		return err
	}
	if err = deleteFile(app.ReleaseVersion.Package.Logo, app.StorageId); err != nil {
		return err
	}
	dao.Remove(model.DownloadLog{}, bson.M{"appid": id})
	return dao.Remove(model.App{}, id)
}

func (this *AppService) DeleteApps(appIds string) error {
	ids := strings.Split(appIds, ",")
	for _, id := range ids {
		if id == "" {
			continue
		}
		err := this.Delete(id)
		if err != nil {
			return err
		}
	}
	log4go.Info("successfully delete apps: " + appIds)
	return nil
}