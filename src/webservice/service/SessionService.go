package service

import (
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/cache"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"webservice/config"
	"webservice/model"
	"webservice/utils"
	"github.com/yaosxi/mgox"
)

func getSessionCacheKey(token string) string {
	return "session_" + token
}

func GetSessionInfoFromCache(token string) model.Session {
	var bm, _ = cache.NewCache("memory", `{"interval":60}`)
	_session := bm.Get(getSessionCacheKey(token))
	if _session == nil {
		session := getSessionInfoByToken(token)
		if session.Token != "" {
			bm.Put(getSessionCacheKey(token), session, config.CacheTimeOut)
		}
		return session
	} else {
		return _session.(model.Session)
	}
}

func getSessionInfoByToken(token string) model.Session {
	var session model.Session
	var resultSession model.Session
	db_Error := util.WithDBContext(func(db *mgo.Database) error {

		c := db.C(config.DataTableSession)

		resultSession = model.Session{}
		err := c.Find(bson.M{"token": token}).One(&resultSession)
		if err != nil {
			return err
		}
		return nil
	})

	if db_Error != nil {
		return session
	} else {
		return resultSession
	}
}

func GetSessionInfoByEmail(email string, session *model.Session) error {
	return mgox.New().Get("email", email).IgnoreNFE().Result(session)
}

func GetSessionInfoByUserId(userid string, session *model.Session) error {
	return mgox.New().Get("userid", userid).Result(session)
}

func AddSessionInfo(session model.Session) bool {

	db_Error := util.WithDBContext(func(db *mgo.Database) error {

		c := db.C(config.DataTableSession)
		err := c.Insert(&session)
		if err != nil {
			beego.Error("error:%s\n", err)
			return err
		}
		return nil
	})

	if db_Error != nil {
		beego.Error(db_Error)
		return false
	}

	return true
}

func UpdateSessionInfo(session model.Session) bool {

	db_Error := util.WithDBContext(func(db *mgo.Database) error {

		c := db.C(config.DataTableSession)

		err := c.UpdateId(session.Id, &session)
		if err != nil {
			beego.Error("error:%s\n", err)
			return err
		}
		return nil
	})

	if db_Error != nil {
		beego.Error(db_Error)
		return false
	}

	return true
}

func RemoveSession(token string) error {
	bm, err := cache.NewCache("memory", `{"interval":60}`)
	if err != nil {
		return err
	}
	session := bm.Get(getSessionCacheKey(token))
	if session != nil {
		err = bm.Delete(getSessionCacheKey(token))
		if err != nil {
			return err
		}
		_session := session.(model.Session)
		return mgox.New().ReplaceDocById(_session.Id, &_session)
	}
	return nil
}
