package service

import (
	"encoding/base64"
	"errors"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/cache"
	"github.com/bitly/go-simplejson"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"regexp"
	"strconv"
	"strings"
	"webservice/config"
	"webservice/constant"
	"webservice/model"
	"webservice/utils"
	"time"
	"github.com/yaosxi/mgox"
)

func IsAdminUser(userid string) bool {
	return userid == "56555b040107060d26000024" || userid == "55cae518e138235587000005"
}

func GetUserById(id string) *model.User {
	var user model.User
	mgox.New().GetById(id).Result(&user)
	return &user
}

func EncodePassword(password string) string {
	UESR_PWD_IV := []byte{0x13, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF}
	encodeByte, _ := util.DESEncode([]byte(password), []byte(config.USER_PWD_KEY), UESR_PWD_IV)
	return base64.StdEncoding.EncodeToString(encodeByte)
}

func GetUserByEmail(email string, user *model.User) error {
	return mgox.New().Get("email", email).Result(&user)
}

func AddUser(user model.User) bool {
	err := mgox.New().Insert(&user)
	if err != nil {
		beego.Error(err)
		return false
	}
	return true
}

func UpdateUser(user model.User) bool {

	db_Error := util.WithDBContext(func(db *mgo.Database) error {

		c := db.C(config.DataTableUser)

		err := c.UpdateId(user.Id, &user)
		if err != nil {
			beego.Error("error:%s\n", err)
			return err
		}
		return nil
	})

	if db_Error != nil {
		beego.Error(db_Error)
		return false
	}

	return true
}

func existedValue(fieldName, fieldValue, excludeId string) (bool, error) {
	return mgox.New().Find(bson.M{fieldName: fieldValue, "_id": bson.M{"$ne": bson.ObjectIdHex(excludeId)}}).Exist(model.UserCollectionName)
}

func newError(code int, tag string) error {
	return errors.New(strconv.Itoa(code) + ":" + tag)
}

func UpdateUserInfo(js *simplejson.Json) error {
	userid, _ := js.Get("id").String()
	var user model.User
	err := mgox.New().Get(bson.ObjectIdHex(userid)).Result(&user)
	if err != nil {
		return err
	}
	fields := make(map[string]interface{})
	if val, ok := js.CheckGet("username"); ok {
		if str, _ := val.String(); user.Username != str {
			exist, err := existedValue("username", str, userid)
			if err != nil {
				return err
			}
			if exist {
				return newError(constant.Code_Error_DuplicatedInfo, "username")
			}

		}
	}
	if val, ok := js.CheckGet("email"); ok {
		if str, _ := val.String(); user.Email != str {
			exist, err := existedValue("email", str, userid)
			if err != nil {
				return err
			}
			if exist {
				return newError(constant.Code_Error_DuplicatedInfo, "email")
			}
			fields["email"] = str
		}
	}
	if val, ok := js.CheckGet("mobile"); ok {
		if str, _ := val.String(); user.Mobile != str {
			exist, err := existedValue("mobile", str, userid)
			if err != nil {
				return err
			}
			if exist {
				return newError(constant.Code_Error_DuplicatedInfo, "mobile")
			}
			fields["mobile"] = str
		}
	}
	if val, ok := js.CheckGet("password"); ok {
		str, _ := val.String()
		str = EncodePassword(str)
		if user.Password != str {
			fields["password"] = str
		}
	}
	if val, ok := js.CheckGet("nickname"); ok {
		if str, _ := val.String(); user.Nickname != str {
			fields["nickname"] = str
		}
	}
	if info, ok := js.CheckGet("info"); ok {
		if val, ok := info.CheckGet("realname"); ok {
			if str, _ := val.String(); user.RealName != str {
				fields["realname"] = str
			}
		}
		if val, ok := info.CheckGet("gender"); ok {
			if str, _ := val.String(); user.Gender != str {
				fields["gender"] = str
			}
		}
		if val, ok := info.CheckGet("image"); ok {
			if str, _ := val.String(); user.Image != str {
				fields["image"] = str
			}
		}
		if val, ok := info.CheckGet("address"); ok {
			if str, _ := val.String(); user.Address != str {
				fields["address"] = str
			}
		}
	}
	return mgox.New().Set(model.UserCollectionName, bson.ObjectIdHex(userid), fields)
}

func getUserName(email string) (username string) {
	if ok, _ := regexp.MatchString("^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$", email); ok {
		reg := regexp.MustCompile("^\\w+([-+.]\\w+)*")
		username := reg.FindAllString(email, -1)[0]
		checkedUsername := username
		for subfix := 1; ; subfix++ {
			exist, _ := mgox.New().Find("username", checkedUsername).Exist(model.UserCollectionName)
			if exist {
				checkedUsername = username + "_" + strconv.Itoa(subfix)
			} else {
				break
			}
		}
		return checkedUsername
	}
	return ""
}

func IsExpiredUesr(id string) (bool, error) {
	var user model.User
	err := mgox.New().Get(bson.ObjectIdHex(id)).IgnoreNFE().Result(&user)
	if err != nil {
		return true, err
	}
	if user.ExpiredTime == nil {
		return false, nil
	}
	if time.Now().After(*user.ExpiredTime) {
		return true, nil
	}
	return false, nil
}

var memory, _ = cache.NewCache("memory", `{"interval":1h}`)

func GetUserFromCache1(id string) model.User {
	if id == "" {
		return model.User{}
	}
	user := memory.Get("user_" + id)
	if user == nil {
		var _user model.User
		err := mgox.New().Get(bson.ObjectIdHex(id)).Result(&_user)
		if err == nil {
			memory.Put("user_"+id, _user, config.CacheTimeOut)
		}
		return _user
	} else {
		return user.(model.User)
	}
}

func GetUserNicknameByIdFromCache(id string) string {
	if id == "" {
		return ""
	}
	//TODO need to optimize in the future
	user := memory.Get("user_" + id)
	if user == nil {
		var _user model.User
		err := mgox.New().Get(bson.ObjectIdHex(id)).Result(&_user)
		if err == nil {
			memory.Put("user_"+id, _user, config.CacheTimeOut)
		}
		return _user.Nickname
	} else {
		return (user.(model.User)).Nickname
	}
}

func GetUsernameByIdFromCache(id string) string {
	if id == "" {
		return ""
	}
	//TODO need to optimize in the future
	user := memory.Get("user_" + id)
	if user == nil {
		var _user model.User
		err := mgox.New().Get(bson.ObjectIdHex(id)).Result(&_user)
		if err == nil {
			memory.Put("user_"+id, _user, config.CacheTimeOut)
		}
		return _user.Username
	} else {
		return (user.(model.User)).Username
	}
}

func RemoveUserFromCache(id string) {
	memory.Delete("user_" + id)
}

func CreateNewUser(email, pass, userType, realname string) (model.User, bool) {

	var user model.User
	user.Id = bson.NewObjectId()
	user.Username = getUserName(email)
	user.Nickname = user.Username
	user.Email = strings.ToLower(email)
	user.Password = pass
	if realname != "" {
		user.RealName = realname
		user.Nickname = realname
	}

	resCode := AddUser(user)
	return user, resCode
}


func DisableUser(id string) error {
	var sessions []model.Session
	err := mgox.New().Find("userid", id).Result(&sessions)
	if err != nil {
		return err
	}
	for _, session := range sessions {
		RemoveSession(session.Token)
	}
	return mgox.New().Set(model.UserCollectionName, bson.ObjectIdHex(id), "disabled", true)
}

func AddPayUser(serverip, email, password, expiredtime string) error {
    exist, err := mgox.New().Get("email", email).Exist(model.UserCollectionName)
    if err != nil {
        return err
    }
    if exist {
        return errors.New("email has existed.")
    }
    var user model.User
    user.ServerIp = serverip
    user.Email = email
    user.Username = user.Email[0:strings.Index(user.Email, "@")]
    user.Nickname = user.Username
    layout := "2006-01-02"
    t, err := time.ParseInLocation(layout, expiredtime, time.Now().Location())
    if err != nil {
        return err
    }
    user.ExpiredTime = &t
    user.Password = password
    return mgox.New().Insert(&user)
}

func EditPayUser(email, password, expiredtime string) error {
	var user model.User
	err := mgox.New().Get("email", email).Result(&user)
	if err != nil {
		return err
	}
	memory.Delete("user_" + user.Id.Hex())
	if password != "" {
		user.Password = password
	}
	if expiredtime != "" {
		layout := "2006-01-02"
		t, err := time.ParseInLocation(layout, expiredtime, time.Now().Location())
		if err != nil {
			return err
		}
		user.ExpiredTime = &t
	}
	return mgox.New().ReplaceDoc(&user)
}