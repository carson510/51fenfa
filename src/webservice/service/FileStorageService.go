package service

import (
	"webservice/model"
	"github.com/yaosxi/mgox"
	"gopkg.in/mgo.v2/bson"
	"webservice/constant"
)

func AddFileStorage(fileStorage *model.FileStorage) error {
	dao := mgox.Connect().ShareError()
	defer dao.Close()
	if fileStorage.Default {
		dao.Set(model.FileStorageCollectionName, bson.M{"userid": fileStorage.UserId, "default": true}, "default", false)
	}
	return dao.Insert(fileStorage)
}

func GetMyFileStorage(userid string, storages *[]model.FileStorage) error {
	err := mgox.New().Find("userid", userid).IgnoreNFE().Result(storages)
	if err != nil {
		return err
	}
	var c constant.Constant
	c.Load(constant.KV_FileStorageType)
	for i, s := range *storages {
		(*storages)[i].TypeName = c.GetVal(s.Type)
	}
	return nil
}

func DeleteFileStorage(id string) error {
	dao := mgox.Connect().ShareError()
	defer dao.Close()
	//TODO Remove all apps?
	return dao.Remove(model.FileStorageCollectionName, bson.ObjectIdHex(id))
}

func SetDefaultFileStorage(userid, id string) error {
	dao := mgox.Connect().ShareError()
	defer dao.Close()
	dao.Set(model.FileStorageCollectionName, bson.M{"userid": userid, "default": true}, "default", false)
	return dao.Set(model.FileStorageCollectionName, bson.ObjectIdHex(id), "default", true)
}

func GetDefaultFileStorage(userid string, storage *model.FileStorage) error {
	return mgox.New().Get("userid", userid, "default", true).IgnoreNFE().Result(storage)
}

func GetStorageById(id string) model.FileStorage {
	var storage model.FileStorage
	mgox.New().GetById(id).Result(&storage)
	return storage
}

func GetStorageHost(id string) string {
	return GetStorageById(id).DownloadDomain
}