package model

import (
	"gopkg.in/mgo.v2/bson"
	"time"
)

type PlatformType int

const (
	Platform_Unknow = iota
	Platform_iOS
	Platform_Android
	Platform_WP
	Platform_All
)

var PlatformNames = []string{
	Platform_Unknow:  "Unknow",
	Platform_iOS:     "iOS",
	Platform_Android: "Android",
	Platform_WP:      "WP",
	Platform_All:     "All",
}

type AppStatus int

const (
	AppStatus_Executing = iota
	AppStatus_Finished
	AppStatus_AllStatus
)

type AppVersionStatus int

const (
	AppVersionStatus_Normal = iota
	AppVersionStatus_Deleted
	AppVersionStatus_Cleaned
)

type TaskPrioroty int

const (
	TaskPrioroty_Normal = iota
	TaskPrioroty_High
	TaskPrioroty_VeryHigh
	TaskPrioroty_VeryLow = -2
	TaskPrioroty_Low     = -1
)

type DownloadStatus int

const (
	DownloadStatus_View DownloadStatus = iota
	DownloadStatus_Begin
	DownloadStatus_End
	DownloadStatus_Cancel
)

//app--1:n--->task
var AppCollectionName = new(App)

type App struct {
	Id             bson.ObjectId `bson:"_id" json:"id"`
	CreateTime     int64         `json:"createtime"`
	PlatForm       PlatformType  `json:"platform"`
	LogoUrl        string        `json:"logourl"`
	AppUri         string        `json:"uri"` //下载链接
	AppName        string        `json:"appname"`
	PinYin         string        `json:"pinyin" bson:"-"`
	PackageName    string        `json:"packagename"` //包名，必须唯一
	AutoExpireTime int64         `json:"-"`
	Status         AppStatus     `json:"status"`
	Disabled       bool          `json:"disabled"`
	Priority       TaskPrioroty  `json:"priority"`
	ReleaseVersion AppVersion    `json:"releaseversion"` //当前对外的发布的版本
	UserId         string        `json:"userid"`
	IsShare        bool          `json:"isshare"`
	IsPublic       bool          `json:"ispublic"`
	ShareFeedback  bool          `json:"sharefeedback"`
	Password       string        `json:"password"`
	DownloadLogId  string        `json:"downloadlogid" bson:"-"`
	DownLoadCount  int           `json:"downloadcount"`
	AppDesc        string        `json:"appdesc"`
	ImgUrls        []string      `json:"imgurls"`
	LastReleased   int64         `json:"lastreleased"`
	GroupId        string        `json:"groupid"`
	GroupName      string        `json:"groupname"`
	StorageId      string        `json:"storageid"`
	MailMsg        MailMessage   `json:"mailmsg,omitempty"`
	FirstCreator   string        `json:"firstcreator"`
	FirstCreated   time.Time     `json:"firstcreated"`
	LastModifier   string        `json:"lastmodifier"`
	LastModified   time.Time     `json:"lastmodified"`
}

type AppVersion struct {
	CreateTime     int64         `json:"createtime"`
	AutoExpireTime int64         `json:"-"` //`json:"auto_expiretime",omitempty` // 默认的过期时间
	Status         int           `json:"status"`
	Package        PackageInfo   `json:"package"`
	ReleaseNote    string        `json:"releasenote"`
	ForceUpgrade   bool          `json:"forceupgrade"`
	UserId         string        `json:"userid"`
	FirstCreator   string        `json:"firstcreator"`
	FirstCreated   time.Time     `json:"firstcreated"`
	LastModifier   string        `json:"lastmodifier"`
	LastModified   time.Time     `json:"lastmodified"`
}

type PackageInfo struct {
	Filename       string       `json:"filename"`
	Url            string       `json:"url"`
	LanUrl         string       `json:"lanurl"`
	Platform       PlatformType `json:"platform"`
	Size           int64        `json:"size,omitempty"`
	Logo           string       `json:"logo"`
	AppName        string       `json:"appname"`
	BundleId       string       `json:"bundleid,omitempty"`
	BundleName     string       `json:"bundlename,omitempty"`
	AppPackage     string       `json:"package,omitempty",bson:"-"`
	Version        string       `json:"version,omitempty"`
	BuildNumber    int          `json:"buildnumber,omitempty"`
	MiniSDKVersion string       `json:"min_sdkverion,omitempty"`
	MiniOSVersion  string       `json:"min_osverion,omitempty"`
	Executable     string       `json:"executable,omitempty"`   // 可执行程序名称
	DeviceFamily   []string     `json:"devicefamily,omitempty"` // 支持的设备类型 ["iPhone", "iPad"]
}

var AppGroupCollectionName = new(AppGroup)
type AppGroup struct {
	Id            bson.ObjectId  `bson:"_id" json:"id"`
	UserId        string         `json:"userid"`
	Name          string         `json:"name"`
	FirstCreated   time.Time     `json:"firstcreated"`
}

type MailMessage struct {
	InviteTester string `json:"invitetester,omitempty"`
}

type DownloadLog struct {
	Id           bson.ObjectId  `bson:"_id" json:"id"`
	AppId        string         `json:"appid"`
	UserId       string         `json:"userid"`
	Version      string         `json:"version"`
	BuildNumber  int            `json:"buildnumber"`
	Client       *Client        `json:"client" bson:",omitempty"`
	Status       DownloadStatus `json:"status"`
	ViewTime     *time.Time     `json:"viewtime"`
	BeginTime    *time.Time     `json:"begintime"`
	EndTime      *time.Time     `json:"endtime"`
	Duration     time.Duration  `json:"duration"`
	Speed        int64          `json:"speed"`
}
