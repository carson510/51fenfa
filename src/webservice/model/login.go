package model

import (
	"gopkg.in/mgo.v2/bson"
	"time"
)

type LoginLog struct {
	Id             bson.ObjectId `bson:"_id,omitempty" json:"id,omitempty"`
	UserId         string        `json:"userid"`
	Client         Client        `json:"client" bson:",omitempty"`
	IsPlatformLast bool          `json:"isplatformlast"`
	IsLastLogin    bool          `json:"islastlogin"`
	FirstCreated   time.Time     `json:"firstcreated"`
	LastModified   time.Time     `json:"lastmodified"`
}

type Session struct {
	UserId          string `json:"userid"`
	UserName        string `json:"username"`
	Nickname        string `json:"nickname"`
	Email           string `json:"email"`
	CreateTime      int64  `json:"createtime"`
	Token           string `json:"token"`
	LastAccessTime  int64  `json:"lastaccesstime"`
	ClientIP        string `json:"clientip`
	ServerHost      string `json:"serverhost`
	PermissonString string `json:"permissionstring`

	Id bson.ObjectId `bson:"_id" json:"id"`
}
