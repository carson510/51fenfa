package model

type Client struct {
	IP         string `json:"ip"`
	Platform   string `json:"platform"`
	DeviceName string `json:"devicename"`
	OsName     string `json:"osname"`
	OsVersion  string `json:"osversion"`
	Name       string `json:"name"`
	Version    string `json:"version"`
	Build      string `json:"build"`
}
