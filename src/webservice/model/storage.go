package model

import (
	"gopkg.in/mgo.v2/bson"
	"time"
)

var FileStorageCollectionName = new(FileStorage)

type FileStorage struct {
	Id             bson.ObjectId `json:"id" bson:"_id,omitempty"`
	UserId         string        `json:"userid"`
	Type           string        `json:"type"`
	TypeName       string        `json:"typename" bson:"-"`
	Name           string        `json:"name"`
	Endpoint       string        `json:"endpoint"`
	UploadDomain   string        `json:"uploaddomain,omitempty" bson:",omitempty"`
	DownloadDomain string        `json:"downloaddomain"`
	Bucket         string        `json:"bucket,omitempty" bson:",omitempty"`
	AccessKey      string        `json:"accesskey,omitempty" bson:",omitempty"`
	SecretKey      string        `json:"secretkey,omitempty" bson:",omitempty"`
	Default        bool          `json:"default"`
	Key            string        `json:"key" bson:"-"`
	Token          string        `json:"token" bson:"-"`
	Policy         string        `json:"policy" bson:"-"`
	Signature      string        `json:"signature" bson:"-"`
	FirstCreated   time.Time     `json:"firstcreated"`
}
