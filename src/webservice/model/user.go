package model

import (
	"gopkg.in/mgo.v2/bson"
	"time"
)

var UserCollectionName = new(User)

type User struct {
	Id           bson.ObjectId `json:"id" bson:"_id,omitempty"`
	Username     string        `json:"username"`
	Email        string        `json:"email"`
	Password     string        `json:"password"`
	ServerIp     string        `json:"serverip"`
	Storage      string        `json:"storage"`
	AccessKey    string        `json:"accesskey"`
	SecretKey    string        `json:"secretkey"`
	Bucket       string        `json:"bucket"`
	Domain       string        `json:"domain"`
	Mobile       string        `json:"mobile"`
	Nickname     string        `json:"nickname"`
	RealName     string        `json:"realname"`
	Gender       string        `json:"gender"`
	Image        string        `json:"image"`
	Address      string        `json:"address"`
	ExpiredTime  *time.Time    `json:"expiredtime"`
	FirstCreator string        `json:"firstcreator"`
	FirstCreated time.Time     `json:"firstcreated"`
	LastModifier string        `json:"lastmodifier"`
	LastModified time.Time     `json:"lastmodified"`
}
