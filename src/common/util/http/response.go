package httputil

import (
    "net/http"
    "encoding/json"
    "common/constant"
)

type Response struct {
    Message string      `json:"message"`
    Ret     int         `json:"ret"`
    Tag     string      `json:"tag,omitempty"`
    Data    interface{} `json:"data,omitempty"`
}

func (r *Response) FormatJsonString() string {
    result, err := json.Marshal(r)
    if err != nil {
        return ""
    }
    return string(result)
}

func WriteParamError(w http.ResponseWriter, tag string) {
    var result Response
    result.Message = constant.Info_Error_ParamError
    result.Ret = constant.Code_Error_ParamError
    result.Tag = tag
    w.Write([]byte(result.FormatJsonString()))
}

func WriteError(w http.ResponseWriter, err error) {
    var result Response
    result.Message = err.Error()
    if err.Error() == "not found" {
        result.Ret = constant.Code_Error_NotFound
    } else {
        result.Ret = constant.Code_Error_OperationFailed
    }
    w.Write([]byte(result.FormatJsonString()))
}

func WriteData(w http.ResponseWriter, data interface{}, tag string) {
    var result Response
    result.Ret = constant.Code_Success
    result.Message = constant.Info_Success
    result.Data = data
    result.Tag = tag
    w.Write([]byte(result.FormatJsonString()))
}

func WriteOK(w http.ResponseWriter) {
    var result Response
    result.Ret = constant.Code_Success
    result.Message = constant.Info_Success
    w.Write([]byte(result.FormatJsonString()))
}

