package util

import "syscall"

type DiskStatus struct {
	All uint64 `json:"all"`
	Used uint64 `json:"used"`
	Free uint64 `json:"free"`
}

func DiskUsage() DiskStatus {
	var disk DiskStatus
	var fs syscall.Statfs_t
	err := syscall.Statfs("/", &fs)
	if err == nil {
		disk.All = fs.Blocks * uint64(fs.Bsize)
		disk.Free = fs.Bfree * uint64(fs.Bsize)
		disk.Used = disk.All - disk.Free
	}
	return disk
}