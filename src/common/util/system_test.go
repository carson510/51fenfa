package util

import (
	"testing"
	"github.com/kimiazhu/log4go"
)

func TestDiskUsage(t *testing.T) {
	disk := DiskUsage()
	log4go.Debug(disk.All)
	log4go.Debug(disk.Free)
	log4go.Debug(disk.Used)
	log4go.Close()
}
