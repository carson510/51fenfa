package constant

const (
	Code_Error_Unknow          = -1
	Code_Success               = 0
	Code_Error_OperationFailed = 1
	Code_Error_InVaildMethod   = 22
	Code_Error_ParamError      = 23
	Code_Error_SessionIllegal  = 24
	Code_Error_DuplicatedInfo  = 27
	Code_Error_NotFound        = 28
	Code_Error_Wrong           = 29
	Code_Error_Disabled        = 30
	Code_Error_ServerError     = 999
	Code_Error_NoPermission    = 1004

	Code_Error_NotActiveUser   = 25
	Code_Error_DuplicatedEmail = 26
	Code_Error_UserIsInVaild   = 1002
)

const (
	Info_Error_Unknow          = "unknow error"
	Info_Success               = "ok"
	Info_Error_OperationFailed = "operation failed"
	Info_Error_InVaildMethod   = "invaild method"
	Info_Error_ParamError      = "param error"
	Info_Error_SessionIllegal  = "session illegal"
	Info_Error_ServerError     = "server error"
	Info_Error_DuplicatedInfo  = "duplicate value"
	Info_Error_NotFound        = "not found"
	Info_Error_Wrong           = "wrong"
	Info_Error_Disabled        = "disabled"
	Info_Error_NoPermission    = "no permission"

	Info_Error_NotActiveUser   = "User is not in active status"
	Info_Error_DuplicatedEmail = "账户已存在，请直接登录"
	Info_Error_UserIsInVaild   = "用户名或密码错误"
)

var Info_Errors = []string{
	Info_Error_Unknow,
	Info_Success,
	Info_Error_OperationFailed,
	Info_Error_InVaildMethod,
	Info_Error_ParamError,
	Info_Error_SessionIllegal,
	Info_Error_ServerError,
	Info_Error_DuplicatedInfo,
	Info_Error_NotFound,
	Info_Error_Wrong,
	Info_Error_Disabled,
	Info_Error_NoPermission,
}
