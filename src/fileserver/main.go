package main

import (
	"net/http"
	"fileserver/service"
)

func main() {
	http.HandleFunc("/upload", service.Receiver)
	http.HandleFunc("/removefile", service.RemoveFile)
	http.HandleFunc("/diskusage", service.DiskUsage)
	http.ListenAndServe(":65501", nil)
}
