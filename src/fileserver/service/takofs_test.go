package service

import (
    "testing"
    "github.com/kimiazhu/log4go"
    "time"
)

func TestRemoveUploadFilesBefore(t *testing.T) {
    RemoveUploadFilesBefore(1)
    time.Sleep(5 * time.Second)
    log4go.Close()
}
