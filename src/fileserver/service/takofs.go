package service

import (
    "io"
    "net/http"
    "os"
    "path"
    "strings"
    "time"
    "github.com/kimiazhu/log4go"
    "common/util/http"
    "errors"
    "io/ioutil"
    "net"
    "gopkg.in/mgo.v2/bson"
    "common/util"
)

const (
    UPLOAD_ROOT_PATH = "static"
    DOWNLOAD_ROOT_PATH = "download"
    UPLOAD_DATE_FORMAT = "20060102"
)

func isPathExists(path string) (bool, error) {
    _, err := os.Stat(path)
    if err == nil {
        return true, nil
    }
    if os.IsNotExist(err) {
        return false, nil
    }
    return false, err
}

func ParseFile(w http.ResponseWriter, r *http.Request) (string, error) {

    log4go.Debug("start to parse form data ...")
    r.ParseMultipartForm(1024 * 1024 * 1024)
    filename := r.Form.Get("filename")
    file, fh, err := r.FormFile("file")
    if err != nil {
        log4go.Error("failed to get form file: ", err)
        return "", err
    }
    if file == nil || fh == nil {
        return "", errors.New("cannot get file data")
    }
    defer file.Close()

    if filename == "" {
        filename = fh.Filename
    }
    log4go.Info("received file: " + filename)
    filenameExt := ""
    if filename != "" {
        if strings.LastIndex(filename, ".") > 0 {
            filenameExt = filename[strings.LastIndex(filename, "."):]
        }
    }

    newName := bson.NewObjectId().Hex() + filenameExt

    t := time.Now()
    date := t.Format(UPLOAD_DATE_FORMAT)

    filepath := UPLOAD_ROOT_PATH + "/" + date + "/" + newName

    dir, err := os.Getwd()
    if err != nil {
        return "", err
    }
    //if dir == "/" { // TODO why not working in linux ???
    //	dir = "/tako"
    //}
    fullpath := path.Join(dir, filepath)
    log4go.Debug("file path: " + fullpath)
    dir = path.Dir(fullpath)
    exists, err := isPathExists(dir)
    if err != nil {
        return "", err
    }
    if !exists {
        err := os.MkdirAll(dir, os.ModePerm)
        if err != nil {
            return "", err
        }
    }
    f, err := os.Create(fullpath)
    if err != nil {
        return "", err
    }
    //buffer := make([]byte, 128*1024*1024)
    log4go.Debug("saving file from memory")
    _, err = io.CopyBuffer(f, file, nil)
    log4go.Debug("saved file: ")
    if err != nil {
        return "", err
    }
    if err != nil {
        return "", err
    }
    return filepath, nil
}

func Receiver(w http.ResponseWriter, r *http.Request) {

    if r.Method != "POST" {
        httputil.WriteParamError(w, "method")
        return
    }
    filepath, err := ParseFile(w, r)
    if err != nil {
        httputil.WriteError(w, err)
        return
    }
    host := r.Host
    log4go.Debug("host=" + host)
    if strings.HasSuffix(host, ":80") {
        host = strings.Replace(host, ":80", "", -1)
    }
    filepath = strings.Replace(filepath, UPLOAD_ROOT_PATH + "/", DOWNLOAD_ROOT_PATH + "/", -1)
    //filepath = "http://" + host + "/" + filepath

    log4go.Debug("file key: %s", filepath)

    httputil.WriteData(w, filepath, "path")
}

func RemoveUploadFilesBefore(day int) {

    wd, _ := os.Getwd()
    log4go.Debug(wd)

    today, _ := time.Parse(UPLOAD_DATE_FORMAT, time.Now().Format(UPLOAD_DATE_FORMAT))
    expiredDate := today.Add(time.Duration(24 * day * -1) * time.Hour)
    dirs, err := ioutil.ReadDir(UPLOAD_ROOT_PATH)
    if err != nil {
        return
    }
    for _, dir := range dirs {
        log4go.Debug(dir.Name())
        t, err := time.Parse(UPLOAD_DATE_FORMAT, dir.Name())
        if err != nil {
            continue
        }
        log4go.Debug(t)
        log4go.Debug(expiredDate)
        log4go.Debug(t.Before(expiredDate))
        if t.Before(expiredDate) {
            removeDir(UPLOAD_ROOT_PATH + "/" + dir.Name())
        }
    }
}

func removeDir(path string) {
    go func() {
        os.RemoveAll(path)
    }()
}

func RemoveFile(w http.ResponseWriter, r *http.Request) {
    if r.Method != "DELETE" {
        httputil.WriteParamError(w, "method")
        return
    }
    filepath := r.URL.Query()["filepath"][0]
    filepath = strings.Replace(filepath, DOWNLOAD_ROOT_PATH, UPLOAD_ROOT_PATH, -1)
    log4go.Info("removing file: " + filepath)
    err := os.Remove(filepath)
    if err != nil {
        log4go.Error("remve failed: ", err)
        httputil.WriteError(w, err)
    } else {
        httputil.WriteOK(w)
    }
}

func DiskUsage(w http.ResponseWriter, r *http.Request) {
    if r.Method != "GET" {
        httputil.WriteParamError(w, "method")
        return
    }
    httputil.WriteData(w, util.DiskUsage(), "disk")
}

var internet_ip = ""
var internal_ip = ""

func getMachinePublicIP() string {
    if internet_ip == "" {
        fetchIP()
    }
    return internet_ip
}

func getMachinePrivateIP() string {
    if internal_ip == "" {
        fetchIP()
    }
    return internal_ip
}

func fetchIP() {
    addrs, err := net.InterfaceAddrs()
    if err != nil {
        return
    }
    for _, a := range addrs {
        if ipnet, ok := a.(*net.IPNet); ok && !ipnet.IP.IsLoopback() {
            if ipnet.IP.To4() != nil {
                ip := ipnet.IP.String()
                if strings.HasPrefix(ip, "10.") ||
                strings.HasPrefix(ip, "172.16.") ||
                strings.HasPrefix(ip, "172.17.") ||
                strings.HasPrefix(ip, "172.19.") ||
                strings.HasPrefix(ip, "172.20.") ||
                strings.HasPrefix(ip, "172.21.") ||
                strings.HasPrefix(ip, "172.22.") ||
                strings.HasPrefix(ip, "172.23.") ||
                strings.HasPrefix(ip, "172.24.") ||
                strings.HasPrefix(ip, "172.25.") ||
                strings.HasPrefix(ip, "172.26.") ||
                strings.HasPrefix(ip, "172.27.") ||
                strings.HasPrefix(ip, "172.28.") ||
                strings.HasPrefix(ip, "172.29.") ||
                strings.HasPrefix(ip, "172.30.") ||
                strings.HasPrefix(ip, "172.31.") ||
                strings.HasPrefix(ip, "192.168.") {
                    internal_ip = ip
                    log4go.Info("get machine internal ip: " + internal_ip)
                } else {
                    internet_ip = ip
                    log4go.Info("get machine internet ip: " + internet_ip)
                }
            }
        }
    }
}