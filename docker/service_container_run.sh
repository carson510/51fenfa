#!/usr/bin/env bash

exist=`docker network inspect lannet`
if [[ $exist = "Error: No such network: lannet" ]]; then
    docker network create lannet
    echo 'create network lannet successfully'
else
    echo 'netowrk lannet existed'
fi

cname="51fenfa_service"
if [[ -n "$1" ]]; then
    cname=$1
fi
docker kill $cname
docker rm $cname
docker run --name $cname --net lannet -v /Users/carson/Developer/51fenfa/docker/service:/51fenfa_service -itd 51fenfa_service


docker logs -f $cname