#!/usr/bin/env bash 

#docker pull mongo

docker network inspect lannet
if [[ $? = 1 ]]; then
    docker network create lannet
    echo 'create network lannet successfully'
else
    echo 'netowrk lannet existed'
fi

cname="51fenfa_db"
if [[ -n "$1" ]]; then
    cname=$1
fi
docker kill $cname
docker rm $cname
docker run --name $cname --net lannet -d mongo

docker logs -f $cname