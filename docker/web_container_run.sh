#!/usr/bin/env bash 

exist=`docker network inspect lannet`
if [[ $exist = "Error: No such network: lannet" ]]; then
    docker network create lannet
    echo 'create network lannet successfully'
else
    echo 'netowrk lannet existed'
fi

cname="51fenfa_web"
if [[ -n "$1" ]]; then
    cname=$1
fi
docker kill $cname
docker rm $cname
docker run --name $cname --net lannet -v /Users/carson/Developer/51fenfa/docker/web:/51fenfa_web -v /Users/carson/Developer/51fenfa/docker/web_nginx.conf:/etc/nginx/nginx.conf:ro -d -p 80:65503 nginx

#docker logs -f $cname