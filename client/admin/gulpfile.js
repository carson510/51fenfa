// var srcPath = './src';
// var distPath = './dist';
'use strict';

var gulp = require('gulp'),
  $ = require('gulp-load-plugins')(),
  path = require('path'),
  sass = require('gulp-sass'),
  mock = require('n-mock'),
  eslint = require('gulp-eslint'),
  mainBowerFiles = require('main-bower-files'),
  serveStatic = require('serve-static'),
  postcss = require('gulp-postcss'),
  useref = require('gulp-useref'),
  minifyCSS = require('gulp-minify-css'),
  //ngAnnotate = require('gulp-ng-annotate'),
  sourcemaps = require('gulp-sourcemaps'),
  autoprefixer = require('autoprefixer'),
  source = require('vinyl-source-stream'),
  //babelify = require('babelify'),
  browserify = require('browserify'),
  runSequence = require('run-sequence'),
  browserSync = require('browser-sync').create(),
  minifyHTML = require('gulp-minify-html'),
  ngHtml2Js = require('gulp-ng-html2js');

var srcPath = path.join(process.cwd(), 'src');
var distPath = path.join(process.cwd(), 'dist');
var mockPath = path.join(process.cwd(), 'mocks');
gulp.task('default', function(callback) {
  return runSequence('css', 'html', 'js', 'copy', 'serve','watch', callback);
});

// Static server
gulp.task('serve', function() {
  browserSync.init({
    port: 8080,
    server: {
      baseDir: distPath
    },
    middleware: [
      serveStatic(distPath),
      mock(mockPath)
    ],
    files: [distPath + '/*.html', distPath + '/**/*']
  });
});

gulp.task('watch', function(){
  gulp.watch(srcPath + '/scss/*', ['css']);
  gulp.watch(srcPath + '/js/*', ['js']);
  gulp.watch(srcPath + '/views/**/*.tpl.html', ['templates']);
  gulp.watch(srcPath + '/*.html', ['html', 'copy']);
});

gulp.task('copy', function() {
  return gulp.src(srcPath + '/img/*').pipe(gulp.dest(distPath + '/img/'));
  gulp.src(srcPath + '/fonts/*').pipe(gulp.dest(distPath + '/fonts'));
});

gulp.task('css', function() {
  return gulp.src(srcPath + '/scss/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass())
    //.pipe(minifyCSS())
    .pipe(postcss([autoprefixer({
      browsers: ['last 2 version']
    })]))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(distPath + '/css'))
    .pipe(gulp.dest(srcPath + '/css'));

});

gulp.task('html', function() {
  var assets = useref.assets();
  return gulp.src(srcPath + '/*.html')
    .pipe(assets)
    .pipe(assets.restore())
    .pipe(useref())
    .pipe(gulp.dest(distPath));
});

gulp.task('fonts', function() {
  return gulp.src(mainBowerFiles({
    filter: '**/*.{eot,svg,ttf,woff,woff2}'
  }).concat(srcPath + '/fonts/**/*'))
    .pipe(gulp.dest(distPath + '/fonts/'));
});

gulp.task('templates', function() {
  return gulp.src(srcPath + '/views/**/*.tpl.html')
    .pipe(minifyHTML({empty: true, spare: true, quotes: true}))
    .pipe(ngHtml2Js({moduleName: 'app.templates', prefix: 'views/'}))
    .pipe($.concat('templates.js'))
    .pipe(gulp.dest(srcPath + '/js'));
});

gulp.task('js',['templates'], function() {
  return browserify(srcPath + '/js/app.js')
    //.transform(babelify)
    .bundle()
    .pipe(source('app.js'))
    .pipe($.rename('bundle.js'))
    //.pipe(uglify())
    .pipe(gulp.dest(srcPath + '/js'))
    //.pipe(ngAnnotate({add: true}))
    .pipe(gulp.dest(distPath + '/js'));
});

gulp.task('lint', function(){
  return gulp.src(srcPath + '/js/*.js')
    .pipe(eslint())
    .pipe(eslint.format());
});

gulp.task('build', function(callback){
  return runSequence('lint', 'css', 'html', 'js', 'copy', callback);
});

gulp.task('deploy-copy', function(){
  gulp.src('./dist/css/*.css').pipe(gulp.dest(staticDir + '/css'));
  gulp.src('./dist/js/*.js').pipe(gulp.dest(staticDir + '/js'));
  gulp.src('./dist/img/*').pipe(gulp.dest(staticDir + '/img'));
  gulp.src('./dist/*.html').pipe(gulp.dest(staticDir));
});

gulp.task('deploy', function() {
  return runSequence('build', 'deploy-copy');
});
