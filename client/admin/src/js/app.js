'use strict';

require('./templates');

var betaApp = angular
  .module('betaApp', ['ui.router',
    // 'ngResource',
    // 'ngAnimate',
    // 'ngSanitize',
    // 'ngStorage',
    'app.templates',
    require('./route/route').name,
    // require('./config/_config').name,
    // require('./directives').name,
    // require('./services').name,
    // require('./filters').name,
    require('./controllers').name


  ]);

$(function() {
  angular.element(document).ready(function() {
    angular.bootstrap(document, ['betaApp']);
  });
});

module.exports = betaApp;
