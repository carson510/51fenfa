'use strict';

module.exports = angular.module('app.route', [])
  .config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/');
    $stateProvider
      .state('home', {url: '/', templateUrl: 'views/tpl/content.tpl.html'})
      .state('account', {
        url: '/admin/account',
        templateUrl: 'views/tpl/account.tpl.html'
      })
      .state('accountlist', {
        url: '/admin/accountlist',
        templateUrl: 'views/tpl/accountlist.tpl.html'
      })
      .state('apptrend', {
        url: '/admin/apptrend',
        templateUrl: 'views/tpl/apptrend.tpl.html'
      })
      .state('applist', {
        url: '/admin/applist',
        templateUrl: 'views/tpl/applist.tpl.html'
      })
  });
