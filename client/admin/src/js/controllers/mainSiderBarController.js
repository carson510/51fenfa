'use strict';

// @ngInject
module.exports = function($scope, $rootScope, $state) {
  var vm = this;
  vm.toAccount = function(){
    $state.go('account');
  };
  vm.toAccountList = function(){
    $state.go('accountlist');
  };
  vm.toAppTrend = function(){
    $state.go('apptrend');
  };
  vm.toAppList = function(){
    $state.go('applist');
  };
}
