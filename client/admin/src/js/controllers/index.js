'use strict';

module.exports = angular
  .module('app.controllers', [])
  .controller('MainSiderBarController', require('./mainSiderBarController'));
